from __future__ import print_function
import numpy as np
import os
import logging as log
import ROOT


def get_color(color):
    if type(color) is int:
        return color
    else:
        return ROOT.TColor.GetColor(color)


def enable_debug():
    log.getLogger().setLevel(log.DEBUG)


def enable_info():
    log.getLogger().setLevel(log.INFO)


def convert_hist_to_arrays(hist, overflow=False, underflow=False):
    nbins = hist.GetNbinsX()
    fbin = 0
    if underflow is False:
        fbin = 1
    nbins = nbins + 1 if overflow is False else nbins + 2
    contents = []
    errors = []
    for ibin in range(fbin, nbins):
        contents.append(hist.GetBinContent(ibin))
        errors.append(hist.GetBinError(ibin))

    return np.array(contents), np.array(errors)


def delete_tobject(tobject):
    """Delete the TObject, freeing the memory for other things.

    Doing the usual Python `del my_var` doesn't necessarily delete the object
    reference by my_var if the object is a ROOT object.
    One way to ensure the object is truely deleted is documented here:
        http://wlav.web.cern.ch/wlav/pyroot/memory.html
    and that is what this method does.
    After this method has been called, the variable tobject will be None.
    """
    if tobject is not None:
        tobject.IsA().Destructor(tobject)

def createGraphAsymmErrors(hist, min_b, max_b, fl_name=None):
    graph = ROOT.TGraphAsymmErrors(hist)
    dbg_list = []
    nbins = hist.GetNbinsX()
    # 1st bin in histo (bin id 1) became point 0 in the graph </3 ROOT
    # Going to have to shift this now correctly so point id gets uncertainty
    # from bin id+1
    for ibin in range(0, nbins):
        graph.SetPointEYhigh(ibin, max_b[ibin+1])
        graph.SetPointEYlow(ibin, min_b[ibin+1])
        dbg_str = '{} & \t {:.4f} &\t {:.4f} &\t {:.4f} \\\\'.format(ibin+1, hist.GetBinContent(ibin+1), max_b[ibin+1], min_b[ibin+1])
        dbg_list.append(dbg_str)
        log.info(dbg_str)
    if fl_name:
        with open(fl_name, 'w') as f:
            f.writelines([d + '\n' for d in dbg_list])

    return graph


def quiet_mode(silence_roofit=True):
    """Enables ROOT batch mode (no X windows) and sets no INFO logging."""
    import ROOT
    # Enable batch mode -> no X windows
    ROOT.gROOT.SetBatch(True)
    # Disable INFO level logging, i.e. WARNING and up
    # ROOT.gErrorIgnoreLevel = ROOT.kWarning
    # Disable INFO level RooFit logging
    # ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.WARNING)
    # *Really* shut up, RooFit


def title(s, banner=False):
    """Print string s as a title, underlined or surrounded by hypens."""
    if banner:
        print('-'*79)
    print(s)
    if banner:
        print('-'*79)
    else:
        print('-'*len(s))


def directory_exists(path):
    """Return True if a directory exists at path."""
    return os.path.exists(path)


def ensure_directory_exists(path):
    """Create directory structure matching path if it doesn't already exist."""
    if not directory_exists(path):
        log.info('Creating directory structure {0}'.format(path))
        # Wrapping this in a try except to catch multiple attempts at creating
        # this directory if running in multiprocessing
        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno == errno.EEXIST:
                log.warn('Directory structure {} did exist'.format(path))
            else:
                raise
