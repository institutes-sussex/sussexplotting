from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define SRs
'''
region('IncSR_emm',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 2),
            ('nEl', 1),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('LepPt[2]', 8, None),                                                                                                                                                                                                                       ('((LepPdgId[0]==11)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==11)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==11)&&(LepCharge[0]*LepCharge[1]==1))',),
            ('((LepPdgId[0]==11)&&(LepCharge[0]*LepCharge[1]==-1)) ||((LepPdgId[1]==11)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[2]==11)&&(LepCharge[2]*LepCharge[0]==-1))',),
            ('LepPt[0]',None, 60),
            ('eT_miss', None, 60) 
           ],
  
)
'''
region('OSSF_emm_ID',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 2),
            ('nEl', 1),
            ('isME==1 || isEM==1 || isMM==1',),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('LepPt[2]', 8, None),
            ('((LepPdgId[0]==11)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[1]==11)&&(LepCharge[0]*LepCharge[2]==-1)) ||((LepPdgId[2]==11)&&(LepCharge[0]*LepCharge[1]==-1))',),
#            ('LepPt[0]',None, 60),
            ('fabs(mll3lZ-91.2)', None, 15),
            ('eT_miss', None, 60)

           ],

)

region('OSSF_emm_AntiID',cuts=[
            ('passHNL3LTrig', 1),
            ('BL_pass3L', 1),
            ('num_bjets', 0),
            ('nMu_baseline', 2),
            ('nMu', 2),
            ('BL_isME==1 || BL_isEM==1 || BL_isMM==1',),                                            
            ('nEl_baseline', 1),
            ('nEl', 0),
            ('BL_LepPt[0]', 15, None),
            ('BL_LepPt[1]', 9, None),
            ('BL_LepPt[2]', 8, None),
            ('((BL_LepPdgId[0]==11)&&(BL_LepCharge[1]*BL_LepCharge[2]==-1)) ||((BL_LepPdgId[1]==11)&&(BL_LepCharge[0]*BL_LepCharge[2]==-1)) ||((BL_LepPdgId[2]==11)&&(BL_LepCharge[0]*BL_LepCharge[1]==-1))',),
#            ('BL_LepPt[0]',None, 60),
            ('fabs(BL_mll3lZ-91.2)', None, 15),
            ('eT_miss', None, 60)
           ],

)


'''
region('IncSR_eem',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('LepPt[2]', 8, None),
            ('((LepPdgId[0]==13)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==13)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==13)&&(LepCharge[0]*LepCharge[1]==1))',),
            ('((LepPdgId[0]==13)&&(LepCharge[0]*LepCharge[1]==-1)) ||((LepPdgId[1]==13)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[2]==13)&&(LepCharge[2]*LepCharge[0]==-1))',),
            ('LepPt[0]',None, 60),
            ('eT_miss', None, 60)                                                                                                                                                                                                            
           ],

)

region('OSSF_eem_Id',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('LepPt[2]', 8, None),
            ('(LepPdgId[1]==13) || (LepPdgId[2]==13)',),
            ('((LepPdgId[1]==13)&&(LepCharge[0]*LepCharge[2]==-1)) ||((LepPdgId[2]==13)&&(LepCharge[0]*LepCharge[1]==-1))',),
            ('fabs(mll3lZ-91.2)', None, 15), 
            ('eT_miss', None, 60)
           ],

)

region('OSSF_eem_AntiId',cuts=[
            ('passHNL3LTrig', 1),
            ('BL_pass3L', 1),
            ('num_bjets', 0),
            ('nMu_baseline', 1),
            ('nEl', 2),
            ('BL_LepPt[0]', 15, None),
            ('BL_LepPt[1]', 9, None),
            ('BL_LepPt[2]', 8, None),
            ('((BL_LepPdgId[1]==13)&&(BL_LepCharge[0]*LepCharge[2]==-1)) ||((BL_LepPdgId[2]==13)&&(BL_LepCharge[0]*BL_LepCharge[1]==-1))',),
	    ('((BL_LepPdgId[2]==13)&&(BL_LepIsIso[2]!=1 || BL_LepD0sig[2] <3.))||((BL_LepPdgId[1]==13)&&(BL_LepIsIso[1]!=1 || BL_LepD0sig[1] <3.))',),
            ('fabs(BL_mll3lZ-91.2)', None, 15),
            ('eT_miss', None, 60)
           ],

)



region('OSSF_emm_PCVeto',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 2),
            ('nEl', 1),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('LepPt[2]', 8, None),
            ('((LepPdgId[0]==11)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[1]==11)&&(LepCharge[0]*LepCharge[2]==-1)) ||((LepPdgId[2]==11)&&(LepCharge[0]*LepCharge[1]==-1))',),
            ('LepPt[0]',None, 60),
            ('fabs(mlll-91.2)',15, None),
            ('fabs(mll3lZ-91.2)', None, 15),
            ('eT_miss', None, 60)
           ],

)

region('OSSF_emm_PC',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 2),
            ('nEl', 1),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('LepPt[2]', 8, None),
            ('((LepPdgId[0]==11)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[1]==11)&&(LepCharge[0]*LepCharge[2]==-1)) ||((LepPdgId[2]==11)&&(LepCharge[0]*LepCharge[1]==-1))',),
            ('LepPt[0]',None, 60),
            ('fabs(mlll-91.2)',None, 15),
            ('fabs(mll3lZ-91.2)', None, 15),
            ('eT_miss', None, 60)
           ],

)
'''
