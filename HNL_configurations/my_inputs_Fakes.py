from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np



lumiA = 139/58.4
lumiD = 0.79
lumiE = 1

# **For histo buffering reasons, if the root file name changes, the name label
# must also change, otherwise it will throw a mismatch error**


# Define directory paths
basedirE = '/mnt/lustre/projects/epp/general/atlas/EXO/mc16e_merged_withxsec/'
basedirD = '/mnt/lustre/projects/epp/general/atlas/EXO/mc16d_merged_withxsec/'
basedirA = '/mnt/lustre/projects/epp/general/atlas/EXO/mc16a_merged_withxsec/'


sigdirE  = '/mnt/lustre/projects/epp/general/atlas/EXO/mc16e_sig_merged_withxsec/'
#sigdirD  = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16d_SigNewGrid_merged_withxsec/'
#sigdirA  = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16e_SigNewGrid_merged_withxsec/'

#fakedirA = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16a/fakes/'
#fakedirD = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16d/fakes/'
#fakedirE = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16e/fakes/'

#datadir16 = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/data16_merged_withvars_skimmed/'
#datadir17 = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/data17_merged_withvars_skimmed/'
datadir18 = '/mnt/lustre/projects/epp/general/atlas/EXO/data_merged/'


# Define weights
basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*HNL3LTriggerSF*btagSFCentral'
#basesel ='lumiXsecWgt*TotalWeight'
fakesel ='FFWeight'

sigsel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*HNL3LTriggerSF*btagSFCentral'
#sigsel ='lumiXsecWgt*TotalWeight'
datasel ='EventWeight'



#input(name='singleTop_E',    files=basedirE+'SingleTop.root',    treename='HFntuple',    color=ROOT.kGreen+9,     scale=lumiE, weights=basesel,    label='SingleTop')

#input(name='ttbar_E',    files=basedirE+'ttbar.root',    treename='HFntuple',    color=ROOT.kMagenta-5,     scale=lumiE, weights=basesel,    label='ttbar')

'''
merge(
input(name='FakeA',    files=basedirA+'Fake_mc.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=fakesel,    label='Fake', isData=True),
input(name='FakeD',    files=basedirD+'Fake_mc.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=fakesel,    label='Fake', isData=True),
input(name='FakeE',    files=basedirE+'Fake_mc.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=fakesel,    label='Fake', isData=True)
)
'''
merge(
input(name='FakeA',    files=basedirA+'Fake_ZJets_HiMET.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=fakesel,    label='Fake', isData=True),
input(name='FakeD',    files=basedirD+'Fake_ZJets_FAKE.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=fakesel,    label='Fake', isData=True),
input(name='FakeE',    files=basedirE+'Fake_ZJets_FAKE.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=fakesel,    label='Fake', isData=True)
)

'''
#merge(
#    input(name='ZjetsA',   files=basedirA+'ZSh2211.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets', force_positive=True),
#input(name='ZjetsD',   files=basedirD+'ZSh2211.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets', force_positive=True),
#input(name='ZjetsE',   files=basedirE+'ZSh2211.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets', force_positive=True)
#)
'''
merge(
input(name='ZjetsA',   files=basedirA+'ZSh2211.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets'),
input(name='ZjetsD',   files=basedirD+'ZSh2211.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets'),
input(name='ZjetsE',   files=basedirE+'ZSh2211.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets')
)
'''
merge(
input(name='Wjets_A',    files=basedirA+'WJets_2211.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='Wjets'),
input(name='Wjets_D',    files=basedirD+'WJets_2211.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='Wjets'),
input(name='Wjets_E',    files=basedirE+'WJets_2211.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='Wjets')
)
'''
