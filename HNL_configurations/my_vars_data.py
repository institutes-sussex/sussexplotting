from config.vars import variable

# Variables to plot
# reverse_zn=true will reverse the Zn curves in N-1 plots in order to look at
# upper cuts instead of lower cuts
# Branch can be specified independent of plot name, so that upper and lower
# plots can be run together

'''
variable(name='mll', branch='mll3lZ', unit='GeV', label='m_{ll}^{OS}', bins=(10, 0, 100),reverse_zn='true')
variable(name='m3l', branch='mlll', unit='GeV', label='m_{lll}', bins=(10, 0, 200),reverse_zn='true')

variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(10, 0, 150),reverse_zn='true')
variable(name='met_Sig', unit='', label='E_{T}^{miss} significance', bins=(20, 0, 20))

variable(name='LepPt0', branch='LepPt[0]', unit='GeV', label='p_{T}^{l1}', bins=(8, 15, 95),reverse_zn='true')
#variable(name='LepEta0', branch='LepEta[0]', unit='', label='#eta^{l1}', bins=(20, -5, 5))
#variable(name='LepPhi0', branch='LepPhi[0]', unit='', label='#phi^{l1}', bins=(20, -5, 5))
variable(name='LepPt1', branch='LepPt[1]', unit='GeV', label='p_{T}^{l2}', bins=(6, 9, 69),reverse_zn='true')
#variable(name='LepEta1', branch='LepEta[1]', unit='', label='#eta^{l2}', bins=(20, -5, 5))
#variable(name='LepPhi1', branch='LepPhi[1]', unit='', label='#phi^{l2}', bins=(20, -5, 5))
variable(name='LepPt2', branch='LepPt[2]', unit='GeV', label='p_{T}^{l3}', bins=(6, 3, 63),reverse_zn='true')

#variable(name='JetPt', branch='JetPt',unit='GeV', label='p_{T}^{jet}', bins=(20, 0, 500))
'''
variable(name='LepPt0', branch='LepPt[0]', unit='GeV', label='p_{T}^{1l}', bins=(20, 0, 200))
variable(name='LepPt1', branch='LepPt[1]', unit='GeV', label='p_{T}^{2l}', bins=(10, 0, 100))
variable(name='LepPt2', branch='LepPt[2]', unit='GeV', label='p_{T}^{3l}', bins=(10, 0, 100))
variable(name='Leppt', branch='LepPt', unit='GeV', label='p_{T}^{l}', bins=(20, 0, 200))

variable(name='m3l', branch='mlll', unit='GeV', label='m_{lll}', bins=(10, 0, 200))
variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(15, 0, 150))
variable(name='met_Sig', unit='', label='E_{T}^{miss} significance', bins=(20, 0, 20)) 

variable(name='ElePt', branch='(LepPdgId[0]==11)*LepPt[0]+(LepPdgId[1]==11)*LepPt[1]+(LepPdgId[2]==11)*LepPt[2]',unit='', label='p_{T}^{e}', bins=(20, 0, 200))
variable(name='MuPt', branch='(LepPdgId[0]==13)*LepPt[0]+(LepPdgId[1]==13)*LepPt[1]+(LepPdgId[2]==13)*LepPt[2]',unit='', label='p_{T}^{#mu}', bins=(20, 0, 200))
#variable(name='ElePt', branch='(LepPdgId[0]==11)*LepPt[0]+(LepPdgId[1]==11)*LepPt[1]+(LepPdgId[2]==11)*LepPt[2]',unit='', label='p_{T}^{e}', bins=(20, 0, 200))
#variable(name='MuPt', branch='(LepPdgId[0]==13)*LepPt[0]+(LepPdgId[1]==13)*LepPt[1]+(LepPdgId[2]==13)*LepPt[2]',unit='', label='p_{T}^{#mu}', bins=(20, 0, 200))
variable(name='mll_OF', branch='mOSDF', unit='GeV', label='m_{ll}^{OSDF}', bins=(20, 0, 200))                     
variable(name='mll_SF', branch='mOSSF', unit='GeV', label='m_{ll}^{SSSF}', bins=(20, 0, 200))                           



