from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np



lumiA = 139/58.4
lumiE = 139/58.4
#lumiE = 1.

# **For histo buffering reasons, if the root file name changes, the name label
# must also change, otherwise it will throw a mismatch error**


# Define directory paths
basedirE = '/mnt/lustre/projects/epp/general/atlas/EXO/mc16e_merged_withxsec/'
basedirD = '/mnt/lustre/projects/epp/general/atlas/EXO/mc16d_merged_withxsec/'
basedirA = '/mnt/lustre/projects/epp/general/atlas/EXO/mc16a_merged_withxsec/'

sigdirE  = '/mnt/lustre/projects/epp/general/atlas/EXO/mc16e_sig_merged_withxsec/'
#sigdirD  = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16d_SigNewGrid_merged_withxsec/'
#sigdirA  = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16e_SigNewGrid_merged_withxsec/'

#fakedirA = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16a/fakes/'
#fakedirD = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16d/fakes/'
#fakedirE = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16e/fakes/'

#datadir16 = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/data16_merged_withvars_skimmed/'
#datadir17 = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/data17_merged_withvars_skimmed/'
datadir = '/mnt/lustre/projects/epp/general/atlas/EXO/data_merged/'


# Define weights
basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*HNL3LTriggerSF*btagSFCentral'
baseSh = 'EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*HNL3LTriggerSF*btagSFCentral'
#basesel ='lumiXsecWgt*TotalWeight'
fakesel ='FakeWeight'
sigsel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*HNL3LTriggerSF*btagSFCentral'
#sigsel ='lumiXsecWgt*TotalWeight'
datasel ='EventWeight'

'''
# Include data
merge(
input(name='Data16',  files=datadir+'data_16.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True),
input(name='Data17',  files=datadir+'data_17.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True),
input(name='Data18',  files=datadir+'data_18.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True)
)

# Include fake ntuple
# force_positive prevents negative fakes substracting from the total SM
#input(name='Fakes', files=basedir+'Fakes_MM_new.root',      treename='HFntuple',       color=ROOT.kOrange-2,   scale=36.1, weights=fakesel,  label='Reducible', force_positive=True)
merge(
input(name='singleTop_A',    files=basedirA+'SingleTop.root',    treename='HFntuple',    color=ROOT.kGreen+9,     scale=lumiE, weights=basesel,    label='SingleTop'),
input(name='singleTop_D',    files=basedirD+'SingleTop.root',    treename='HFntuple',    color=ROOT.kGreen+9,     scale=lumiE, weights=basesel,    label='SingleTop'),
input(name='singleTop_E',    files=basedirE+'SingleTop.root',    treename='HFntuple',    color=ROOT.kGreen+9,     scale=lumiE, weights=basesel,    label='SingleTop')
)

merge(
input(name='VVV_A',    files=basedirA+'VVV.root',    treename='HFntuple',    color=ROOT.kCyan-5,     scale=lumiE, weights=basesel,    label='VVV'),
input(name='VVV_D',    files=basedirD+'VVV.root',    treename='HFntuple',    color=ROOT.kCyan-5,     scale=lumiE, weights=basesel,    label='VVV'),
input(name='VVV_E',    files=basedirE+'VVV.root',    treename='HFntuple',    color=ROOT.kCyan-5,     scale=lumiE, weights=basesel,    label='VVV')
)

merge(
input(name='ttV_A',    files=basedirA+'ttV.root',    treename='HFntuple',    color=ROOT.kCyan-1,     scale=lumiE, weights=basesel,    label='ttV'),
input(name='ttV_D',    files=basedirD+'ttV.root',    treename='HFntuple',    color=ROOT.kCyan-1,     scale=lumiE, weights=basesel,    label='ttV'),
input(name='ttV_E',    files=basedirE+'ttV.root',    treename='HFntuple',    color=ROOT.kCyan-1,     scale=lumiE, weights=basesel,    label='ttV')
)

merge(
input(name='ttbar_A',    files=basedirA+'ttbar.root',    treename='HFntuple',    color=ROOT.kMagenta-5,     scale=lumiE, weights=basesel,    label='ttbar'),
input(name='ttbar_D',    files=basedirD+'ttbar.root',    treename='HFntuple',    color=ROOT.kMagenta-5,     scale=lumiE, weights=basesel,    label='ttbar'),
input(name='ttbar_E',    files=basedirE+'ttbar.root',    treename='HFntuple',    color=ROOT.kMagenta-5,     scale=lumiE, weights=basesel,    label='ttbar')
)


merge(
input(name='ttbar_A',    files=basedirA+'ttbar_DiLep.root',    treename='HFntuple',    color=ROOT.kMagenta-5,     scale=lumiE, weights=basesel,    label='ttbar'),
input(name='ttbar_D',    files=basedirD+'ttbar_DiLep.root',    treename='HFntuple',    color=ROOT.kMagenta-5,     scale=lumiE, weights=basesel,    label='ttbar'),
input(name='ttbar_E',    files=basedirE+'ttbar_DiLep.root',    treename='HFntuple',    color=ROOT.kMagenta-5,     scale=lumiE, weights=basesel,    label='ttbar')
)



merge(
input(name='Vgamma_A',    files=basedirA+'Vgamma.root',    treename='HFntuple',    color=ROOT.kAzure+1,     scale=lumiE, weights=basesel,    label='Vgamma'),
input(name='Vgamma_D',    files=basedirD+'Vgamma.root',    treename='HFntuple',    color=ROOT.kAzure+1,     scale=lumiE, weights=basesel,    label='Vgamma'),
input(name='Vgamma_E',    files=basedirE+'Vgamma.root',    treename='HFntuple',    color=ROOT.kAzure+1,     scale=lumiE, weights=basesel,    label='Vgamma')
)

merge(
input(name='Wjets_A',    files=basedirA+'WJets.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='Wjets'),
input(name='Wjets_D',    files=basedirD+'WJets.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='Wjets'),
input(name='Wjets_E',    files=basedirE+'WJets.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='Wjets')
)

'''
#merge(
#input(name='Wjets_A',    files=basedirA+'WJets_2211.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=baseSh,    label='Wjets'),
#input(name='Wjets_D',    files=basedirD+'WJets_2211.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=baseSh,    label='Wjets'),
input(name='Wjets_E',    files=basedirE+'WJets.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=baseSh,    label='Wjets')
#)
'''
merge(
input(name='Wjets_A',    files=basedirA+'WSh2211.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=baseSh,    label='Wjets'),
input(name='Wjets_D',    files=basedirD+'WSh2211.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=baseSh,    label='Wjets'),
input(name='Wjets_E',    files=basedirE+'WSh2211.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=baseSh,    label='Wjets')
)

merge(
input(name='VV_1L_A',    files=basedirA+'VV_1L.root',    treename='HFntuple',    color=ROOT.kCyan-6,     scale=lumiE, weights=basesel,    label='VV_1L'),
input(name='VV_1L_D',    files=basedirD+'VV_1L.root',    treename='HFntuple',    color=ROOT.kCyan-6,     scale=lumiE, weights=basesel,    label='VV_1L'),
input(name='VV_1L_E',    files=basedirE+'VV_1L.root',    treename='HFntuple',    color=ROOT.kCyan-6,     scale=lumiE, weights=basesel,    label='VV_1L')
)

merge(
input(name='VV_2L_A',    files=basedirA+'VV_2L.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=lumiE, weights=basesel,    label='VV_2L'),
input(name='VV_2L_D',    files=basedirD+'VV_2L.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=lumiE, weights=basesel,    label='VV_2L'),
input(name='VV_2L_E',    files=basedirE+'VV_2L.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=lumiE, weights=basesel,    label='VV_2L')
)

merge(
input(name='VV_3L_A',    files=basedirA+'VV_3L.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=lumiE, weights=basesel,    label='VV_3L'),
input(name='VV_3L_1A',    files=basedirA+'VV_3L_lllv.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=lumiE, weights=basesel,    label='VV_3L'),
input(name='VV_3L_D',    files=basedirD+'VV_3L.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=lumiE, weights=basesel,    label='VV_3L'),
input(name='VV_3L_1D',    files=basedirD+'VV_3L_lllv.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=lumiE, weights=basesel,    label='VV_3L'),
input(name='VV_3L_E',    files=basedirE+'VV_3L.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=lumiE, weights=basesel,    label='VV_3L'),
input(name='VV_3L_1E',    files=basedirE+'VV_3L_lllv.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=lumiE, weights=basesel,    label='VV_3L')
)

merge(
input(name='VV_4L_A',    files=basedirA+'VV_4L.root',    treename='HFntuple',    color=ROOT.kYellow-3,     scale=lumiE, weights=basesel,    label='VV_4L'),
input(name='VV_4L_1A',    files=basedirA+'VV_4L_llll.root',    treename='HFntuple',    color=ROOT.kYellow-3,     scale=lumiE, weights=basesel,    label='VV_4L'),
input(name='VV_4L_D',    files=basedirD+'VV_4L.root',    treename='HFntuple',    color=ROOT.kYellow-3,     scale=lumiE, weights=basesel,    label='VV_4L'),
input(name='VV_4L_1D',    files=basedirD+'VV_4L_llll.root',    treename='HFntuple',    color=ROOT.kYellow-3,     scale=lumiE, weights=basesel,    label='VV_4L'),
input(name='VV_4L_E',    files=basedirE+'VV_4L.root',    treename='HFntuple',    color=ROOT.kYellow-3,     scale=lumiE, weights=basesel,    label='VV_4L'),
input(name='VV_4L_1E',    files=basedirE+'VV_4L_llll.root',    treename='HFntuple',    color=ROOT.kYellow-3,     scale=lumiE, weights=basesel,    label='VV_4L')
)


merge(
    input(name='Zjets_A',                   files=basedirA+'ZSh2211.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=baseSh,  label='Zjets',force_positive=True),
    input(name='Zjets_D',                   files=basedirD+'ZSh2211.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=baseSh,  label='Zjets',force_positive=True),
    input(name='Zjets_E',                   files=basedirE+'ZSh2211.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=baseSh,  label='Zjets',force_positive=True),
input(name='Zjets_1A',                   files=basedirA+'ZJets_LMll.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets'),
input(name='Zjets_1D',                   files=basedirD+'ZJets_LMll.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets'),
input(name='Zjets_1E',                   files=basedirE+'ZJets_LMll.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets')
)

'''
#merge(
#input(name='Zjets_A',                   files=basedirA+'ZJets_2211.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=baseSh,  label='Zjets'),
#input(name='Zjets_D',                   files=basedirD+'ZJets_2211.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=baseSh,  label='Zjets'),
input(name='Zjets_E',                   files=basedirE+'ZJets_2211.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=baseSh,  label='Zjets')
#input(name='Zjets_1A',                   files=basedirA+'ZJets_LMll.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets'),
#input(name='Zjets_1D',                   files=basedirD+'ZJets_LMll.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets'),
#input(name='Zjets_1E',                   files=basedirE+'ZJets_LMll.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets')
#)
'''
merge(
    input(name='Zjets_A',                   files=basedirA+'ZJets.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets', force_positive=True),
    input(name='Zjets_D',                   files=basedirD+'ZJets.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets', force_positive=True),
    input(name='Zjets_E',			files=basedirE+'ZJets.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets', force_positive=True)
)
# include signal
#input(name='WmuHNL_10G_lt1dd',    files=sigdirE+'WmuHNL50_10G_lt1dd_lepfilt_ch1.root',     treename='HFntuple',    color=ROOT.kPink+4,      linestyle=5,    scale=lumiD, weights=sigsel,    label='HNL10G_1m',   isSignal=True)
input(name='WeHNL_10G_lt1dd',    files=sigdirE+'WeHNL50_10G_lt1dd_lepfilt_ch1.root',     treename='HFntuple',    color=ROOT.kPink+4,      linestyle=5,    scale=lumiD, weights=sigsel,    label='HNL10G_1m',   isSignal=True)

#input(name='WmuHNL_30G_lt0dd',    files=sigdirE+'WmuHNL50_30G_lt01dd_lepfilt_ch1.root',     treename='HFntuple',    color=ROOT.kYellow+2,      linestyle=5,    scale=lumiD, weights=sigsel,    label='HNL30G_0.1m',   isSignal=True)
input(name='WeHNL_30G_lt0dd',    files=sigdirE+'WeHNL50_30G_lt01dd_lepfilt_ch1.root',     treename='HFntuple',    color=ROOT.kYellow+2,      linestyle=5,    scale=lumiD, weights=sigsel,    label='HNL30G_0.1m',   isSignal=True)

#input(name='WemHNL_30G_lt0dd',    files=sigdirE+'WeHNL50_30G_lt01dd_lepfilt_ch1.root',     treename='HFntuple',    color=ROOT.kYellow+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='HNL30G_0.1m',   isSignal=True)
#merge(
#input(name='sig_450p0_150p0_A',    files=sigdirA+'C1N2_Wh_hall_450p0_150p0_2L7.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(450,150)',   isSignal=True),
#input(name='sig_450p0_150p0_D',    files=sigdirD+'C1N2_Wh_hall_450p0_150p0_2L7.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(450,150)',   isSignal=True),
#input(name='WmuHNL_50G_lt1dd',    files=sigdirE+'WmuHNL50_50G_lt01dd_lepfilt_ch1.root',     treename='HFntuple',    color=ROOT.kRed+3,      linestyle=5,    scale=lumiD, weights=sigsel,    label='HNL50G_0.1m',   isSignal=True)
input(name='WeHNL_50G_lt1dd',    files=sigdirE+'WeHNL50_50G_lt01dd_lepfilt_ch1.root',     treename='HFntuple',    color=ROOT.kRed+3,      linestyle=5,    scale=lumiD, weights=sigsel,    label='HNL50G_0.1m',   isSignal=True)

'''


