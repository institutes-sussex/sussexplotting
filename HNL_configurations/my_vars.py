from config.vars import variable

# Variables to plot
# reverse_zn=true will reverse the Zn curves in N-1 plots in order to look at
# upper cuts instead of lower cuts
# Branch can be specified independent of plot name, so that upper and lower
# plots can be run together

'''
variable(name='mll', branch='mll3lZ', unit='GeV', label='m_{ll}^{OS}', bins=(12, 0, 60),reverse_zn='true')
#variable(name='mll', branch='mll3lZ', unit='GeV', label='m_{ll}^{OS}', bins=(10, 0, 50),reverse_zn='true')
variable(name='m3l', branch='mlll', unit='GeV', label='m_{lll}', bins=(15, 0, 150),reverse_zn='true')
#variable(name='m3l_mZ', branch='fabs(mlll-91.2)', unit='GeV', label='|m_{lll}-m_{Z}|', bins=(15, 0, 150))
'''
#variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(15, 0, 150),reverse_zn='true')

variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(8, 0, 120))
variable(name='met_Sig', unit='', label='E_{T}^{miss} significancE', bins=(5, 0, 10))
variable(name='LepPt0', branch='LepPt[0]', unit='GeV', label='p_{T}^{l1}', bins=(10, 15, 115))
variable(name='LepPt1', branch='LepPt[1]', unit='GeV', label='p_{T}^{l2}', bins=(5, 9, 59))

variable(name='LepPt2', branch='LepPt[2]', unit='GeV', label='p_{T}^{l3}', bins=(6, 4, 52))



#variable(name='LepPt0', branch='LepPt[0]', unit='GeV', label=' p_{T}^{l1}',bins=(4, np.asarray([ 8, 15, 25, 40, 60],'f')))

variable(name='LepEta0', branch='fabs(LepEta[0])', unit='', label=' #eta^{l1}', bins=(3, np.asarray([0, 0.8, 1.5, 2.5],'f')))

#variable(name='LepPt1', branch='LepPt[1]', unit='GeV', label=' p_{T}^{l2}',bins=(4, np.asarray([ 8, 15, 25, 40, 60],'f')))
variable(name='LepEta1', branch='fabs(LepEta[1])', unit='', label=' #eta^{l2}', bins=(3, np.asarray([0, 0.8, 1.5, 2.5],'f')))

#variable(name='LepPt2', branch='LepPt[2]', unit='GeV', label=' p_{T}^{l3}',bins=(4, np.asarray([ 8, 15, 25, 40, 60],'f')))
variable(name='LepEta2', branch='fabs(LepEta[2])', unit='', label=' #eta^{l3}', bins=(3, np.asarray([0, 0.8, 1.5, 2.5],'f')))

#variable(name='LepPt01', branch='LepPt[0]', unit='GeV', label='p_{T}^{l1}', bins=(9, 15, 60),reverse_zn='true')
#variable(name='LLepPt0', branch='LepPt[0]', unit='GeV', label='p_{T}^{l1}', bins=(4, 15, 35))
#variable(name='HLepPt0', branch='LepPt[0]', unit='GeV', label='p_{T}^{l1}', bins=(5, 35, 60))



#variable(name='IFFtype_Ele0', branch='BL_EleType_IFFclass[0]',unit='', label='Electron IFF type', bins=(12, -0.5, 11.5))
#variable(name='IFFtype_Mu0', branch='BL_MuType_IFFclass[0]',unit='', label='1st Muon IFF type', bins=(12, -0.5, 11.5))
#variable(name='IFFtype_Mu1', branch='BL_MuType_IFFclass[1]',unit='', label='2nd Muon IFF type', bins=(12, -0.5, 11.5))
'''
variable(name='IFFtype_Ele0', branch='BL_EleType_IFFclass[0]',unit='', label='1st Electron IFF type', bins=(12, -0.5, 11.5))
variable(name='IFFtype_El1', branch='BL_EleType_IFFclass[1]',unit='', label='2nd ELectron IFF type', bins=(12, -0.5, 11.5))
variable(name='IFFtype_Mu0', branch='BL_MuType_IFFclass[0]',unit='', label='Muon IFF type', bins=(12, -0.5, 11.5))
'''
#variable(name='IFFtype0', branch='LepType_IFFclass[0]',unit='', label='1st Lep IFF type', bins=(12, -0.5, 11.5))
#variable(name='IFFtype1', branch='LepType_IFFclass[1]',unit='', label='2nd Lep IFF type', bins=(12, -0.5, 11.5))
#variable(name='IFFtype2', branch='LepType_IFFclass[2]',unit='', label='3rd Lep IFF type', bins=(12, -0.5, 11.5))
#variable(name='IFFtype0', branch='LepType_IFFclass[0]',unit='', label='1st Lep IFF type', bins=())   
#variable(name='IFFtype_Mu2', branch='(LepPdgId[2]==13)*LepType_IFFclass[2]',unit='', label='Muon IFF type', bins=(11, -0.5, 10.5))

variable(name='mlll', unit='GeV', label='m_{lll}', bins=(10, 20, 120))
#variable(name='mll_OF', branch='mOSDF', unit='GeV', label='m_{ll}^{OSDF}', bins=(15, 0, 150))
#variable(name='mll_SF', branch='mOSSF', unit='GeV', label='m_{ll}^{SSSF}', bins=(15, 0, 150))
#variable(name='LepPt1', branch='LepPt[1]', unit='GeV', label='p_{T}^{l2}', bins=(12, 9, 69),reverse_zn='true')
#variable(name='LepPt2', branch='LepPt[2]', unit='GeV', label='p_{T}^{l3}', bins=(12, 3, 39)) 
#variable(name='ht', unit='GeV', label='H_{T}', bins=(15,0,350))
