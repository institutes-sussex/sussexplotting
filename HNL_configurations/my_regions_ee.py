from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define SRs
'''
region('Base_eem',cuts=[
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('LepPt[0]', 9, None),
            ('LepPt[1]', 9, None),
            ],
)
region('Trigger_eem',cuts=[
            ('passHNL3LTrigMatch', 1),                                                                                                                 
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('isME==1 || isEM==1 || isEE==1',),                                                                                                        
            ('LepPt[0]', 17, None),
            ('LepPt[1]', 9, None),
        ],
)



region('SSSF_eem',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('isME==1 || isEM==1 || isEE==1',),
            ('LepPt[0]', 17, None),
            ('LepPt[1]', 9, None),
            ('((LepPdgId[0]==13)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==13)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==13)&&(LepCharge[0]*LepCharge[1]==1))',),
           ],
)
'''
region('Pre-Selection_eem',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('isME==1 || isEM==1 || isEE==1',),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('LepPt[2]', 4, None),
            ('((LepPdgId[0]==13)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==13)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==13)&&(LepCharge[0]*LepCharge[1]==1))',),
            ('((LepPdgId[0]==13)&&(LepCharge[0]*LepCharge[1]==-1)) ||((LepPdgId[1]==13)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[2]==13)&&(LepCharge[2]*LepCharge[0]==-1))',),
            ('eT_miss', None, 20),       
           ],
)
'''
region('Pre-Selection_eem_lowMET',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('isME==1 || isEM==1 || isEE==1',),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('LepPt[2]', 8, None),
            ('((LepPdgId[0]==13)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==13)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==13)&&(LepCharge[0]*LepCharge[1]==1))',),
            ('((LepPdgId[0]==13)&&(LepCharge[0]*LepCharge[1]==-1)) ||((LepPdgId[1]==13)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[2]==13)&&(LepCharge[2]*LepCharge[0]==-1))',),
            ('eT_miss', None, 30),
           ],
)


region('Pre-Selection_eem_HiMET',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('isME==1 || isEM==1 || isEE==1',),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('LepPt[2]', 8, None),
            ('((LepPdgId[0]==13)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==13)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==13)&&(LepCharge[0]*LepCharge[1]==1))',),
            ('((LepPdgId[0]==13)&&(LepCharge[0]*LepCharge[1]==-1)) ||((LepPdgId[1]==13)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[2]==13)&&(LepCharge[2]*LepCharge[0]==-1))',),
            ('eT_miss', 30, None),
           ],
)

region('Pre_n-1_eem',cuts=[
#region('SR',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('isME==1 || isEM==1 || isEE==1',),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
#            ('LepPt[2]', 8, None),
            ('((LepPdgId[0]==13)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==13)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==13)&&(LepCharge[0]*LepCharge[1]==1))',),
            ('((LepPdgId[0]==13)&&(LepCharge[0]*LepCharge[1]==-1)) ||((LepPdgId[1]==13)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[2]==13)&&(LepCharge[2]*LepCharge[0]==-1))',),
#           ('eT_miss', None, 60),
#           ('LepPt[0]',None, 60),
#           ('mlll',None, 90),
#           ('mll3lZ', None, 50),
           ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
                ('eT_miss', None, 60, 0,0,1e3,2),
                ('LepPt[0]', None, 60, 0,0,1e3,2),
#                ('LepPt[2]',8, None, 0,0,1e3,2),
                ('mlll', None, 90,0,0,1e3,2),
                ('mll3lZ', None, 50,0,0,1e3,2),
            ]
)





region('SR_eem',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('((LepPdgId[0]==13)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==13)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==13)&&(LepCharge[0]*LepCharge[1]==1))',),
            ('((LepPdgId[0]==13)&&(LepCharge[0]*LepCharge[1]==-1)) ||((LepPdgId[1]==13)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[2]==13)&&(LepCharge[2]*LepCharge[0]==-1))',),
            ('eT_miss', None, 60),
            ('LepPt[0]',None, 60),
            ('LepPt[2]',8, None),
            ('mlll',40, 90),  
            ('mOSDF', None, 50), 
           ],
  
)

region('SRInc_eem',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('isME==1 || isEM==1 || isEE==1',),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('((LepPdgId[0]==13)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==13)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==13)&&(LepCharge[0]*LepCharge[1]==1))',),
            ('((LepPdgId[0]==13)&&(LepCharge[0]*LepCharge[1]==-1)) ||((LepPdgId[1]==13)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[2]==13)&&(LepCharge[2]*LepCharge[0]==-1))',),
            ('eT_miss', None, 60),
            ('LepPt[0]',None, 60),
            ('LepPt[2]',8, None),
            ('mlll',40, 90),
#            ('mOSDF', None, 50),
           ],

)

region('CRTopeem',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 1, 3),
            ('nMu', 1),
            ('nEl', 2),
            ('isME==1 || isEM==1 || isEE==1',),
            ('LepPt[0]', 17, None),
            ('LepPt[1]', 9, None),
            ('((LepPdgId[0]==13)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==13)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==13)&&(LepCharge[0]*LepCharge[1]==1))',),
            ('((LepPdgId[0]==13)&&(LepCharge[0]*LepCharge[1]==-1)) ||((LepPdgId[1]==13)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[2]==13)&&(LepCharge[2]*LepCharge[0]==-1))',),
            ('eT_miss', None, 60),
            ('LepPt[0]',None, 60),
            ('LepPt[2]',8, None),
    ],
)


region('SR_Deltam_eem',cuts=[
            ('passHNL3LTrigMatch',),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('((LepPdgId[0]==13)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==13)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==13)&&(LepCharge[0]*LepCharge[1]==1))',),
            ('((LepPdgId[0]==13)&&(LepCharge[0]*LepCharge[1]==-1)) ||((LepPdgId[1]==13)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[2]==13)&&(LepCharge[2]*LepCharge[0]==-1))',),
            ('eT_miss', None, 60),
            ('LepPt[0]',None, 60),
            ('LepPt[2]',8, None),
            ('mlll',40, 90),
            ('mOSDF', None, 50),
            ('massn', None, 50),
           ],

)

region('SR_Deltam_SF_eem',cuts=[
            ('passHNL3LTrigMatch',),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('((LepPdgId[0]==13)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==13)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==13)&&(LepCharge[0]*LepCharge[1]==1))',),
            ('((LepPdgId[0]==13)&&(LepCharge[0]*LepCharge[1]==-1)) ||((LepPdgId[1]==13)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[2]==13)&&(LepCharge[2]*LepCharge[0]==-1))',),
            ('eT_miss', None, 60),
            ('LepPt[0]',None, 60),
            ('LepPt[2]',8, None),
            ('mlll',40, 90),
            ('mOSDF', None, 50),
            ('massn', None, 50),
            ('mOSSF', 40, None)
           ],

)


region('SR_3rdLep_eem',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('isME==1 || isEM==1 || isEE==1',),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('LepPt[2]', 8, None),
            ('((LepPdgId[0]==13)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==13)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==13)&&(LepCharge[0]*LepCharge[1]==1))',),
            ('((LepPdgId[0]==13)&&(LepCharge[0]*LepCharge[1]==-1)) ||((LepPdgId[1]==13)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[2]==13)&&(LepCharge[2]*LepCharge[0]==-1))',),
            ('eT_miss', None, 60),
            ('LepPt[0]',None, 60),
#            ('fabs(mlll-91.2)',15,None),
            ('mlll',None, 90),
            ('mll3lZ', None, 50),
           ],

)


region('SR_Highmll',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 2),
            ('nEl', 1),
            ('isME==1 || isEM==1 || isMM==1',),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('((LepPdgId[0]==11)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==11)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==11)&&(LepCharge[0]*LepCharge[1]==1))',),
            ('((LepPdgId[0]==11)&&(LepCharge[0]*LepCharge[1]==-1)) ||((LepPdgId[1]==11)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[2]==11)&&(LepCharge[2]*LepCharge[0]==-1))',),
            ('eT_miss', None, 60),
            ('LepPt[0]',None, 60),
#            ('fabs(mlll-91.2)',15,None),
            ('mlll',None, 90),
            ('mll3lZ', 30, 50),
           ],

)


region('SR_1stpt30_eem',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('isME==1 || isEM==1 || isEE==1',),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('LepPt[2]', 8, None),
            ('((LepPdgId[0]==13)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==13)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==13)&&(LepCharge[0]*LepCharge[1]==1))',),
            ('((LepPdgId[0]==13)&&(LepCharge[0]*LepCharge[1]==-1)) ||((LepPdgId[1]==13)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[2]==13)&&(LepCharge[2]*LepCharge[0]==-1))',),  
            ('eT_miss', None, 60),
            ('LepPt[0]',None, 30),
            ('mlll',None, 90),
            ('mll3lZ', None, 50),
           ],

)

region('SR_1stpt3060_eem',cuts=[
            ('passHNL3LTrigMatch', 1),
            ('pass3lcut', 1),
            ('num_bjets', 0),
            ('nMu', 1),
            ('nEl', 2),
            ('isME==1 || isEM==1 || isEE==1',),
            ('LepPt[0]', 15, None),
            ('LepPt[1]', 9, None),
            ('LepPt[2]', 8, None),
            ('((LepPdgId[0]==13)&&(LepCharge[1]*LepCharge[2]==1)) ||((LepPdgId[1]==13)&&(LepCharge[0]*LepCharge[2]==1)) ||((LepPdgId[2]==13)&&(LepCharge[0]*LepCharge[1]==1))',),
            ('((LepPdgId[0]==13)&&(LepCharge[0]*LepCharge[1]==-1)) ||((LepPdgId[1]==13)&&(LepCharge[1]*LepCharge[2]==-1)) ||((LepPdgId[2]==13)&&(LepCharge[2]*LepCharge[0]==-1))',),
            ('eT_miss', None, 60),
            ('LepPt[0]',30, 60),
            ('mlll',None, 90),
            ('mll3lZ', None, 50),
           ],

)
'''

