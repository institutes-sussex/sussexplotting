from config.regions import utils
from collections import OrderedDict

_all = OrderedDict()


def get(*name):
    if len(name) == 0:
        return None
    if len(name) == 1:
        return _all[name[0]] if name[0] in _all else None

    return [_all[r] for r in name if name in _all]


def get_all():
    """Returns a list of all regions. Skips blinded if specified."""
    return _all.values()


class region:
    """Class to store regions for the analysis, specified by their name.
    Defined classes are automatically added to the module and are accessible
    by regions.<name>. Region can be marked marked blinded which will ignore
    any data being plotted for them."""

    def __init__(self, name, cuts=None, label=None,
                 blinded=False, banned_inputs=None,
                 n_mo_cuts=None):
        """Constructor for a region. Optionally, a list of cuts can be
        specified which select this region. Cuts are specified in the in tuples
        of ('varname', min, max). One-sided intervals are set by specifying
        min or max as None. Equality is set by ('varname', value).
        Blinded flag to steer plotting.
        behaviour involving data. Optional labels are used to specify a plot
        label. If none is given, name is used."""
        self.name = name
        self.banned_inputs = banned_inputs if banned_inputs is not None else []
        self.n_mo_cuts = n_mo_cuts if n_mo_cuts is not None else []
        self.n_mo_cuts_dict = {n: (l, u, ld, ud, ah, al) for n, l, u, ld, ud, ah, al in self.n_mo_cuts}
        self.label = name if label is None else label
        self.cuts = cuts
        self.blinded = blinded
        _all[name] = self

    def all_cuts(self, *var):
        """Specified variable is ignored from the n_mp_cuts"""
        cuts = []
        cuts += self.cuts
        if self.n_mo_cuts is not None:
            for c in self.n_mo_cuts:
                if True not in [c[0] == v for v in var]:
                    cuts.append(c[:3])
        return utils.combine_cut_strings(*cuts)

    def cut_vars(self):
        return [c[0] for c in self.cuts]

    def __repr__(self):
        """Return a string uniquely identifying this region and its
        properties which are relevant for the actual filling of histogram:
        regionname, all the cuts!"""
        return '{}:{}'.format(self.name, self.all_cuts())


__all__ = [region]
