class StupidCuts(Exception):
    pass

def combine_cut_strings(*args):
    formatter_cuts = []
    for cut in args:
        if len(cut) == 3:
            ma_c = None
            mi_c = None
            var, mi, ma = cut
            if mi:
                mi_c = '({} >= {})'.format(var, mi)
            if ma:
                ma_c = '({} < {})'.format(var, ma)
            if mi_c and ma_c:
                if mi > ma:
                    formatter_cuts.append('({} || {})'.format(mi_c, ma_c))
                else:
                    formatter_cuts.append('({} && {})'.format(mi_c, ma_c))
            elif mi_c:
                formatter_cuts.append(mi_c)
            elif ma_c:
                formatter_cuts.append(ma_c)
        elif len(cut) == 2:
            var, m = cut
            m_c = '({} == {})'.format(var, m)
            formatter_cuts.append(m_c)
        elif len(cut) == 1:
            var = cut[0]
            c = '({})'.format(var)
            formatter_cuts.append(c)
        elif type(cut) == str:
            formatter_cuts.append(cut)
        else:
            raise StupidCuts

    return '&&'.join([str(cstr) for cstr in formatter_cuts])
