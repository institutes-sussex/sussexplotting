from collections import OrderedDict
import sys

_central = None


def get_central():
    return _central


_all = OrderedDict()


def get(*name):
    if len(name) == 0:
        return None
    if len(name) == 1:
        return _all[name[0]] if name[0] in _all else None

    return [_all[r] for r in name if name in _all]


def get_all():
    """Returns a list of all regions. Skips blinded if specified."""
    return _all.values()


class syst:

    """Class acting as a wrapper around the systematic input ttree's. Knows all
    the files and the tree name in the file and can store the label and
    plotting informations like color etc"""

    def __init__(self, name, up_down=None, central=False, isWeight=False,
                 replaces=None, method=None):
        """Creates input dataset and sets all the necessary informations.
        Needs some more documentation. If isWeight is specified, the
        systematic is looked for in the nominal tree and added as an
        additional weight. If replaces is defined, the weight replaces a
        previously defined weight."""
        self.name = name
        self.up_down = up_down
        self.central = central
        self.isWeight = isWeight
        self.replaces = replaces
        self.method=method
        _all[name] = self
        if central:
            sys.modules[__name__]._central = self

    def __repr__(self):
        """Return a string uniquely identifying this systematic to and its
        properties which are relevant for the actual filling of histogram:
        systname, up_down, replaces, method, isweight!"""
        return '{}:{}:{}:{}:{}'.format(self.name, self.up_down,
                                       'isWeight' if self.isWeight else '',
                                       self.replaces, self.method)


__all__ = [syst, get_central]
