from collections import OrderedDict

_all = OrderedDict()


def last_modified(filename):
    import os
    return os.stat(filename).st_mtime

def get(*name):
    if len(name) == 0:
        return None
    if len(name) == 1:
        return _all[name[0]] if name[0] in _all else None

    return [_all[r] for r in name if name in _all]


def get_all():
    """Returns a list of all variables."""
    return _all.values()


def merge(*inputs):
    """Easy function to set the correct merging information for plots.
    First listed input is taken as the master input and subsequent inputs
    will be added after all histrograms have been filled."""
    if len(inputs) < 2:
        print 'ERROR: merge needs at least two inputs to make sense.'
        raise MissingInputError
    master = inputs[0]
    for i in inputs[1:]:
        master._child_inputs.append(i)
        i._master_input = master


import hashlib


def checksum_md5(filename):
    """Function to compute the hash of a provided file"""
    md5 = hashlib.md5()
    with open(filename, 'rb') as f:
        for chunk in iter(lambda: f.read(200000), b''):
            md5.update(chunk)
    return md5.digest()


def hashing_files(files):
    if type(files) == list:
        tohash = files
    else:
        tohash = [files]
    return ','.join([str(last_modified(f))  for f in tohash])

class MissingInputError(Exception):
    pass


class input:

    """Class acting as a wrapper around the input ttree's. Knows all
    the files and the tree name in the file and can store the label and
    plotting informations like color etc"""

    def __init__(
            self,
            name,
            files=None,
            treename=None,
            color=None,
            scale=1.,
            weights=None,
            label=None,
            isData=False,
            isSignal=False,
            rel_unc=None,
            region_scales=None,
            force_positive=False,
            linestyle=2):
        """Creates input dataset and sets all the necessary informations.
        Needs some more documentation"""
        self.name = name
        self.files = files
        if rel_unc:
            self.rel_unc = rel_unc
        else:
            self.rel_unc = {}
        if region_scales:
            self.region_scales = region_scales
        else:
            self.region_scales = {}
        self.label = name if label is None else label
        self.isData = isData
        self.isSignal = isSignal
        self.force_positive = force_positive
        self.color = color
        self.treename = treename
        self.scale = scale
        self.weights = weights
        self.linestyle = linestyle
        self._child_inputs = []  # For merging of inputs if needed
        self._master_input = None  # link to the master input if needed
        self._stored_repr = None
        _all[name] = self

    def __repr__(self):
        """Return a string uniquely identifying this input and its
        properties which are relevant for the actual filling of histogram:
        inputname, list of input files, hashes of input files, treenames
        and weights!"""
        if self._stored_repr:
            return self._stored_repr
        self._stored_repr = '{}:{}:{}:{}:{}'.format(self.name,
                                                    ''.join(self.files),
                                                    hashing_files(self.files),
                                                    self.treename,
                                                    self.weights)
        return self._stored_repr

__all__ = [input]
