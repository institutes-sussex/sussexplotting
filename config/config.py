ratio_color = '#ffff00'

output_prefix = 'output'
sort_stack_by_integral = False
output_types = ['eps', 'png', 'C', 'pdf', 'root']
# output_types = ['png']
use_conf_syst_convention = True
use_buffered_histograms = True
histogram_root_folder = 'histos'

# Exception management
delete_throwing_systs = False
automatically_deactivate_buffer = False
