import ROOT

# For some reason, PyROOT insists on parsing the argv command line argument
# list. Let's disable this!
ROOT.PyConfig.IgnoreCommandLineOptions = True

# Ignore more erros
ROOT.gErrorIgnoreLevel = 1999

# Initialise the ROOT graphics engine. This sometimes helps (on some platforms
# ... fucking ROOT) with the problem of creating canvases in batch mode and
# drawing them later on.
ROOT.TApplication.NeedGraphicsLibs()
ROOT.gApplication.InitializeGraphics()

# Always create Sumw2 array
ROOT.TH1.SetDefaultSumw2()

# Set max digits to a more reasonable level.
ROOT.TGaxis.SetMaxDigits(4)

# Disable the stats box and histogram titles.
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)
ROOT.gStyle.SetOptFit(0)

# Turn off borders
ROOT.gStyle.SetCanvasBorderMode(0)
ROOT.gStyle.SetFrameBorderMode(0)
ROOT.gStyle.SetPadBorderMode(0)
ROOT.gStyle.SetHistMinimumZero()

# Use a plain style.
ROOT.gStyle.SetFrameFillColor(0)
ROOT.gStyle.SetCanvasColor(0)
ROOT.gStyle.SetPadColor(0)
ROOT.gStyle.SetStatColor(0)

# Reduce top margin space.
ROOT.gStyle.SetPadTopMargin(0.05)
ROOT.gStyle.SetPadRightMargin(0.05)
ROOT.gStyle.SetPadBottomMargin(0.1)
ROOT.gStyle.SetPadLeftMargin(0.1)
#ROOT.gStyle.SetPadBottomMargin(0.16)
#ROOT.gStyle.SetPadLeftMargin(0.16)

# Set canvas area size.
ROOT.gStyle.SetCanvasDefW(800)
ROOT.gStyle.SetCanvasDefH(800)

# Put tick marks on all sides of the canvas.
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)

# Use a Pixel-Correct font
font_style = 43
leg_font_size = 20
font_size = 26
#font_size = 20

ROOT.gStyle.SetLegendFont(font_style)
try:
    ROOT.gStyle.SetLegendTextSize(leg_font_size)
except AttributeError:
    pass

for dim in ('x', 'y', 'z'):
    ROOT.gStyle.SetLabelFont(font_style, dim)
    ROOT.gStyle.SetTitleFont(font_style, dim)

    ROOT.gStyle.SetLabelSize(font_size, dim)
    ROOT.gStyle.SetTitleSize(font_size, dim)

ROOT.gStyle.SetHistLineWidth(2)


# The T{Text,Fill,Line,Marker}Att Setting Function
def set_style(style):
    style.SetTextFont(font_style)
    style.SetTextSize(font_size)

    # Use thicker lines and a larger marker by default
    style.SetMarkerStyle(20)
    style.SetMarkerSize(1.2)

# For gStyle
set_style(ROOT.gStyle)
# For gVirtualX (if available)
if ROOT.gVirtualX:
    set_style(ROOT.gVirtualX)
