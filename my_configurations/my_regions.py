from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define SRs
region('SR3-a',cuts=[
            # Format: variable, lower cut, upper cut
            ('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('pass3lcut', 1),
            ('pass2lossf', 1),
            ('num_bjets', 0),
            ('mlll',20, None),
            ('eT_miss',  20, None),
            ('LepPt[1]', 25, None),
            ('LepPt[2]', 30, None),
            ('mll3lZmin',20, None),
            ('mll3lZmin', 101.2, 81.2),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            ('mTWZmin', 110, None,0,0,10,20),
            ('eT_miss', 120, None,0,0,10,20),
            ]
)

region('SR3-b',cuts=[
            ('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('pass3lcut', 1),
            ('pass2lossf', 1),
            ('num_bjets', 0),
            ('mlll',20, None),
            ('eT_miss',  20, None),
            ('LepPt[1]', 25, None),
            ('LepPt[2]', 80, None),
            ('mll3lZmin',20, None),
            ('mll3lZmin', 101.2, None),
            ],
            n_mo_cuts=[
            ('mTWZmin', 110, None,0,0,10,20),
            ('eT_miss', 60, None,0,0,10,20),
            ]
)

