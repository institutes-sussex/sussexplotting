from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np

#fakes_rel = sqrt(0.30**2+0.15**2)
fakes_rel = 0.30

#lumi_a = 36100.
#lumi_d = 44300.
#lumi_e = 58400.
lumi_a = 1.
lumi_d = 1.
lumi_e = 58450.1
lumiVg_a = 36100.
lumiVg_d = 44300.
lumiVg_e = 58400.

sig_lumi_a = 36100000.
sig_lumi_d = 103900000.


basedir_a = "/lustre/scratch/epp/atlas/bs336/WorkDir/MiniNtuples/Rel91/mc16a/Processed/"
basedir_d = "/lustre/scratch/epp/atlas/bs336/WorkDir/MiniNtuples/Rel91/mc16d/Processed/"
basedir_e = "/mnt/lustre/scratch/epp_test/shared/WhSS/v120/mc16e_merged_withxsec_skimmed/"

sigdir_a = '/lustre/scratch/epp/atlas/ft81/workdir/MiniNtuples/AB59/SignalWh/mc16a/processed/'
sigdir_d = '/lustre/scratch/epp/atlas/ft81/workdir/MiniNtuples/AB59/SignalWh/mc16d/processed/'
sigdir_e = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120/mc16e_sig_merged_withxsec/'


datadir = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120/data18_merged_skimmed/'
fakedir = '/lustre/scratch/epp/atlas/ft81/workdir/scripts/addVar/'


#merge(
#input(name='Data1516', files=datadir+'data_2016.root', treename='HFntuple', color=ROOT.kBlack, scale=1., weights='EventWeight', label='Data', isData=True),
#input(name='Data16', files=datadir_a+'data_data15R59V1_data16_13TeV.root', treename='HFntuple', color=ROOT.kBlack, scale=1., weights='EventWeight', label='Data', isData=True),
#input(name='Data17', files=datadir+'data_2017.root', treename='HFntuple', color=ROOT.kBlack, scale=1., weights='EventWeight', label='Data', isData=True),
input(name='Data18', files=datadir+'data18.root', treename='HFntuple', color=ROOT.kBlack, scale=1., weights='1', label='Data', isData=True)
#)
#merge(
#input(name='ttVa', files=basedir_a+'ttV.root', treename='HFntuple', color=ROOT.kCyan-1, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='t#bar{t}V'),
#input(name='ttVd', files=basedir_d+'ttV.root', treename='HFntuple', color=ROOT.kCyan-1, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='t#bar{t}V'),
input(name='ttVe', files=basedir_e+'ttV.root', treename='HFntuple', color=ROOT.kCyan-1, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='t#bar{t}V')
#merge(
#input(name='Vgammaa', files=basedir_a+'Vgamma.root', treename='HFntuple', color='#228b22', scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Vgamma'),
#input(name='Vgammad', files=basedir_d+'Vgamma.root', treename='HFntuple', color='#228b22', scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Vgamma'),
#input(name='Vgammae', files=basedir_d+'Vgamma.root', treename='HFntuple', color='#228b22', scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Vgamma'))

#merge(
#input(name='ttbara', files=basedir_a+'ttbar.root', treename='HFntuple', color=ROOT.kMagenta-5 , scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='t#bar{t}'),
#input(name='ttbard', files=basedir_d+'ttbar.root', treename='HFntuple', color=ROOT.kMagenta-5, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='t#bar{t}'),
input(name='ttbare', files=basedir_e+'ttbar.root', treename='HFntuple', color=ROOT.kMagenta-5, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='t#bar{t}')
#merge(
#input(name='singletopa', files=basedir_a+'SingleT.root', treename='HFntuple', color=ROOT.kGreen+9, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='SingleT'),
#input(name='singletopd', files=basedir_d+'SingleT.root', treename='HFntuple', color=ROOT.kGreen+9, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='SingleT'),
input(name='singletope', files=basedir_e+'SingleTop.root', treename='HFntuple', color=ROOT.kGreen+9, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='SingleT')

'''
input(name='singletopa', files=basedir_a+'SingleT.root', treename='HFntuple', color=ROOT.kAzure-5, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='SingleT'),
input(name='singletopd', files=basedir_d+'SingleT.root', treename='HFntuple', color=ROOT.kAzure-5, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='SingleT'),
input(name='singletope', files=basedir_e+'SingleT.root', treename='HFntuple', color=ROOT.kAzure-5, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='SingleT'))
'''
#, rel_unc={'VRWZoffZ':0.2, 'VRWZ':0.2, 'VRWZhighMET':0.2, 'VR3Lfakes':0.2, 'SR3LH_MT':0.2, 'SR3LI_MT':0.2})

#, rel_unc={'VRWZoffZ':0.2, 'VRWZ':0.2, 'VRWZhighMET':0.2, 'VR3Lfakes':0.2, 'SR3LH_MT':0.2, 'SR3LI_MT':0.2})

#merge(
#input(name='DY', files=basedir_e+'DY.root', treename='HFntuple', color=ROOT.kOrange+6, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='DY')
#input(name='Zmll', files=basedir_e+'ZMll.root', treename='HFntuple', color=ROOT.kOrange+5, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Zmll')
merge(
input(name='ZjetsEl', files=basedir_e+'ZJets_ee.root', treename='HFntuple', color=ROOT.kOrange-5, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='ZJets'),
input(name='ZjetsMu', files=basedir_e+'ZJets_mm.root', treename='HFntuple', color=ROOT.kOrange-7, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='ZJets'),
input(name='ZjetsTau', files=basedir_e+'ZJets_tt.root', treename='HFntuple', color=ROOT.kOrange-9, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='ZJets'))
#input(name='Zgammaa', files=basedir_a+'Vgamma.root', treename='HFntuple', color=ROOT.kGray, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Fakes'),
#input(name='Zgammad', files=basedir_d+'Vgamma.root', treename='HFntuple', color=ROOT.kGray, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Fakes'),
#input(name='Zgammae', files=basedir_e+'Vgamma.root', treename='HFntuple', color=ROOT.kPink+1, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='V#gamma')

#input(name='Wgammaa', files=basedir_a+'Wgamma.root', treename='HFntuple', color=ROOT.kGray-1, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='MCFakes'),
#input(name='Wgammad', files=basedir_d+'Wgamma.root', treename='HFntuple', color=ROOT.kGray-1, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='MCFakes'),
input(name='WJetse', files=basedir_e+'WJets.root', treename='HFntuple', color=ROOT.kGray, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WJets')



#merge(
#input(name='Higgsa', files=basedir_a+'Higgs.root', treename='HFntuple', color=ROOT.kPink+1, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Higgs'),
#input(name='Higgsd', files=basedir_d+'Higgs.root', treename='HFntuple', color=ROOT.kPink+1, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Higgs'),
#input(name='Higgse', files=basedir_e+'Higgs.root', treename='HFntuple', color=ROOT.kPink+1, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Higgs'))

#merge(
#input(name='VVVa', files=basedir_a+'VVV.root', treename='HFntuple', color=ROOT.kCyan-5, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='VVV'),
#input(name='VVVd', files=basedir_d+'VVV.root', treename='HFntuple', color=ROOT.kCyan-5, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='VVV'),
input(name='VVVe', files=basedir_e+'VVV.root', treename='HFntuple', color=ROOT.kCyan-5, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='VVV')


#merge(
#input(name='Higgsa', files=basedir_a+'Higgs_3V.root', treename='HFntuple', color=ROOT.kPink+1, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Higgs'),
#input(name='Higgsd', files=basedir_d+'Higgs_3V.root', treename='HFntuple', color=ROOT.kPink+1, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Higgs'),
#input(name='Higgse', files=basedir_e+'Higgs_3V.root', treename='HFntuple', color=ROOT.kPink+1, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Higgs'))

#merge(
#input(name='VVVa', files=basedir_a+'VVV_222.root', treename='HFntuple', color=ROOT.kCyan-5, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='VVV'),
#input(name='VVVd', files=basedir_d+'VVV_222.root', treename='HFntuple', color=ROOT.kCyan-5, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='VVV'),
#input(name='VVVe', files=basedir_e+'VVV_222.root', treename='HFntuple', color=ROOT.kCyan-5, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='VVV'))

#, rel_unc={'VR3L-offZ_high':(0.0971,0.0852), 'VR3L-offZ0j':(0.0945,0.1037), 'VR3L-offZ1j':(0.0444,0.0416), 'VR3L-top':(0.0597,0.0460), 'SR3L-DFOS-0j':(0.0603,0.075), 'SR3L-DFOS-1ja':(0.0878,0.09), 'SR3L-DFOS-1jb':(0.1020,0.1163)})
#merge(
#input(name='diboson2La', files=basedir_a+'Dibosons_2L.root', treename='HFntuple', color=ROOT.kAzure+4, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='VV_2L'),
#input(name='diboson2Ld', files=basedir_d+'Dibosons_2L.root', treename='HFntuple', color=ROOT.kAzure+4, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='VV_2L'),
input(name='diboson2Le', files=basedir_e+'VV_2L.root', treename='HFntuple', color=ROOT.kAzure+4, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='VV_2L')
#, rel_unc={'VR3L-offZ_high':(0.1614,0.1220), 'VR3L-offZ0j':(0.1088,0.1193), 'VR3L-offZ1j':(0.0180,0.0204), 'VR3L-top':(0.1036,0.0778), 'SR3L-DFOS-0j':(0.1052,0.1287), 'SR3L-DFOS-1ja':(0.1328,0.1130), 'SR3L-DFOS-1jb':(0.2837,0.1880)})

#merge(
#input(name='diboson4La', files=basedir_a+'Dibosons_4L.root', treename='HFntuple', color=ROOT.kYellow-3, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='VV_4L'),
#input(name='diboson4Ld', files=basedir_d+'Dibosons_4L.root', treename='HFntuple', color=ROOT.kYellow-3, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='VV_4L'),
input(name='diboson4Le', files=basedir_e+'VV_4L.root', treename='HFntuple', color=ROOT.kYellow-3, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='VV_4L')
#, rel_unc={'VR3offZ':0.3, 'VR3Za':0.3, 'VR3Zb':0.3, 'VR3offZb':0.3, 'VR3Za-j0':0.3, 'VR3Za-j1':0.3, 'VR3Zb-j1':0.3, 'Bin1-0Ja':0.3, 'Bin2-0Jb':0.3, 'Bin3-0Jc':0.3, 'Bin4-1Ja':0.3, 'Bin5-1Jb':0.3, 'Bin6-1Jc':0.3, 'Bin1':0.3, 'Bin2':0.3, 'Bin3':0.3, 'Bin4':0.3, 'Bin5':0.3})
# SF HighHT : 0.87 LowHT :0.96
#merge(
#input(name='diboson3La', files=basedir_a+'Dibosons_3L.root', treename='HFntuple', color=ROOT.kOrange-3, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF*1.0', label='VV_3L'),
#input(name='diboson3Ld', files=basedir_d+'Dibosons_3L.root', treename='HFntuple', color=ROOT.kOrange-3, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF*1.0', label='VV_3L'),
input(name='diboson3Le', files=basedir_e+'VV_3L.root', treename='HFntuple', color=ROOT.kOrange-3, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF*1.0', label='VV_3L')
#, rel_unc={'VR3L-offZ_high':(0.1614,0.1220), 'VR3L-offZ0j':(0.1088,0.1193), 'VR3L-offZ1j':(0.0180,0.0204), 'VR3L-top':(0.1036,0.0778), 'SR3L-DFOS-0j':(0.1052,0.1287), 'SR3L-DFOS-1ja':(0.1328,0.1130), 'SR3L-DFOS-1jb':(0.2837,0.1880)})

input(name='diboson1Le', files=basedir_e+'VV_1L.root', treename='HFntuple', color=ROOT.kRed-2, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='VV_1L')


input(name='Signal1e', files=sigdir_e+'C1N2_Wh_hall_202p5_72p5_2L7.root', treename='HFntuple', color=ROOT.kAzure-2, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Wh(202,72.5)', isSignal=True)

input(name='Signal2e', files=sigdir_e+'C1N2_Wh_hall_400p0_0p0_2L7.root', treename='HFntuple', color=ROOT.kGreen-2, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Wh(400,0)', isSignal=True)
