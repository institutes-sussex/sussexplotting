from config.regions import region

'''
region('Pre-selection',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ],
        blinded=True
)


region('On-shell',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('mll3lZ', 75, 105),
##        ('mll3lZ', 12, None),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None)
        ],
        blinded=True
)


region('Lowmll',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('mll3lZ', 12, 75),
##        ('mll3lZ', 12, None),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None)
        ],
        blinded=True
)

region('Highmll',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('mll3lZ', 105, None),
##        ('mll3lZ', 12, None),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None)
        ],
        blinded=True
)

region('JetVeto_Lowmll',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('mll3lZ', 12, 75),
        ('nJets', 0),
##        ('mll3lZ', 12, None),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None)
        ],
        blinded=True
)

region('LowHT_Lowmll',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('mll3lZ', 12, 75),
        ('nJets', 1, None),
##        ('mll3lZ', 12, None),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('ht', 20, 200)
        ],
        blinded=True
)

region('HighHT_Lowmll',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('mll3lZ', 12, 75),
        ('nJets', 1, None),
##        ('mll3lZ', 12, None),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('ht', 200, None)
        ],
        blinded=True
)

region('JetVeto_Highmll',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('mll3lZ', 105, None),
        ('nJets', 0),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None)
        ],
        blinded=True
)

region('LowHT_Highmll',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('mll3lZ',105, None),
        ('nJets', 1, None),
##        ('mll3lZ', 12, None),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('ht', 20, 200)
        ],
        blinded=True
)

region('HighHT_Highmll',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('mll3lZ', 75, 105),
        ('nJets', 0),
##        ('mll3lZ', 12, None),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('ht', 200, None)
        ],
        blinded=True
)


'''

region('JetVeto_Onshell',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('nJets', 0),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('mTWZ', 100, 160),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None)
        ],
        blinded=True
)
'''
region('Jet_Onshell',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('nJets', 1, None),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('mTWZ', 100, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None)
        ],
        blinded=True
)

region('Onshell',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('nJets', 0, None),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('mTWZ', 100, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None)
        ],
        blinded=True
)



region('LowHT_Onshell',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('nJets', 1, None),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
#        ('mlll',20, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('ht', 20, 200)
        ],
        blinded=True
)

region('HighHT_Onshell',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('mll3lZ', 75, 105),
        ('nJets', 1, None),
        ('fabs(mlll-91.2)',15, None),
#        ('mlll',20, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('ht', 200, None)
        ],
        blinded=True
)

region('Jets_On-shell',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('nJets', 1, None),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
#        ('mlll',20, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None)
        ],
        blinded=True
)


region('JetVeto_Onshell_LowmT',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('nJets', 0),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('mTWZ', 0, 100),
        ],
        blinded=True
)

region('JetVeto_Onshell_ModeratemT',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('nJets', 0),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('mTWZ', 100, 160),
        ],
        blinded=True
)
region('JetVeto_Onshell_HighmT',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('nJets', 0),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('mTWZ', 160, None),
        ],
        blinded=True
)


region('LowHT_Onshell_LowmT',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('nJets', 1, None),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('ht', 20, 200),
        ('mTWZ', 0, 100),
        ],
        blinded=True
)

region('LowHT_Onshell_ModeratemT',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('nJets', 1, None),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('ht', 20, 200),
        ('mTWZ', 100, 160),
        ],
        blinded=True
)


region('LowHT_Onshell_HighmT',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('nJets', 1, None),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('ht', 20, 200),
        ('mTWZ', 160, None),
        ],
        blinded=True
)

region('HighHT_Onshell_LowmT',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('nJets', 1, None),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('ht', 200, None),
        ('LepPt[0]+LepPt[1]+LepPt[2]', 0, 350),
        ('mTWZ', 0, 100),
        ],
        blinded=True
)

region('HighHT_Onshell_ModeratemT',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('nJets', 1, None),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('ht', 200, None),
        ('LepPt[0]+LepPt[1]+LepPt[2]', 0, 350),
        ('mTWZ', 100, None),
        ],
        blinded=True
)


region('LowHT_Onshell_HighmT',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('nJets', 1, None),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('ht', 20, 200),
        ('mTWZ', 160, None),
        ],
        blinded=True
)

region('LowHT_Onshell',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('num_bjets', 0),
        ('nJets', 1, None),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
#        ('mlll',20, None),
        ('eT_miss', 50, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('ht', 20, 200)
        ],
        blinded=True
)
'''
