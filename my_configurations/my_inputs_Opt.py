from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np

#fakes_rel = sqrt(0.30**2+0.15**2)
fakes_rel = 0.30
lumi_a = 1.
lumi_d = 1.
lumi_e = 1.

lumi = 1.


basedir_a = "/lustre/scratch/epp/atlas/bs336/WorkDir/MiniNtuples/Rel91/mc16a/Processed/"
basedir_d = "/lustre/scratch/epp/atlas/bs336/WorkDir/MiniNtuples/Rel91/mc16d/Processed/"
basedir_e = "/lustre/scratch/epp/atlas/bs336/WorkDir/MiniNtuples/Rel91/mc16e/Processed/"

sigdir_a = "/lustre/scratch/epp/atlas/bs336/WorkDir/MiniNtuples/Rel91/mc16a_Signal/Processed/"
sigdir_d = "/lustre/scratch/epp/atlas/bs336/WorkDir/MiniNtuples/Rel91/mc16d_Signal/Processed/"
sigdir_e = "/lustre/scratch/epp/atlas/bs336/WorkDir/MiniNtuples/Rel91/mc16e_Signal/Processed/"



merge(
input(name='ttVa', files=basedir_a+'ttV.root', treename='HFntuple', color=ROOT.kCyan-1, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='t#bar{t}V'),
input(name='ttVd', files=basedir_d+'ttV.root', treename='HFntuple', color=ROOT.kCyan-1, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='t#bar{t}V'),
input(name='ttVe', files=basedir_e+'ttV.root', treename='HFntuple', color=ROOT.kCyan-1, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='t#bar{t}V'))
#merge(
#input(name='Vgammaa', files=basedir_a+'Vgamma.root', treename='HFntuple', color='#228b22', scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Vgamma'),
#input(name='Vgammad', files=basedir_d+'Vgamma.root', treename='HFntuple', color='#228b22', scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Vgamma'),
#input(name='Vgammae', files=basedir_d+'Vgamma.root', treename='HFntuple', color='#228b22', scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Vgamma'))

merge(
input(name='ttbara', files=basedir_a+'ttbar.root', treename='HFntuple', color=ROOT.kMagenta-5 , scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Top'),
input(name='ttbard', files=basedir_d+'ttbar.root', treename='HFntuple', color=ROOT.kMagenta-5, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Top'),
input(name='ttbare', files=basedir_e+'ttbar.root', treename='HFntuple', color=ROOT.kMagenta-5, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Top'),
#merge(
input(name='singletopa', files=basedir_a+'SingleT.root', treename='HFntuple', color=ROOT.kMagenta-5, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Top'),
input(name='singletopd', files=basedir_d+'SingleT.root', treename='HFntuple', color=ROOT.kMagenta-5, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Top'),
input(name='singletope', files=basedir_e+'SingleT.root', treename='HFntuple', color=ROOT.kMagenta-5, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Top'))

merge(
input(name='Zjetsa', files=basedir_a+'SherpaZjets.root', treename='HFntuple', color=ROOT.kOrange+9, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Fakes'),
input(name='Zjetsd', files=basedir_d+'SherpaZjets.root', treename='HFntuple', color=ROOT.kOrange+9, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Fakes'),
input(name='Zjetse', files=basedir_e+'SherpaZjets.root', treename='HFntuple', color=ROOT.kOrange+9, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Fakes'))

merge(
input(name='Higgsa', files=basedir_a+'Higgs_3V.root', treename='HFntuple', color=ROOT.kCyan-5, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Higgs/VVV'),
input(name='Higgsd', files=basedir_d+'Higgs_3V.root', treename='HFntuple', color=ROOT.kCyan-5, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Higgs/VVV'),
input(name='Higgse', files=basedir_e+'Higgs_3V.root', treename='HFntuple', color=ROOT.kCyan-5, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Higgs/VVV'),
input(name='VVVa', files=basedir_a+'VVV_222.root', treename='HFntuple', color=ROOT.kCyan-5, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Higgs/VVV'),
input(name='VVVd', files=basedir_d+'VVV_222.root', treename='HFntuple', color=ROOT.kCyan-5, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Higgs/VVV'),
input(name='VVVe', files=basedir_e+'VVV_222.root', treename='HFntuple', color=ROOT.kCyan-5, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Higgs/VVV'))
#, rel_unc={'VR3L-offZ_high':(0.0971,0.0852), 'VR3L-offZ0j':(0.0945,0.1037), 'VR3L-offZ1j':(0.0444,0.0416), 'VR3L-top':(0.0597,0.0460), 'SR3L-DFOS-0j':(0.0603,0.075), 'SR3L-DFOS-1ja':(0.0878,0.09), 'SR3L-DFOS-1jb':(0.1020,0.1163)})
merge(
input(name='diboson2La', files=basedir_a+'Dibosons_2L.root', treename='HFntuple', color=ROOT.kAzure+4, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WW/ZZ'),
input(name='diboson2Ld', files=basedir_d+'Dibosons_2L.root', treename='HFntuple', color=ROOT.kAzure+4, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WW/ZZ'),
input(name='diboson2Le', files=basedir_e+'Dibosons_2L.root', treename='HFntuple', color=ROOT.kAzure+4, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WW/ZZ'),
input(name='diboson4La', files=basedir_a+'Dibosons_4L.root', treename='HFntuple', color=ROOT.kAzure+4, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WW/ZZ'),
input(name='diboson4Ld', files=basedir_d+'Dibosons_4L.root', treename='HFntuple', color=ROOT.kAzure+4, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WW/ZZ'),
input(name='diboson4Le', files=basedir_e+'Dibosons_4L.root', treename='HFntuple', color=ROOT.kAzure+4, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WW/ZZ'))
#, rel_unc={'VR3offZ':0.3, 'VR3Za':0.3, 'VR3Zb':0.3, 'VR3offZb':0.3, 'VR3Za-j0':0.3, 'VR3Za-j1':0.3, 'VR3Zb-j1':0.3, 'Bin1-0Ja':0.3, 'Bin2-0Jb':0.3, 'Bin3-0Jc':0.3, 'Bin4-1Ja':0.3, 'Bin5-1Jb':0.3, 'Bin6-1Jc':0.3, 'Bin1':0.3, 'Bin2':0.3, 'Bin3':0.3, 'Bin4':0.3, 'Bin5':0.3})
# SF HighHT : 0.87 LowHT :0.96
merge(
input(name='diboson3La', files=basedir_a+'Dibosons_3L.root', treename='HFntuple', color=ROOT.kOrange-3, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ'),
input(name='diboson3Ld', files=basedir_d+'Dibosons_3L.root', treename='HFntuple', color=ROOT.kOrange-3, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ'),
input(name='diboson3Le', files=basedir_e+'Dibosons_3L.root', treename='HFntuple', color=ROOT.kOrange-3, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ'))

#Wh
'''

'''

#merge(
#input(name='Signal3a', files=sigdirWZOff_e+'MGPy8EG_C1N2_WZ_200_130_3L_3L3.root', treename='HFntuple', color=ROOT.kOrange-3, scale=lumi, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ (200,130) GeV', isSignal=True)

#input(name='Signal4a', files=sigdirWZOff_e+'MGPy8EG_C1N2_WZ_250_170_3L_3L3.root', treename='HFntuple', color=ROOT.kRed-3, scale=lumi, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ (250,170) GeV', isSignal=True)


merge(
input(name='Signal3a', files=sigdir_a+'MGPy8EG_C1N2_WZ_250_150_3L_2L7.root', treename='HFntuple', color=ROOT.kOrange+3, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ (250,150)', isSignal=True),
input(name='Signal3d', files=sigdir_d+'MGPy8EG_C1N2_WZ_250_150_3L_2L7.root', treename='HFntuple', color=ROOT.kOrange+3, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ (250,150)', isSignal=True),
input(name='Signal3e', files=sigdir_e+'MGPy8EG_C1N2_WZ_250_150_3L_2L7.root', treename='HFntuple', color=ROOT.kOrange+3, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ (250,150)', isSignal=True)
)

merge(
input(name='Signal4a', files=sigdir_a+'MGPy8EG_C1N2_WZ_600_100_3L_2L7.root', treename='HFntuple', color=ROOT.kRed, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ (600,100)', isSignal=True),
input(name='Signal4d', files=sigdir_d+'MGPy8EG_C1N2_WZ_600_100_3L_2L7.root', treename='HFntuple', color=ROOT.kRed, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ (600,100)', isSignal=True),
input(name='Signal4e', files=sigdir_e+'MGPy8EG_C1N2_WZ_600_100_3L_2L7.root', treename='HFntuple', color=ROOT.kRed, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ (600,100)', isSignal=True)
)

merge(
input(name='SignalZ5a', files=sigdir_a+'MGPy8EG_C1N2_WZ_400_200_3L_2L7.root', treename='HFntuple', color=ROOT.kGreen-2, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ (400,200)', isSignal=True),
input(name='SignalZ5d', files=sigdir_d+'MGPy8EG_C1N2_WZ_400_200_3L_2L7.root', treename='HFntuple', color=ROOT.kGreen-2, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ (400,200)', isSignal=True),
input(name='SignalZ5e', files=sigdir_e+'MGPy8EG_C1N2_WZ_400_200_3L_2L7.root', treename='HFntuple', color=ROOT.kGreen-2, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ (400,200)', isSignal=True)
)
'''
merge(
input(name='Signal1a', files=sigdir_a+'MGPy8EG_C1N2_Wh_200_25_3L7.root', treename='HFntuple', color=ROOT.kMagenta+3, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Wh (200,25) GeV', isSignal=True),
input(name='Signal1d', files=sigdir_d+'MGPy8EG_C1N2_Wh_200_25_3L7.root', treename='HFntuple', color=ROOT.kMagenta+3, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Wh (200,25) GeV', isSignal=True),
input(name='Signal1e', files=sigdir_e+'MGPy8EG_C1N2_Wh_200_25_3L7.root', treename='HFntuple', color=ROOT.kMagenta+3, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Wh (200,25) GeV', isSignal=True))


merge(
input(name='Signal2a', files=sigdir_a+'MGPy8EG_C1N2_Wh_175_25_3L7.root', treename='HFntuple', color=ROOT.kGreen-3, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Wh (175,25) GeV', isSignal=True),
input(name='Signal2d', files=sigdir_d+'MGPy8EG_C1N2_Wh_175_25_3L7.root', treename='HFntuple', color=ROOT.kGreen-3, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Wh (175,25) GeV', isSignal=True),
input(name='Signal2e', files=sigdir_e+'MGPy8EG_C1N2_Wh_175_25_3L7.root', treename='HFntuple', color=ROOT.kGreen-3, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Wh (175,25) GeV', isSignal=True))


merge(
input(name='Signal5a', files=sigdirWZ_a+'MGPy8EG_C1pN2_WZ_250_170_3L_3L3.root', treename='HFntuple', color=ROOT.kRed-3, scale=sig_lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ (250,170) GeV', isSignal=True),
input(name='Signal5d', files=sigdirWZ_d+'MGPy8EG_C1pN2_WZ_250_170_3L_3L3.root', treename='HFntuple', color=ROOT.kRed-3, scale=sig_lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ (250,170) GeV', isSignal=True),
input(name='Signal5e', files=sigdirWZ_e+'MGPy8EG_C1pN2_WZ_250_170_3L_3L3.root', treename='HFntuple', color=ROOT.kRed-3, scale=sig_lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='WZ (250,170) GeV', isSignal=True)
)

'''
