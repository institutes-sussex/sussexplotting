from config.regions import region


region('SS_WP60',cuts=[
            ('passSS3LTrigMatch_noMET',),
            ('pass2lcut', 1),
            ('nBjet_WP60', 0),
            ('isSS', 1),
#            ('isMM', 1),
            ('CFpass_L', 1),
            ('eT_miss', 50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
                      ],
)

region('SS_WP70',cuts=[
            ('passSS3LTrigMatch_noMET',),
            ('pass2lcut', 1),
            ('nBjet_WP70', 0),
            ('isSS', 1),
 #           ('isMM', 1),
            ('CFpass_L', 1),
            ('eT_miss', 50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
                      ],
)


region('SS_WP77',cuts=[
            ('passSS3LTrigMatch_noMET',),
            ('pass2lcut', 1),
            ('nBjet_WP77', 0),
            ('isSS', 1),
 #           ('isMM', 1),
            ('CFpass_L', 1),
            ('eT_miss', 50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
                      ],
)

region('SS_WP85',cuts=[
            ('passSS3LTrigMatch_noMET',),
            ('pass2lcut', 1),
            ('nBjet_WP85', 0),
            ('isSS', 1),
 #           ('isMM', 1),
            ('CFpass_L', 1),
            ('eT_miss', 50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
                      ],
)

