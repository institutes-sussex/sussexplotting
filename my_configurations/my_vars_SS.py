from config.vars import variable





variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(15, 50, 350))
variable(name='mt2', unit='GeV', label='m_{T2}', bins=(20,0, 200))
#variable(name='ht', unit='GeV', label='H_{T}', bins=(15,0,450))
variable(name='nJets', unit='', label='nJets', bins=(10, 0, 10),reverse_zn='true')
#variable(name='ElPt', unit='GeV', label='p_{T}^{e}', bins=(15,25, 200))
#variable(name='mll3lZ', unit='GeV', label='m_{ll}', bins=(20, 0, 300))
#variable(name='JetPt0',branch='JetPt[0]', unit='GeV', label='1st Jet p_{T} ', bins=(19,20, 400))

#variable(name='mlll', branch ='mlll', unit='GeV', label='m_{lll}', bins=(20, 100, 200))
#variable(name='JetPt1',branch='JetPt[1]', unit='GeV', label='2nd Jet p_{T} ', bins=(10,20, 270))
#variable(name='HTLep', branch='LepPt[0]+LepPt[1]+LepPt[2]',unit='GeV', label='H_{T}^{Lep}', bins=(15, 50, 400))
#variable(name='nJet_Rebins', branch='nJets', unit='', label='nJets', bins=(5, 0, 5))



#variable(name='num_bjets', unit='', label='nbJets', bins=(10, 0, 10))

#variable(name='num_bjets', unit='', label='nbJets', bins=(2, 0, 2))

#variable(name='mTWZ', unit='GeV', label='m_{T}', bins=(8, 20, 100))
#variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(10, 100, 250))

#variable(name='RenHTLep', branch='LepPt[0]+LepPt[1]+LepPt[2]',unit='GeV', label='H_{T}^{Lep}', bins=(15, 50, 400))


