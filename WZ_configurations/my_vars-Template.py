from config.vars import variable

# Variables to plot
# reverse_zn=true will reverse the Zn curves in N-1 plots in order to look at
# upper cuts instead of lower cuts
# Branch can be specified independent of plot name, so that upper and lower
# plots can be run together
variable(name='mll3lZmin', unit='GeV', label='m_{SFOS}^{min}', bins=(20, 1.2, 201.2))
variable(name='mSFOS', branch='mll3lZmin', unit='GeV', label='m_{SFOS}^{min}', bins=(20, 1.2, 201.2),reverse_zn='true')

variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(35, 0, 350))

variable(name='LepPt2', branch='LepPt[2]',unit='GeV', label='p_{T}^{l_{3}}', bins=(32,0, 160))
variable(name='LepPt_2', branch='LepPt[2]',unit='GeV', label='p_{T}^{l_{3}}', bins=(32,0, 160),reverse_zn='true')
