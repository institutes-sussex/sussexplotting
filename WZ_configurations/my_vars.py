from config.vars import variable




#variable(name='ElEta2', branch='ElEta[2]', unit='GeV', label='3rd Electron  #eta', bins=(12,-2.5, 2.5))#,reverse_zn='true')                                                                                                                  
#variable(name='MuEta2', branch='MuEta[2]', unit='GeV', label='3rd Muon #eta', bins=(12,-2.5, 2.5))#,reverse_zn='true'  
#variable(name='averageIntPerXing', unit='', label='#mu', bins=(100, 0, 100))#
#variable(name='actualIntPerXing', unit='', label='#mu', bins=(100, 0, 100))#
#variable(name='LepPt[0]', unit='GeV', label='p_{T}^{l_{1}}', bins=(15,25, 325))#,reverse_zn='true')
#variable(name='LepPt[1]', unit='GeV', label='p_{T}^{l_{2}}', bins=(13,20, 280))#,reverse_zn='true')
#variable(name='LepPt[2]', unit='GeV', label='p_{T}^{l_{3}}', bins=(8,10, 170))
#variable(name='nJets', unit='', label='nJets', bins=(9, 1, 10))
#variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(10, 50, 200))
#variable(name='MET_Phi', branch='atan(MET_py/MET_px)', unit='', label='E_{T}^{miss} #phi', bins=(30, -2, 2))

#variable(name='met_Sig', unit='', label='Significance', bins=(20, 0, 10))
#variable(name='mll3lZ', unit='GeV', label='m_{ll}', bins=(40, 0, 200))
#variable(name='mTWZ', unit='GeV', label='m_{T}', bins=(40, 0, 200))
#variable(name='mlll', branch ='mlll', unit='GeV', label='m_{3l}', bins=(10, 100, 300))
variable(name='mTWZ', unit='GeV', label='m_{T}', bins=(8, 20, 100))
#variable(name='nJets', unit='', label='nJets', bins=(10, -0.5, 9.5))
#variable(name='ht', unit='GeV', label='H_{T}', bins=(15,0,350))   
#variable(name='dRnear', unit='', label='#DeltaR_{OS,near}', bins=(10,0, 5))
#variable(name='MET', branch='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(10, 50, 400))

variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(5, 50, 100))
variable(name='MET', branch='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(5, 50, 100))
#variable(name='mTWZ', unit='GeV', label='m_{T}', bins=(8, 20, 100))
#variable(name='HTLep', branch='LepPt[0]+LepPt[1]+LepPt[2]',unit='GeV', label='H_{T}^{Lep}', bins=(15, 50, 350))
'''
#variable(name='ElPt0', branch='ElPt[0]', unit='GeV', label='1st Electron p_{T}', bins=(8,20, 80))#,reverse_zn='true')#only for emm VR Fakes
variable(name='ElPt0', branch='ElPt[0]', unit='GeV', label='1st Electron p_{T}', bins=(12,20, 180))#,reverse_zn='true')
#variable(name='nJets', unit='', label='nJets', bins=(10, -0.5, 9.5))

variable(name='MuPt0', branch='MuPt[0]', unit='GeV', label='1st Muonp_{T}', bins=(12,20, 180))#,reverse_zn='true')
variable(name='ElPt1', branch='ElPt[1]', unit='GeV', label='2nd Electron p_{T}', bins=(8,20, 100))#,reverse_zn='true')
variable(name='MuPt1', branch='MuPt[1]', unit='GeV', label='2nd Muon p_{T}', bins=(8,20, 100))#,reverse_zn='true')
variable(name='ElPt2', branch='ElPt[2]', unit='GeV', label='3rd Electron p_{T}', bins=(8,10, 100))#,reverse_zn='true')
variable(name='MuPt2', branch='MuPt[2]', unit='GeV', label='3rd Muon p_{T}', bins=(8,10, 100))#,reverse_zn='true'

variable(name='ElEta0', branch='ElEta[0]', unit='GeV', label='1st Electron #eta', bins=(12,-2.5, 2.5))#,reverse_zn='true')
variable(name='MuEta0', branch='MuEta[0]', unit='GeV', label='1st Muonp #eta', bins=(12,-2.5, 2.5))#,reverse_zn='true')
variable(name='ElEta1', branch='ElEta[1]', unit='GeV', label='2nd Electron #eta', bins=(10,-2.5, 2.5))#,reverse_zn='true')
variable(name='MuEta1', branch='MuEta[1]', unit='GeV', label='2nd Muon  #eta', bins=(10,-2.5, 2.5))#,reverse_zn='true')
variable(name='ElEta2', branch='ElEta[2]', unit='GeV', label='3rd Electron  #eta', bins=(10,-2.5, 2.5))#,reverse_zn='true')
variable(name='MuEta2', branch='MuEta[2]', unit='GeV', label='3rd Muon #eta', bins=(10,-2.5, 2.5))#,reverse_zn='true'
variable(name='met_Sig', unit='', label='Significance', bins=(10, 0, 10))
variable(name='ht', unit='GeV', label='H_{T}', bins=(15,0,350))
variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(10, 50, 100))
'''
#variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(10, 50, 100))
#variable(name='ht', unit='GeV', label='H_{T}', bins=(15,0,350))
#variable(name='dRnear', unit='', label='#DeltaR_{OS,near}', bins=(10,0, 5))
#variable(name='LepPt0', branch='LepPt[0]', unit='GeV', label='1st Lepton p_{T}', bins=(12,25, 200))#,reverse_zn='true')
#variable(name='LepPt1', branch='LepPt[1]', unit='GeV', label='2nd Lepton p_{T}', bins=(8,20, 100))#,reverse_zn='true')
#variable(name='LepPt2', branch='LepPt[2]', unit='GeV', label='3rd Lepton p_{T}', bins=(8,10, 100))#,reverse_zn='true')

#variable(name='mlll', branch ='mlll', unit='GeV', label='m_{3l}', bins=(10, 100, 300))
'''
#variable(name='mll3lZ', unit='GeV', label='m_{ll}', bins=(20, 0, 300))

#variable(name='mlll', branch ='mlll', unit='GeV', label='m_{3l}', bins=(10, 100, 300))
#variable(name='mTWZ', unit='GeV', label='m_{T}', bins=(4, 0, 20))

variable(name='mTWZ',branch='mTWZ', unit='GeV', label='m_{T}', bins=(12, 0, 100))
variable(name='LepPt0', branch='LepPt[0]', unit='GeV', label='1st Lepton p_{T}', bins=(12,25, 200))#,reverse_zn='true')
variable(name='LepPt1', branch='LepPt[1]', unit='GeV', label='2nd Lepton p_{T}', bins=(8,20, 100))#,reverse_zn='true')                                                                                     
variable(name='LepPt2', branch='LepPt[2]', unit='GeV', label='3rd Lepton p_{T}', bins=(8,10, 100))#,reverse_zn='true') 
variable(name='JetPt0',branch='JetPt[0]', unit='GeV', label='1st Jet p_{T} ', bins=(19,20, 400)) 
variable(name='JetPt1',branch='JetPt[1]', unit='GeV', label='2nd Jet p_{T} ', bins=(10,20, 270)) 

variable(name='met_Sig', unit='', label='Significance', bins=(12, 0, 12))
#variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(10, 50, 100))
#variable(name='MuPt', unit='GeV', label='p_{T}^{#mu}', bins=(15,25, 200))
#variable(name='ht', unit='GeV', label='H_{T}', bins=(15,0,450))
#variable(name='nJets', unit='', label='nJets', bins=(10, 0, 10))
#variable(name='ElPt', unit='GeV', label='p_{T}^{e}', bins=(15,25, 200))
#variable(name='mll3lZ', unit='GeV', label='m_{ll}', bins=(20, 0, 300))
#variable(name='JetPt0',branch='JetPt[0]', unit='GeV', label='1st Jet p_{T} ', bins=(19,20, 400))

#variable(name='mlll', branch ='mlll', unit='GeV', label='m_{lll}', bins=(20, 100, 200))
#variable(name='JetPt1',branch='JetPt[1]', unit='GeV', label='2nd Jet p_{T} ', bins=(10,20, 270))

#variable(name='nJet_Rebins', branch='nJets', unit='', label='nJets', bins=(5, 0, 5))



#variable(name='num_bjets', unit='', label='nbJets', bins=(10, 0, 10))

#variable(name='num_bjets', unit='', label='nbJets', bins=(2, 0, 2))


#variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(10, 100, 250))

#variable(name='RenHTLep', branch='LepPt[0]+LepPt[1]+LepPt[2]',unit='GeV', label='H_{T}^{Lep}', bins=(15, 50, 400))


'''
