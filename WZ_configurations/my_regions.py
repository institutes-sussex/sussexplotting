from config.regions import region


region('CR',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('Cleaning', 1),
        ('nCombLep', 3),
        ('num_bjets', 0),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, None),
        ('mTWZ', 0, 100),
        ('mTWZ', 20, 100),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None)
        ],
)



region('WZ-CR-nJ0',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('nCombLep', 3),
        ('Cleaning', 1),
        ('num_bjets', 0),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, 100),
        ('mTWZ', 20, 100),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('nJets', 0)
        ],
)




region('WZ-VR-nJ0',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('Cleaning', 1),
        ('nCombLep', 3),
        ('num_bjets', 0),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 100, None),
        ('mTWZ', 20, 100),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None),
        ('nJets', 0)
        ],
)



region('WZ-CR-HighHT',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('Cleaning', 1),
        ('nCombLep', 3),
        ('nJets',1,5),
        ('num_bjets', 0),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, 100),
#        ('mTWZ', 0, 100),
        ('mTWZ', 20, 100),
        ('ht',200, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None)
        ],
)

region('WZ-VR-HighHT',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('Cleaning', 1),
        ('nCombLep', 3),
        ('nJets',1,5),
        ('num_bjets', 0),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 100, None),
#        ('mTWZ', 0, 100),
        ('mTWZ', 20, 100),
        ('ht',200, None),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None)
        ],
)


region('WZ-CR-LowHT',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('Cleaning', 1),
        ('nCombLep', 3),
        ('nJets',1,None),
        ('num_bjets', 0),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 50, 100),
#        ('mTWZ', 0, 100),
        ('mTWZ', 20, 100),
        ('ht',20, 200),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None)
        ],
)

region('WZ-VR-LowHT',cuts=[
        ('PassDiLepTriggerMatch',),
        ('pass3lcut', 1),
        ('pass2lossf', 1),
        ('Cleaning', 1),
        ('nCombLep', 3),
        ('nJets',1,None),
        ('num_bjets', 0),
        ('mll3lZ', 75, 105),
        ('fabs(mlll-91.2)',15, None),
        ('eT_miss', 100, None),
#        ('mTWZ', 0, 100),
        ('mTWZ', 20, 100),
        ('ht',20, 200),
        ('LepPt[0]', 25, None),
        ('LepPt[1]', 20, None)
        ],
)

