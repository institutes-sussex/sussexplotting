from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np

#fakes_rel = sqrt(0.30**2+0.15**2)
fakes_rel = 1.0
rel_unc = 0.3
lumi_a = 1.
lumi_d = 1.
lumi_e = 1.

lumi = 1.


basedir_a = "/mnt/lustre/projects/epp/general/atlas/SUSYEWK/3LAna/Rel91/mc16a/Processed/"
basedir_d = "/mnt/lustre/projects/epp/general/atlas/SUSYEWK/3LAna/Rel91/mc16d/Processed/"
basedir_e = "/mnt/lustre/projects/epp/general/atlas/SUSYEWK/3LAna/Rel91/mc16e/Processed/"


datadir = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/3LAna/Rel91/Data/'
#fakedir = '/lustre/scratch/epp/atlas/ft81/workdir/scripts/addVar/'
#   "ZZ" : ROOT.kAzure-2,
#    "ttbar" : ROOT.kOrange+1,
#    "WZ0j" : ROOT.kAzure+1,
#    "WZnj" : ROOT.kAzure+1,
#    "Others" : ROOT.kPink+1,
#    "Fakes" : ROOT.kGray,
#    "ttX" : ROOT.kOrange-4,


merge(
input(name='Data1516', files=datadir+'data_2016.root', treename='HFntuple', color=ROOT.kBlack, scale=1., weights='EventWeight', label='Data', isData=True),
input(name='Data17', files=datadir+'data_2017.root', treename='HFntuple', color=ROOT.kBlack, scale=1., weights='EventWeight', label='Data', isData=True),
input(name='Data18', files=datadir+'data_2018.root', treename='HFntuple', color=ROOT.kBlack, scale=1., weights='EventWeight', label='Data', isData=True))





'''
merge(
input(name='Zjetsa', files=basedir_a+'SherpaZjets.root', treename='HFntuple', color=ROOT.kGray, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='ZJets/Z#gamma'),
input(name='Zjetsd', files=basedir_d+'SherpaZjets.root', treename='HFntuple', color=ROOT.kGray, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='ZJets/Z#gamma'),
input(name='Zjetse', files=basedir_e+'SherpaZjets.root', treename='HFntuple', color=ROOT.kGray, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='ZJets/Z#gamma'))
'''
merge(
input(name='diboson2La', files=basedir_a+'Dibosons_2L.root', treename='HFntuple', color=ROOT.kPink+1, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Others'),
input(name='diboson2Ld', files=basedir_d+'Dibosons_2L.root', treename='HFntuple', color=ROOT.kPink+1, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Others'),
input(name='diboson2Le', files=basedir_e+'Dibosons_2L.root', treename='HFntuple', color=ROOT.kPink+1, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Others'),
input(name='singletopa', files=basedir_a+'SingleT.root', treename='HFntuple', color=ROOT.kPink+1, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Others'),
input(name='singletopd', files=basedir_d+'SingleT.root', treename='HFntuple', color=ROOT.kPink+1, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Others'),
input(name='singletope', files=basedir_e+'SingleT.root', treename='HFntuple', color=ROOT.kPink+1, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Others'),
input(name='Higgsa', files=basedir_a+'Higgs_3V.root', treename='HFntuple', color=ROOT.kMagenta-5, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Others'),
input(name='Higgsd', files=basedir_d+'Higgs_3V.root', treename='HFntuple', color=ROOT.kMagenta-5, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Others'),
input(name='Higgse', files=basedir_e+'Higgs_3V.root', treename='HFntuple', color=ROOT.kMagenta-5, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Others'),
input(name='VVVa', files=basedir_a+'VVV_222.root', treename='HFntuple', color=ROOT.kCyan-5, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Others'),
input(name='VVVd', files=basedir_d+'VVV_222.root', treename='HFntuple', color=ROOT.kCyan-5, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Others'),
input(name='VVVe', files=basedir_e+'VVV_222.root', treename='HFntuple', color=ROOT.kCyan-5, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='Others'))

merge(
input(name='ttVa', files=basedir_a+'ttV.root', treename='HFntuple', color=ROOT.kOrange-4, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='t#bar{t}+X'),
input(name='ttVd', files=basedir_d+'ttV.root', treename='HFntuple', color=ROOT.kOrange-4, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='t#bar{t}+X'),
input(name='ttVe', files=basedir_e+'ttV.root', treename='HFntuple', color=ROOT.kOrange-4, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='t#bar{t}+X'))


merge(                                                                                                                                                               
input(name='Zjetsa', files=basedir_a+'SherpaZjets.root', treename='HFntuple', color=ROOT.kGray, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='ZJets'), 
input(name='Zjetsd', files=basedir_d+'SherpaZjets.root', treename='HFntuple', color=ROOT.kGray, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='ZJets'),
input(name='Zjetse', files=basedir_e+'SherpaZjets.root', treename='HFntuple', color=ROOT.kGray, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='ZJets'))

#input(name='fake', files=fakedir+'fakes_eRJRsetup_revMt_noNeg_20met50_var_final_comb_mt20.root', treename='HFntuple', color=ROOT.kGray, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF*FFWeight', label='Z+Jets',rel_unc={'SFOS':0.5},force_positive=True)
#input(name='fake', files=fakedir+'fakes_eRJRsetup_revMt_noNeg_20met50_var_final_comb_mt20.root', treename='HFntuple', color=ROOT.kGray, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF*FFWeight', label='Z+Jets',force_positive=True)


merge(
input(name='ttbara', files=basedir_a+'ttbar.root', treename='HFntuple', color=ROOT.kOrange+1 , scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF',label='t#bar{t}'),
input(name='ttbard', files=basedir_d+'ttbar.root', treename='HFntuple', color=ROOT.kOrange+1, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='t#bar{t}'),
input(name='ttbare', files=basedir_e+'ttbar.root', treename='HFntuple', color=ROOT.kOrange+1, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='t#bar{t}'))


merge(
input(name='diboson4La', files=basedir_a+'Dibosons_4L.root', treename='HFntuple', color=ROOT.kAzure-2, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='ZZ'),
input(name='diboson4Ld', files=basedir_d+'Dibosons_4L.root', treename='HFntuple', color=ROOT.kAzure-2, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='ZZ'),
input(name='diboson4Le', files=basedir_e+'Dibosons_4L.root', treename='HFntuple', color=ROOT.kAzure-2, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF', label='ZZ'))
#, rel_unc={'VR3offZ':0.3, 'VR3Za':0.3, 'VR3Zb':0.3, 'VR3offZb':0.3, 'VR3Za-j0':0.3, 'VR3Za-j1':0.3, 'VR3Zb-j1':0.3, 'Bin1-0Ja':0.3, 'Bin2-0Jb':0.3, 'Bin3-0Jc':0.3, 'Bin4-1Ja':0.3, 'Bin5-1Jb':0.3, 'Bin6-1Jc':0.3, 'Bin1':0.3, 'Bin2':0.3, 'Bin3':0.3, 'Bin4':0.3, 'Bin5':0.3})
# SF HighHT : 0.87 LowHT :0.

merge(
    input(name='diboson3La', files=basedir_a+'Dibosons_3L.root', treename='HFntuple', color=ROOT.kAzure+1, scale=lumi_a, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF*1.07', label='WZ'),
    input(name='diboson3Ld', files=basedir_d+'Dibosons_3L.root', treename='HFntuple', color=ROOT.kAzure+1, scale=lumi_d, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF*1.07', label='WZ'),
    input(name='diboson3Le', files=basedir_e+'Dibosons_3L.root', treename='HFntuple', color=ROOT.kAzure+1, scale=lumi_e, weights='pileupweight*btagSFCentral*jvtSF*EventWeight*XSecWeight*elecSF*muonSF*1.07', label='WZ')
)




