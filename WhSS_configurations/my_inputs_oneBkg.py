from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np

lumiA = 36207.66
lumiD = 44304.7
#lumiE = 58450.1
#lumiE = 1.
lumiE = 138962.46

# **For histo buffering reasons, if the root file name changes, the name label
# must also change, otherwise it will throw a mismatch error**


# Define directory paths
basedirA = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16a/bkg/'
basedirD = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16d/bkg/'
basedirE = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120/mc16e_merged_withxsec_skimmed/'

sigdirA  = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16a/Wh_signal/'
sigdirD  = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16d/Wh_signal/'
sigdirE  = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120/mc16e_sig_merged_withxsec_skimmed/'

fakedirA = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16a/fakes/'
fakedirD = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16d/fakes/'
fakedirE = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16e/fakes/'

datadir = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120/data18_merged_skimmed/'

# Define weights
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*btagSFCentral*chargeFlipSF*lumi'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*bjetSF_WP85'
basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*bjetSF_WP85*((isEE/1114.11)+(isEM/1980.17)+(isMM/1345.62))'
#basesel ='lumiXsecWgt*TotalWeight'
fakesel ='FakeWeight'
#sigsel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*btagSFCentral*chargeFlipSF*lumi'
sigsel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*bjetSF_WP85'
#sigsel ='lumiXsecWgt*TotalWeight'
datasel ='EventWeight'


# Include data
#input(name='Data18',  files=datadir+'data18.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True)

# Include fake ntuple
# force_positive prevents negative fakes substracting from the total SM
#input(name='Fakes', files=basedir+'Fakes_MM_new.root',      treename='HFntuple',       color=ROOT.kOrange-2,   scale=36.1, weights=fakesel,  label='Reducible', force_positive=True)

merge(
input(name='singleTop_E',    files=basedirE+'SingleTop.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='All SM', force_positive=True),
input(name='VVV_E',    files=basedirE+'VVV.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='All SM', force_positive=True),
input(name='ttV_E',    files=basedirE+'ttV.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='All SM', force_positive=True),
input(name='ttbar_E',    files=basedirE+'ttbar.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='All SM', force_positive=True),
#input(name='Vgamma_E',    files=basedirE+'Vgamma.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='All SM', force_positive=True),
input(name='Wjets_E',    files=basedirE+'WJets.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='All SM', force_positive=True),
input(name='VV_1L_E',    files=basedirE+'VV_1L.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='All SM', force_positive=True),
input(name='VV_2L_E',    files=basedirE+'VV_2L.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='All SM', force_positive=True),
input(name='VV_3L_E',    files=basedirE+'VV_3L.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='All SM', force_positive=True),
input(name='VV_4L_E',    files=basedirE+'VV_4L.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='All SM', force_positive=True),
input(name='Zjets_ee_E',			files=basedirE+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kGray, scale=lumiE,   weights=basesel,  label='All SM', force_positive=True),
input(name='Zjets_mm_E',			files=basedirE+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kGray, scale=lumiE,   weights=basesel,  label='All SM', force_positive=True),
input(name='Zjets_tt_E',			files=basedirE+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kGray, scale=lumiE,   weights=basesel,  label='All SM', force_positive=True)
)

sigsel1 ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*bjetSF_WP85*((isEE/41.26)+(isEM/99.71)+(isMM/84.89))'
sigsel2 ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*bjetSF_WP85*((isEE/41.24)+(isEM/118.33)+(isMM/82.64))'
sigsel3 ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*bjetSF_WP85*((isEE/40.43)+(isEM/122.49)+(isMM/90.09))'

# include signal
input(name='sig_202p5_72p5',    files=sigdirE+'user.safarzad.396647.MGPy8EG_A14N23LO_C1N2_Wh_hall_202p5_72p5_2L7.SUSY2.mc16e.p3990.SSV0.07May20_HFntuple.root',     treename='HFntuple',    color=ROOT.kBlue,      linestyle=5,    scale=lumiE, weights=sigsel1,    label='Wh(202.5,72.5)',   isSignal=True)
input(name='sig_200p0_50p0',    files=sigdirE+'user.safarzad.396646.MGPy8EG_A14N23LO_C1N2_Wh_hall_200p0_50p0_2L7.SUSY2.mc16e.p3990.SSV0.07May20_HFntuple.root',     treename='HFntuple',    color=ROOT.kGreen+2,      linestyle=5,    scale=lumiE, weights=sigsel3,    label='Wh(200,50)',   isSignal=True)
input(name='sig_200p0_25p0',    files=sigdirE+'user.safarzad.396645.MGPy8EG_A14N23LO_C1N2_Wh_hall_200p0_25p0_2L7.SUSY2.mc16e.p3990.SSV0.07May20_HFntuple.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel2,    label='Wh(200,25)',   isSignal=True)
#input(name='sig_200p0_0p0',    files=sigdirE+'user.safarzad.396644.MGPy8EG_A14N23LO_C1N2_Wh_hall_200p0_0p0_2L7.SUSY2.mc16e.p3990.SSV0.07May20_HFntuple.root',     treename='HFntuple',    color=ROOT.kMagenta+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(200,0)',   isSignal=True)
