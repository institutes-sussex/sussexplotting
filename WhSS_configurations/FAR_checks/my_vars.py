from config.vars import variable

# Variables to plot
# reverse_zn=true will reverse the Zn curves in N-1 plots in order to look at
# upper cuts instead of lower cuts
# Branch can be specified independent of plot name, so that upper and lower
# plots can be run together

variable(name='PileUp', branch='averageIntPerXing', unit='', label='<#mu>', bins=(20, 0, 80))

variable(name='MET', branch='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(30, 0, 300))
variable(name='METphi', branch='atan2(MET_py,MET_px)', unit='', label='E_{T}^{miss} #phi', bins=(20, -5, 5))
variable(name='METsig', branch='met_Sig', unit='', label='E_{T}^{miss} Significance', bins=(20, 0, 20))

variable(name='mt2', unit='GeV', label='m_{T2}', bins=(15, 0, 300))
variable(name='mTmin', branch='min(mTl1,mTl2)', unit='GeV', label='m_{T}^{min}', bins=(15, 0, 300))
variable(name='mjj', unit='GeV', label='m_{jj}', bins=(40, 0, 2000))

variable(name='LepPt0', branch='LepPt[0]', unit='GeV', label='Leading lepton p_{T}', bins=(20, 0, 500))
variable(name='LepEta0', branch='LepEta[0]', unit='', label='Leading lepton #eta', bins=(20, -5, 5))
variable(name='LepPhi0', branch='LepPhi[0]', unit='', label='Leading lepton #phi', bins=(20, -5, 5))

variable(name='JetPt0', branch='JetPt[0]', unit='GeV', label='Leading jet p_{T}', bins=(20, 0, 500))
variable(name='JetEta0', branch='JetEta[0]', unit='', label='Leading jet #eta', bins=(20, -5, 5))
variable(name='JetPhi0', branch='JetPhi[0]', unit='', label='Leading jet #phi', bins=(20, -5, 5))
