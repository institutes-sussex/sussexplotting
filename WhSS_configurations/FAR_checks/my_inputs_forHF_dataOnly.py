from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np

#lumiA = 36207.66
#lumiD = 44304.7
#lumiE = 58450.1
#lumiE = 138962.46

lumiA = 1.
lumiD = 1.
lumiE = 1.

# **For histo buffering reasons, if the root file name changes, the name label
# must also change, otherwise it will throw a mismatch error**


# Define directory paths
basedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_a/'
basedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_d/'
basedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_e/'

sigdirA  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16a_sig_merged_withxsec_skimmed/'
sigdirD  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16d_sig_merged_withxsec_skimmed/'
sigdirE  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16e_sig_merged_withxsec_skimmed/'

fakedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_a/'
fakedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_d/'
fakedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_e/'

# Define weights
basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(isPromptMatched==1)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(isPromptMatched==0)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(EventTruthType!=2)'
fakesel ='FFWeight'
cfsel ='CFWeight'
sigsel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral'
datasel ='EventWeight'

# Add-ons for bySouce grouping
# these will work for a 2lep region and for signal leptons
isPrompt='*(EventTruthType==1)'
isPrPc='*(EventTruthType==1 || EventTruthType==3)'
isCF='*(EventTruthType==2)'
isPhConv='*(EventTruthType==3)'
isFake='*(EventTruthType==0 || EventTruthType==4 || EventTruthType==5 || EventTruthType==6)' # all including Tau, ElFromMu and (known)Unknown
isFakeLF='*(EventTruthType==0 || EventTruthType==5 || EventTruthType==6)' # LF including Tau, ElFromMu and (known)Unknown
isFakeHF='*(EventTruthType==4)' # HF only

# Include data
merge(
input(name='Data16',  files=basedirA+'data16.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True),
input(name='Data17',  files=basedirD+'data17.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True),
input(name='Data18',  files=basedirE+'data18.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True)
)

# Include data as histo
merge(
input(name='Data16_stack',  files=basedirA+'data16.root',      treename='HFntuple',    color=ROOT.kYellow,      scale=1.,   weights=datasel,  label='Data'),
input(name='Data17_stack',  files=basedirD+'data17.root',      treename='HFntuple',    color=ROOT.kYellow,      scale=1.,   weights=datasel,  label='Data'),
input(name='Data18_stack',  files=basedirE+'data18.root',      treename='HFntuple',    color=ROOT.kYellow,      scale=1.,   weights=datasel,  label='Data')
)
