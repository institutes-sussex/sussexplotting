from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)

mu_cut_l=0
mu_cut_u=None

region('CR_WWSS_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('isEE', 1),
            ('num_bjets', 0),
						('nJets',  2, None),
						('eT_miss',  50, None),
						('fabs(mll-91.18)', 15, None),
						('mll', 20, None),
						('mjj',  350, None),
            ('met_Sig', None, 6),
            ('JetPt[1]', 75, None),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ]
)

region('CR_WWSS_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('isEM', 1),
            ('num_bjets', 0),
						('nJets',  2, None),
						('eT_miss',  50, None),
						('mll', 20, None),
						('mjj',  350, None),
            ('met_Sig', None, 6),
            ('JetPt[1]', 75, None),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ]
)

region('CR_WWSS_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('isMM', 1),
            ('num_bjets', 0),
						('nJets',  2, None),
						('eT_miss',  50, None),
						('mll', 20, None),
						('mjj',  350, None),
            ('met_Sig', None, 6),
            ('JetPt[1]', 75, None),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ]
)

region('VR_WWSS_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('isEE', 1),
            ('num_bjets', 0),
						('nJets',  2, None),
						('eT_miss',  50, None),
						('fabs(mll-91.18)', 15, None),
						('mll', 20, None),
						('mjj',  350, None),
            ('met_Sig', 6, None),
            ('JetPt[1]', 75, None),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ]
)

region('VR_WWSS_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('isEM', 1),
            ('num_bjets', 0),
						('nJets',  2, None),
						('eT_miss',  50, None),
						('mll', 20, None),
						('mjj',  350, None),
            ('met_Sig', 6, None),
            ('JetPt[1]', 75, None),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ]
)

region('VR_WWSS_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('isMM', 1),
            ('num_bjets', 0),
						('nJets',  2, None),
						('eT_miss',  50, None),
						('mll', 20, None),
						('mjj',  350, None),
            ('met_Sig', 6, None),
            ('JetPt[1]', 75, None),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ]
)
