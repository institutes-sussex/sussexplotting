from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np

#lumiA = 36207.66
#lumiD = 44304.7
#lumiE = 58450.1
#lumiE = 138962.46

lumiA = 1.
lumiD = 1.
lumiE = 1.

# **For histo buffering reasons, if the root file name changes, the name label
# must also change, otherwise it will throw a mismatch error**


# Define directory paths
basedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_a/'
basedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_d/'
basedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_e/'

sigdirA  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16a_sig_merged_withxsec_skimmed/'
sigdirD  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16d_sig_merged_withxsec_skimmed/'
sigdirE  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16e_sig_merged_withxsec_skimmed/'

fakedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_a/'
fakedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_d/'
fakedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_e/'

# Define weights
basesel ='EventWeight*XSecWeight*elecSF*muonSF*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(isPromptMatched==1)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(isPromptMatched==0)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(EventTruthType!=2)'
fakesel ='FFWeight'
cfsel ='CFWeight'
sigsel ='EventWeight*XSecWeight*elecSF*muonSF*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral'
#sigsel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral'
datasel ='EventWeight'

# Add-ons for bySouce grouping
# these will work for a 2lep region and for signal leptons
isPrompt='*(EventTruthType==1)'
isPrPc='*(EventTruthType==1 || EventTruthType==3)'
isCF='*(EventTruthType==2)'
isPhConv='*(EventTruthType==3)'
isFake='*(EventTruthType==0 || EventTruthType==4 || EventTruthType==5 || EventTruthType==6)' # all including Tau, ElFromMu and (known)Unknown
isFakeLF='*(EventTruthType==0 || EventTruthType==5 || EventTruthType==6)' # LF including Tau, ElFromMu and (known)Unknown
isFakeHF='*(EventTruthType==4)' # HF only

# Include data
input(name='Data16',  files=basedirA+'data16.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True)

# force_positive prevents negative fakes substracting from the total SM

# Include fake ntuple

input(name='FakeA',        files=basedirA+'Fake.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=1,       weights=fakesel,  label='Fake/non-prompt')

input(name='ChargeFlipA',   files=basedirA+'ChargeFlip.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel,    label='Charge-flip')

# Grouped backgrounds

# Entire WZ - no bySource splitting
input(name='VV_3L_A',    files=basedirA+'VV_3L.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=lumiE, weights=basesel,    label='WZ') 

# Entire WW-SS - no bySource splitting - 1.5 scale factor applied
#k_WWSS=1.
k_WWSS=1.44  # Sherpa k-factor
input(name='VV_2L_SS_A',    files=basedirA+'VV_2L_SS.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=k_WWSS, weights=basesel,    label='W^{#pm}W^{#pm}') 

# prompt ttV - no bySource splitting
input(name='ttV_A',          files=basedirA+'ttV.root',         treename='HFntuple',    color=ROOT.kCyan-1,  scale=lumiE,    weights=basesel+isPrPc,   label='t#bar{t}+V') 

# Other - grouped - prompt and PhConv only
merge(
input(name='singleTop_pr_A',    files=basedirA+'SingleTop.root',   treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VVV_pr_A',          files=basedirA+'VVV_221.root',     treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='ttbar_pr_A',        files=basedirA+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Wjets_pr_A',        files=basedirA+'WJets.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VV_1L_pr_A',        files=basedirA+'VV_1L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VV_2L_OS_pr_A',     files=basedirA+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VV_4L_pr_A',        files=basedirA+'VV_4L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Zjets_ee_pr_A',     files=basedirA+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Zjets_mm_pr_A',     files=basedirA+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Zjets_tt_pr_A',     files=basedirA+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other') 
)

# include signal

input(name='sig_177p5_47p5_A',    files=sigdirA+'C1N2_Wh_hall_177p5_47p5_2L7.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(177.5,47.5)',   isSignal=True)

input(name='sig_300p0_100p0_A',    files=sigdirA+'C1N2_Wh_hall_300p0_100p0_2L7.root',     treename='HFntuple',    color=ROOT.kGreen+3,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(300,100)',   isSignal=True)

input(name='sig_400p0_0p0_A',    files=sigdirA+'C1N2_Wh_hall_400p0_0p0_2L7.root',     treename='HFntuple',    color=ROOT.kMagenta+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(400,0)',   isSignal=True)
