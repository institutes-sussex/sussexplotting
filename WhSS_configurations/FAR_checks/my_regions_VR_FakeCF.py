from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)

mu_cut_l=0
mu_cut_u=None

#Define VRs
region('VR_Fake_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEE', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
						('mll', 20, None),
            ('mt2', None, 80),
            ('min(mTl1,mTl2)', None, 100),
            ('met_Sig', None, 5),
						('mjj',  None, 350),
						('fabs(mll-91.18)', 15, None),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ]
)

region('VR_Fake_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
						('mll', 20, None),
            ('mt2', None, 80),
            ('min(mTl1,mTl2)', None, 100),
            ('met_Sig', None, 5),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ]
)

region('VR_Fake_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isMM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
						('mll', 20, None),
            ('mt2', None, 80),
            ('min(mTl1,mTl2)', None, 100),
            ('met_Sig', None, 5),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ]
)

region('VR_CF_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEE', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
						('mll', 20, None),
            ('mt2', None, 80),
            ('min(mTl1,mTl2)', None, 100),
            ('met_Sig', None, 5),
						('mjj',  None, 350),
						('fabs(mll-91.18)', None, 15),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ]
)
