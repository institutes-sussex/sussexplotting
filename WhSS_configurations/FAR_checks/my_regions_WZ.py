from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)

mu_cut_l=0
mu_cut_u=None

region('CR_WZ_EE',cuts=[
            ('passSS3LTrig_noMET', 1),
            ('nEl+nMu', 2),
            ('isSS', 1),
            ('isEE', 1),
            ('nCombLep', 3),
            ('nBaseLep', 3),
            ('BL_pass2lossf', 1),
            ('num_bjets', 0),
            ('nJets',  1, None),
            ('mll', 20, None),
            ('eT_miss',  50, None),
            ('fabs(BL_mlll-91.2)', 10, None),
            ('fabs(BL_mll3lZ-91.2)', 0, 15),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('met_Sig', None, 6),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            # all electrons passing ECIDS
            ('Alt$(BL_ECIDS_L_pass[0],1)', 1),
            ('Alt$(BL_ECIDS_L_pass[1],1)', 1),
            ('Alt$(BL_ECIDS_L_pass[2],1)', 1),
            # all electrons |eta|<2
            ('(BL_LepPdgId[0]==11)*fabs(BL_LepEta[0])', None, 2),
            ('(BL_LepPdgId[1]==11)*fabs(BL_LepEta[1])', None, 2),
            ('(BL_LepPdgId[2]==11)*fabs(BL_LepEta[2])', None, 2),
            ]
)

region('CR_WZ_EM',cuts=[
            ('passSS3LTrig_noMET', 1),
            ('nEl+nMu', 2),
            ('isSS', 1),
            ('isEM', 1),
            ('nCombLep', 3),
            ('nBaseLep', 3),
            ('BL_pass2lossf', 1),
            ('num_bjets', 0),
            ('nJets',  1, None),
            ('mll', 20, None),
            ('eT_miss',  50, None),
            ('fabs(BL_mlll-91.2)', 10, None),
            ('fabs(BL_mll3lZ-91.2)', 0, 15),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('met_Sig', None, 6),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            # all electrons passing ECIDS
            ('Alt$(BL_ECIDS_L_pass[0],1)', 1),
            ('Alt$(BL_ECIDS_L_pass[1],1)', 1),
            # all electrons |eta|<2
            ('(BL_LepPdgId[0]==11)*fabs(BL_LepEta[0])', None, 2),
            ('(BL_LepPdgId[1]==11)*fabs(BL_LepEta[1])', None, 2),
            ]
)

region('CR_WZ_MM',cuts=[
            ('passSS3LTrig_noMET', 1),
            ('nEl+nMu', 2),
            ('isSS', 1),
            ('isMM', 1),
            ('nCombLep', 3),
            ('nBaseLep', 3),
            ('BL_pass2lossf', 1),
            ('num_bjets', 0),
            ('nJets',  1, None),
            ('mll', 20, None),
            ('eT_miss',  50, None),
            ('fabs(BL_mlll-91.2)', 10, None),
            ('fabs(BL_mll3lZ-91.2)', 0, 15),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('met_Sig', None, 6),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ]
)

region('VR_WZ_EE',cuts=[
            ('passSS3LTrig_noMET', 1),
            ('nEl+nMu', 2),
            ('isSS', 1),
            ('isEE', 1),
            ('nCombLep', 3),
            ('nBaseLep', 3),
            ('BL_pass2lossf', 1),
            ('num_bjets', 0),
            ('nJets',  1, None),
            ('mll', 20, None),
            ('eT_miss',  50, None),
            ('fabs(BL_mlll-91.2)', 10, None),
            ('fabs(BL_mll3lZ-91.2)', 0, 15),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('LepPt[1]', 25, None),
            ('met_Sig', 6, None),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            # all electrons passing ECIDS
            ('Alt$(BL_ECIDS_L_pass[0],1)', 1),
            ('Alt$(BL_ECIDS_L_pass[1],1)', 1),
            ('Alt$(BL_ECIDS_L_pass[2],1)', 1),
            # all electrons |eta|<2
            ('(BL_LepPdgId[0]==11)*fabs(BL_LepEta[0])', None, 2),
            ('(BL_LepPdgId[1]==11)*fabs(BL_LepEta[1])', None, 2),
            ('(BL_LepPdgId[2]==11)*fabs(BL_LepEta[2])', None, 2),
            ]
)

region('VR_WZ_EM',cuts=[
            ('passSS3LTrig_noMET', 1),
            ('nEl+nMu', 2),
            ('isSS', 1),
            ('isEM', 1),
            ('nCombLep', 3),
            ('nBaseLep', 3),
            ('BL_pass2lossf', 1),
            ('num_bjets', 0),
            ('nJets',  1, None),
            ('mll', 20, None),
            ('eT_miss',  50, None),
            ('fabs(BL_mlll-91.2)', 10, None),
            ('fabs(BL_mll3lZ-91.2)', 0, 15),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('met_Sig', 6, None),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            # all electrons passing ECIDS
            ('Alt$(BL_ECIDS_L_pass[0],1)', 1),
            ('Alt$(BL_ECIDS_L_pass[1],1)', 1),
            # all electrons |eta|<2
            ('(BL_LepPdgId[0]==11)*fabs(BL_LepEta[0])', None, 2),
            ('(BL_LepPdgId[1]==11)*fabs(BL_LepEta[1])', None, 2),
            ]
)

region('VR_WZ_MM',cuts=[
            ('passSS3LTrig_noMET', 1),
            ('nEl+nMu', 2),
            ('isSS', 1),
            ('isMM', 1),
            ('nCombLep', 3),
            ('nBaseLep', 3),
            ('BL_pass2lossf', 1),
            ('num_bjets', 0),
            ('nJets',  1, None),
            ('mll', 20, None),
            ('eT_miss',  50, None),
            ('fabs(BL_mlll-91.2)', 10, None),
            ('fabs(BL_mll3lZ-91.2)', 0, 15),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('met_Sig', 6, None),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ]
)
