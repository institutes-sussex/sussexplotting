from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)

mu_cut_l=0
mu_cut_u=None

region('SR1_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEE', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', 80, None),
            ('min(mTl1,mTl2)', 100, None),
            ('met_Sig', 7, None),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ],
            blinded=True
)

region('SR1_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', 80, None),
            ('min(mTl1,mTl2)', 100, None),
            ('met_Sig', 7, None),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ],
            blinded=True
)

region('SR1_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isMM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', 80, None),
            ('min(mTl1,mTl2)', 100, None),
            ('met_Sig', 7, None),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ],
            blinded=True
)

region('SR2_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEE', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', None, 80),
            ('min(mTl1,mTl2)', 100, None),
            ('met_Sig', 6, None),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ],
            blinded=True
)

region('SR2_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', None, 80),
            ('min(mTl1,mTl2)', 100, None),
            ('met_Sig', 6, None),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ],
            blinded=True
)

region('SR2_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isMM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', None, 80),
            ('min(mTl1,mTl2)', 100, None),
            ('met_Sig', 6, None),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ],
            blinded=True
)




region('SR1a_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEE', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', 80, None),
            ('min(mTl1,mTl2)', 100, None),
            ('met_Sig', 7, None),
            ('eT_miss', 75, 125),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ],
            blinded=True
)

region('SR1a_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEM', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', 80, None),
            ('min(mTl1,mTl2)', 100, None),
            ('met_Sig', 7, None),
            ('eT_miss', 75, 125),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ],
            blinded=True
)

region('SR1a_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isMM', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', 80, None),
            ('min(mTl1,mTl2)', 100, None),
            ('met_Sig', 7, None),
            ('eT_miss', 75, 125),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ],
            blinded=True
)

region('SR1b_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEE', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', 80, None),
            ('min(mTl1,mTl2)', 100, None),
            ('met_Sig', 7, None),
            ('eT_miss', 125, 200),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ],
            blinded=True
)

region('SR1b_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEM', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', 80, None),
            ('min(mTl1,mTl2)', 100, None),
            ('met_Sig', 7, None),
            ('eT_miss', 125, 200),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ],
            blinded=True
)

region('SR1b_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isMM', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', 80, None),
            ('min(mTl1,mTl2)', 100, None),
            ('met_Sig', 7, None),
            ('eT_miss', 125, 200),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ],
            blinded=True
)

region('SR1c_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEE', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', 80, None),
            ('min(mTl1,mTl2)', 100, None),
            ('met_Sig', 7, None),
            ('eT_miss', 200, None),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ],
            blinded=True
)

region('SR1c_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEM', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', 80, None),
            ('min(mTl1,mTl2)', 100, None),
            ('met_Sig', 7, None),
            ('eT_miss', 200, None),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ],
            blinded=True
)

region('SR1c_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isMM', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', 80, None),
            ('min(mTl1,mTl2)', 100, None),
            ('met_Sig', 7, None),
            ('eT_miss', 200, None),
						('mjj',  None, 350),
						('averageIntPerXing',  mu_cut_l, mu_cut_u),
            ],
            blinded=True
)
