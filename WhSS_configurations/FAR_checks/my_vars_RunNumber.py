from config.vars import variable

# Variables to plot
# reverse_zn=true will reverse the Zn curves in N-1 plots in order to look at
# upper cuts instead of lower cuts
# Branch can be specified independent of plot name, so that upper and lower
# plots can be run together

variable(name='PileUp', branch='averageIntPerXing', unit='', label='<#mu>', bins=(20, 0, 80))
variable(name='RunNumber', unit='', label='Run Number', bins=(100, 270000, 370000))
