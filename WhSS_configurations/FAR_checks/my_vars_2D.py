from config.vars import variable

# Variables to plot
# reverse_zn=true will reverse the Zn curves in N-1 plots in order to look at
# upper cuts instead of lower cuts
# Branch can be specified independent of plot name, so that upper and lower
# plots can be run together

variable(name='LepEta0', branch='LepEta[0]', unit='', label='Leading lepton #eta', bins=(20, -5, 5))
variable(name='LepPhi0', branch='LepPhi[0]', unit='', label='Leading lepton #phi', bins=(20, -5, 5))

variable(name='JetEta0', branch='JetEta[0]', unit='', label='Leading jet #eta', bins=(20, -5, 5))
variable(name='JetPhi0', branch='JetPhi[0]', unit='', label='Leading jet #phi', bins=(20, -5, 5))
