from config.vars import variable

# Variables to plot
# reverse_zn=true will reverse the Zn curves in N-1 plots in order to look at
# upper cuts instead of lower cuts
# Branch can be specified independent of plot name, so that upper and lower
# plots can be run together

variable(name='nJets', unit='', label='N_{jets}', bins=(10, 0, 10))
variable(name='num_bjets', unit='', label='N_{bjets}', bins=(10, 0, 10))

#variable(name='mTminCorr', branch='fabs(min(mTl1,mTl2)-80)', unit='GeV', label='|m_{T}^{min}-80|', bins=(15, 0, 300))
#variable(name='mTmin', branch='min(mTl1,mTl2)', unit='GeV', label='m_{T}^{min}', bins=(15, 0, 300))
#variable(name='mTmax', branch='mTlmax', unit='GeV', label='m_{T}^{max}', bins=(30, 0, 600))
variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(12, 50, 350))
#variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(30, 0, 300))
variable(name='mt2', unit='GeV', label='m_{T2}', bins=(8, 0, 80))
#variable(name='mt2', unit='GeV', label='m_{T2}', bins=(15, 0, 300))
variable(name='mTmin', branch='min(mTl1,mTl2)', unit='GeV', label='m_{T}^{min}', bins=(10, 0, 100))
#variable(name='mTmin', branch='min(mTl1,mTl2)', unit='GeV', label='m_{T}^{min}', bins=(15, 0, 300))
variable(name='met_Sig', unit='', label='E_{T}^{miss} significance', bins=(5, 0, 5))
#variable(name='met_Sig', unit='', label='E_{T}^{miss} significance', bins=(20, 0, 20))
variable(name='mjj', unit='GeV', label='m_{jj}', bins=(14, 0, 350),reverse_zn='true')
#variable(name='mjj', unit='GeV', label='m_{jj}', bins=(33, 350, 2000))
variable(name='mll', unit='GeV', label='m_{ll}', bins=(20, 0, 200))
#variable(name='BL_mlll', unit='GeV', label='m_{3l}', bins=(20, 0, 200))
#variable(name='BL_mll3lZ', unit='GeV', label='m_{ll}^{SFOS}', bins=(40, 0, 200))
#variable(name='ptll', unit='GeV', label='p_{T}^{ll}', bins=(20, 0, 500),reverse_zn='true')
#variable(name='mlj', branch='mlj_Wh', unit='GeV', label='m_{lj(j)}', bins=(30, 0, 600),reverse_zn='true')

variable(name='LepPt0', branch='LepPt[0]', unit='GeV', label='p_{T}^{l1}', bins=(12, 0, 300))
variable(name='LepEta0', branch='LepEta[0]', unit='', label='#eta^{l1}', bins=(12, -3, 3))
#variable(name='LepPhi0', branch='LepPhi[0]', unit='', label='#phi^{l1}', bins=(20, -5, 5))
variable(name='LepPt1', branch='LepPt[1]', unit='GeV', label='p_{T}^{l2}', bins=(12, 0, 300))
variable(name='LepEta1', branch='LepEta[1]', unit='', label='#eta^{l2}', bins=(12, -3, 3))
#variable(name='LepPhi1', branch='LepPhi[1]', unit='', label='#phi^{l2}', bins=(20, -5, 5))
#variable(name='JetPt', branch='JetPt',unit='GeV', label='p_{T}^{jet}', bins=(20, 0, 500))
variable(name='ElPt', branch='LepPt[0]*(LepPdgId[0]==11)+LepPt[1]*(LepPdgId[1]==11)', unit='GeV', label='p_{T}^{e}', bins=(12, 0, 300))
variable(name='ElEta', branch='LepEta[0]*(LepPdgId[0]==11)+LepEta[1]*(LepPdgId[1]==11)', unit='', label='#eta^{e}', bins=(12, -3, 3))
variable(name='MuPt', branch='LepPt[0]*(LepPdgId[0]==13)+LepPt[1]*(LepPdgId[1]==13)', unit='GeV', label='p_{T}^{#mu}', bins=(12, 0, 300))
variable(name='MuEta', branch='LepEta[0]*(LepPdgId[0]==13)+LepEta[1]*(LepPdgId[1]==13)', unit='', label='#eta^{#mu}', bins=(12, -3, 3))

#variable(name='IFFtype_El', branch='LepType_IFFclass[0]*(LepPdgId[0]==11)+LepType_IFFclass[1]*(LepPdgId[1]==11)',unit='', label='Electron IFF type', bins=(12, 0, 12))
#variable(name='IFFtype_El1', branch='LepType_IFFclass[0]*(LepPdgId[0]==11)',unit='', label='Electron 1 IFF type', bins=(12, 0, 12))
#variable(name='IFFtype_El2', branch='LepType_IFFclass[1]*(LepPdgId[1]==11)',unit='', label='Electron 2 IFF type', bins=(12, 0, 12))
#variable(name='IFFtype_Mu', branch='LepType_IFFclass[0]*(LepPdgId[0]==13)+LepType_IFFclass[1]*(LepPdgId[1]==13)',unit='', label='Muon IFF type', bins=(12, 0, 12))
#variable(name='IFFtype_Mu1', branch='LepType_IFFclass[0]*(LepPdgId[0]==13)',unit='', label='Muon 1 IFF type', bins=(12, 0, 12))
#variable(name='IFFtype_Mu2', branch='LepType_IFFclass[1]*(LepPdgId[1]==13)',unit='', label='Muon 2 IFF type', bins=(12, 0, 12))

#variable(name='IFFtype_Ele0', branch='BL_EleType_IFFclass[0]',unit='', label='Electron IFF type', bins=(11, 0, 11))
#variable(name='IFFtype_Ele1', branch='BL_EleType_IFFclass[1]',unit='', label='IFF type', bins=(11, 0, 11))
#variable(name='IFFtype_Mu0', branch='BL_MuType_IFFclass[0]',unit='', label='IFF type', bins=(11, 0, 11))
#variable(name='IFFtype_Mu1', branch='BL_MuType_IFFclass[1]',unit='', label='IFF type', bins=(11, 0, 11))
#variable(name='BL_ElePt', branch='BL_LepPt[0]*(BL_LepPdgId[0]==11)+BL_LepPt[1]*(BL_LepPdgId[1]==11)+BL_LepPt[2]*(BL_LepPdgId[2]==11)', unit='GeV', label='Electron p_{T}', bins=(20, 0, 500))
