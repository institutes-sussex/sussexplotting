from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define VRs
region('VR_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
            #('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('isSS', 1),
            ('isEE', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
						('mll', 20, None),
            ('mt2', None, 80),
            ('min(mTl1,mTl2)', None, 100),
            ('met_Sig', None, 5),
						('mjj',  None, 350),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', None, 5, 0,0,800,1),
						#('mjj',  None, 350, 0,0,1e2,100),
            ]
)

region('VR_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
            #('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('isSS', 1),
            ('isEM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
						('mll', 20, None),
            ('mt2', None, 80),
            ('min(mTl1,mTl2)', None, 100),
            ('met_Sig', None, 5),
						('mjj',  None, 350),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', None, 5, 0,0,800,1),
						#('mjj',  None, 350, 0,0,1e2,100),
            ]
)

region('VR_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
            #('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('isSS', 1),
            ('isMM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
						('mll', 20, None),
            ('mt2', None, 80),
            ('min(mTl1,mTl2)', None, 100),
            ('met_Sig', None, 5),
						('mjj',  None, 350),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', None, 5, 0,0,800,1),
						#('mjj',  None, 350, 0,0,1e2,100),
            ]
)
