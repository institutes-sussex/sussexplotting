from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np

#lumiA = 36207.66
#lumiD = 44304.7
#lumiE = 58450.1
#lumiE = 138962.46

lumiA = 1.
lumiD = 1.
lumiE = 1.

# **For histo buffering reasons, if the root file name changes, the name label
# must also change, otherwise it will throw a mismatch error**


# Define directory paths
basedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_a/'
basedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_d/'
basedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_e/'

sigdirA  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16a_sig_merged_withxsec_skimmed/'
sigdirD  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16d_sig_merged_withxsec_skimmed/'
sigdirE  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16e_sig_merged_withxsec_skimmed/'

fakedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_a/'
fakedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_d/'
fakedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_e/'

# Define weights
basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral*(EventNumber!=24159755)*(EventNumber!=22049940)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(isPromptMatched==1)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(isPromptMatched==0)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(EventTruthType!=2)'
fakesel ='FFWeight'
cfsel ='CFWeight'
sigsel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral'
datasel ='EventWeight'


# Include data
#merge(
#input(name='Data16',  files=datadir16+'data16.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True),
#input(name='Data17',  files=datadir17+'data17.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True),
#input(name='Data18',  files=datadir18+'data18.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True)
#)

# Include fake ntuple
# force_positive prevents negative fakes substracting from the total SM
#input(name='Fakes', files=basedir+'Fakes_MM_new.root',      treename='HFntuple',       color=ROOT.kOrange-2,   scale=36.1, weights=fakesel,  label='Reducible')
merge(
input(name='singleTop_A',    files=basedirA+'SingleTop.root',    treename='HFntuple',    color=ROOT.kGreen+9,     scale=lumiE, weights=basesel,    label='SingleTop'),
input(name='singleTop_D',    files=basedirD+'SingleTop.root',    treename='HFntuple',    color=ROOT.kGreen+9,     scale=lumiE, weights=basesel,    label='SingleTop'),
input(name='singleTop_E',    files=basedirE+'SingleTop.root',    treename='HFntuple',    color=ROOT.kGreen+9,     scale=lumiE, weights=basesel,    label='SingleTop')
)

merge(
input(name='VVV_A',    files=basedirA+'VVV_221.root',    treename='HFntuple',    color=ROOT.kCyan-5,     scale=lumiE, weights=basesel,    label='VVV'),
input(name='VVV_D',    files=basedirD+'VVV_221.root',    treename='HFntuple',    color=ROOT.kCyan-5,     scale=lumiE, weights=basesel,    label='VVV'),
input(name='VVV_E',    files=basedirE+'VVV_221.root',    treename='HFntuple',    color=ROOT.kCyan-5,     scale=lumiE, weights=basesel,    label='VVV')
)

merge(
input(name='ttV_A',    files=basedirA+'ttV.root',    treename='HFntuple',    color=ROOT.kCyan-1,     scale=lumiE, weights=basesel,    label='t#bar{t}+V'),
input(name='ttV_D',    files=basedirD+'ttV.root',    treename='HFntuple',    color=ROOT.kCyan-1,     scale=lumiE, weights=basesel,    label='t#bar{t}+V'),
input(name='ttV_E',    files=basedirE+'ttV.root',    treename='HFntuple',    color=ROOT.kCyan-1,     scale=lumiE, weights=basesel,    label='t#bar{t}+V')
)

merge(
input(name='ttbar_A',    files=basedirA+'ttbar.root',    treename='HFntuple',    color=ROOT.kMagenta-5,     scale=lumiE, weights=basesel,    label='t#bar{t}'),
input(name='ttbar_D',    files=basedirD+'ttbar.root',    treename='HFntuple',    color=ROOT.kMagenta-5,     scale=lumiE, weights=basesel,    label='t#bar{t}'),
input(name='ttbar_E',    files=basedirE+'ttbar.root',    treename='HFntuple',    color=ROOT.kMagenta-5,     scale=lumiE, weights=basesel,    label='t#bar{t}')
)

#merge(
#input(name='Vgamma_A',    files=basedirA+'Vgamma.root',    treename='HFntuple',    color=ROOT.kAzure+1,     scale=lumiE, weights=basesel,    label='Vgamma'),
#input(name='Vgamma_D',    files=basedirD+'Vgamma.root',    treename='HFntuple',    color=ROOT.kAzure+1,     scale=lumiE, weights=basesel,    label='Vgamma'),
#input(name='Vgamma_E',    files=basedirE+'Vgamma.root',    treename='HFntuple',    color=ROOT.kAzure+1,     scale=lumiE, weights=basesel,    label='Vgamma')
#)

merge(
input(name='Wjets_A',    files=basedirA+'WJets.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='W+jets', force_positive=True),
input(name='Wjets_D',    files=basedirD+'WJets.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='W+jets', force_positive=True),
input(name='Wjets_E',    files=basedirE+'WJets.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='W+jets', force_positive=True)
)

merge(
input(name='VV_1L_A',    files=basedirA+'VV_1L.root',    treename='HFntuple',    color=ROOT.kCyan-6,     scale=lumiE, weights=basesel,    label='VV(1L)'),
input(name='VV_1L_D',    files=basedirD+'VV_1L.root',    treename='HFntuple',    color=ROOT.kCyan-6,     scale=lumiE, weights=basesel,    label='VV(1L)'),
input(name='VV_1L_E',    files=basedirE+'VV_1L.root',    treename='HFntuple',    color=ROOT.kCyan-6,     scale=lumiE, weights=basesel,    label='VV(1L)')
)

merge(
#input(name='VV_2L_A',    files=basedirA+'VV_2L.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=lumiE, weights=basesel,    label='VV_2L'),
#input(name='VV_2L_D',    files=basedirD+'VV_2L.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=lumiE, weights=basesel,    label='VV_2L'),
#input(name='VV_2L_E',    files=basedirE+'VV_2L.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=lumiE, weights=basesel,    label='VV_2L')
input(name='VV_2L_OS_A',    files=basedirA+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=lumiE, weights=basesel,    label='VV(2L)'),
input(name='VV_2L_OS_D',    files=basedirD+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=lumiE, weights=basesel,    label='VV(2L)'),
input(name='VV_2L_OS_E',    files=basedirE+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=lumiE, weights=basesel,    label='VV(2L)'),
input(name='VV_2L_SS_A',    files=basedirA+'VV_2L_SS.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=1.5, weights=basesel,    label='VV(2L)'),
input(name='VV_2L_SS_D',    files=basedirD+'VV_2L_SS.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=1.5, weights=basesel,    label='VV(2L)'),
input(name='VV_2L_SS_E',    files=basedirE+'VV_2L_SS.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=1.5, weights=basesel,    label='VV(2L)')
##input(name='WWSS_A',    files=basedirA+'WWSS_MGPy8EG.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=1.5, weights=basesel,    label='VV_2L'),
##input(name='WWSS_D',    files=basedirD+'WWSS_MGPy8EG.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=1.5, weights=basesel,    label='VV_2L'),
##input(name='WWSS_E',    files=basedirE+'WWSS_MGPy8EG.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=1.5, weights=basesel,    label='VV_2L')
)

merge(
input(name='VV_3L_A',    files=basedirA+'VV_3L.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=lumiE, weights=basesel,    label='VV(3L)'),
input(name='VV_3L_D',    files=basedirD+'VV_3L.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=lumiE, weights=basesel,    label='VV(3L)'),
input(name='VV_3L_E',    files=basedirE+'VV_3L.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=lumiE, weights=basesel,    label='VV(3L)')
)

merge(
input(name='VV_4L_A',    files=basedirA+'VV_4L.root',    treename='HFntuple',    color=ROOT.kYellow-3,     scale=lumiE, weights=basesel,    label='VV(4L)'),
input(name='VV_4L_D',    files=basedirD+'VV_4L.root',    treename='HFntuple',    color=ROOT.kYellow-3,     scale=lumiE, weights=basesel,    label='VV(4L)'),
input(name='VV_4L_E',    files=basedirE+'VV_4L.root',    treename='HFntuple',    color=ROOT.kYellow-3,     scale=lumiE, weights=basesel,    label='VV(4L)')
)
""""
merge(
input(name='Higgs_A',    files=basedirA+'Higgs.root',    treename='HFntuple',    color=ROOT.kGreen+2,     scale=lumiE, weights=basesel,    label='Higgs'),
input(name='Higgs_D',    files=basedirD+'Higgs.root',    treename='HFntuple',    color=ROOT.kGreen+2,     scale=lumiE, weights=basesel,    label='Higgs'),
input(name='Higgs_E',    files=basedirE+'Higgs.root',    treename='HFntuple',    color=ROOT.kGreen+2,     scale=lumiE, weights=basesel,    label='Higgs')
)
"""
merge(
input(name='Zjets_ee_A',			files=basedirA+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Z+jets', force_positive=True),
input(name='Zjets_ee_D',			files=basedirD+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Z+jets', force_positive=True),
input(name='Zjets_ee_E',			files=basedirE+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Z+jets', force_positive=True),
input(name='Zjets_mm_A',			files=basedirA+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Z+jets', force_positive=True),
input(name='Zjets_mm_D',			files=basedirD+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Z+jets', force_positive=True),
input(name='Zjets_mm_E',			files=basedirE+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Z+jets', force_positive=True),
input(name='Zjets_tt_A',			files=basedirA+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Z+jets', force_positive=True),
input(name='Zjets_tt_D',			files=basedirD+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Z+jets', force_positive=True),
input(name='Zjets_tt_E',			files=basedirE+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Z+jets', force_positive=True)
)

# include signal
merge(
input(name='sig_202p5_72p5_A',    files=sigdirA+'C1N2_Wh_hall_202p5_72p5_2L7.root',     treename='HFntuple',    color=ROOT.kBlue,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(202.5,72.5)',   isSignal=True),
input(name='sig_202p5_72p5_D',    files=sigdirD+'C1N2_Wh_hall_202p5_72p5_2L7.root',     treename='HFntuple',    color=ROOT.kBlue,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(202.5,72.5)',   isSignal=True),
input(name='sig_202p5_72p5_E',    files=sigdirE+'C1N2_Wh_hall_202p5_72p5_2L7.root',     treename='HFntuple',    color=ROOT.kBlue,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(202.5,72.5)',   isSignal=True)
)

merge(
input(name='sig_400p0_0p0_A',    files=sigdirA+'C1N2_Wh_hall_400p0_0p0_2L7.root',     treename='HFntuple',    color=ROOT.kGreen+3,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(400,0)',   isSignal=True),
input(name='sig_400p0_0p0_D',    files=sigdirD+'C1N2_Wh_hall_400p0_0p0_2L7.root',     treename='HFntuple',    color=ROOT.kGreen+3,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(400,0)',   isSignal=True),
input(name='sig_400p0_0p0_E',    files=sigdirE+'C1N2_Wh_hall_400p0_0p0_2L7.root',     treename='HFntuple',    color=ROOT.kGreen+3,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(400,0)',   isSignal=True)
)

#merge(
#input(name='sig_450p0_150p0_A',    files=sigdirA+'C1N2_Wh_hall_450p0_150p0_2L7.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(450,150)',   isSignal=True),
#input(name='sig_450p0_150p0_D',    files=sigdirD+'C1N2_Wh_hall_450p0_150p0_2L7.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(450,150)',   isSignal=True),
#input(name='sig_450p0_150p0_E',    files=sigdirE+'C1N2_Wh_hall_450p0_150p0_2L7.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(450,150)',   isSignal=True)
#)

merge(
input(name='sig_177p5_47p5_A',    files=sigdirA+'C1N2_Wh_hall_177p5_47p5_2L7.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(177.5,47.5)',   isSignal=True),
input(name='sig_177p5_47p5_D',    files=sigdirD+'C1N2_Wh_hall_177p5_47p5_2L7.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(177.5,47.5)',   isSignal=True),
input(name='sig_177p5_47p5_E',    files=sigdirE+'C1N2_Wh_hall_177p5_47p5_2L7.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(177.5,47.5)',   isSignal=True)
)

merge(
input(name='sig_300p0_100p0_A',    files=sigdirA+'C1N2_Wh_hall_300p0_100p0_2L7.root',     treename='HFntuple',    color=ROOT.kMagenta+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(300,100)',   isSignal=True),
input(name='sig_300p0_100p0_D',    files=sigdirD+'C1N2_Wh_hall_300p0_100p0_2L7.root',     treename='HFntuple',    color=ROOT.kMagenta+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(300,100)',   isSignal=True),
input(name='sig_300p0_100p0_E',    files=sigdirE+'C1N2_Wh_hall_300p0_100p0_2L7.root',     treename='HFntuple',    color=ROOT.kMagenta+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(300,100)',   isSignal=True)
)
