from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


region(name='presel_EE', label='e^{#pm}e^{#pm}', cuts=[
            # Format: variable, lower cut, upper cut
            ('passSS3LTrig_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEE', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ]
)

region(name='presel_EM', label='e^{#pm}#mu^{#pm}', cuts=[
            # Format: variable, lower cut, upper cut
            ('passSS3LTrig_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ]
)

region(name='presel_MM', label='#mu^{#pm}#mu^{#pm}', cuts=[
            # Format: variable, lower cut, upper cut
            ('passSS3LTrig_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isMM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ]
)

""""
region(name='SR1_EE', label='e^{#pm}e^{#pm}', cuts=[
            # Format: variable, lower cut, upper cut
            ('passSS3LTrig_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEE', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            ('mt2', 80, None,0,0,1e3,20),
            ('met_Sig', 7, None,0,0,20,1),
						('mjj',  None, 350, 0,0,1e2,100),
            ]
)

region(name='SR1_EM', label='e^{#pm}#mu^{#pm}', cuts=[
            # Format: variable, lower cut, upper cut
            ('passSS3LTrig_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            ('mt2', 80, None,0,0,2e3,20),
            ('met_Sig', 7, None,0,0,20,1),
						('mjj',  None, 350, 0,0,1e2,100),
            ]
)

region(name='SR1_MM', label='#mu^{#pm}#mu^{#pm}', cuts=[
            # Format: variable, lower cut, upper cut
            ('passSS3LTrig_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isMM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            ('mt2', 80, None,0,0,1e3,20),
            ('met_Sig', 7, None,0,0,20,1),
						('mjj',  None, 350, 0,0,1e2,100),
            ]
)

region(name='SR2_EE', label='e^{#pm}e^{#pm}', cuts=[
            # Format: variable, lower cut, upper cut
            ('passSS3LTrig_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEE', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            ('mt2', None, 80, 0,0,1e4,20),
            ('min(mTl1,mTl2)', 100, None, 0,0,1e3,20),
            ('met_Sig', 6, None,0,0,80,1),
						('mjj',  None, 350, 0,0,30,100),
            ]
)

region(name='SR2_EM', label='e^{#pm}#mu^{#pm}', cuts=[
            # Format: variable, lower cut, upper cut
            ('passSS3LTrig_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            ('mt2', None, 80, 0,0,1e4,20),
            ('min(mTl1,mTl2)', 100, None, 0,0,1e3,20),
            ('met_Sig', 6, None,0,0,80,1),
						('mjj',  None, 350, 0,0,30,100),
            ]
)

region(name='SR2_MM', label='#mu^{#pm}#mu^{#pm}', cuts=[
            # Format: variable, lower cut, upper cut
            ('passSS3LTrig_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
						('nJets',  1, None),
            ('isSS', 1),
            ('isMM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            ('mt2', None, 80, 0,0,1e4,20),
            ('min(mTl1,mTl2)', 100, None, 0,0,1e3,20),
            ('met_Sig', 6, None,0,0,80,1),
						('mjj',  None, 350, 0,0,30,100),
            ]
)
"""
