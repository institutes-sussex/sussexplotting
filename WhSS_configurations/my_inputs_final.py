from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np
import json

#lumiA = 36207.66
#lumiD = 44304.7
#lumiE = 58450.1
#lumiE = 138962.46

lumiA = 1.
lumiD = 1.
lumiE = 1.

# **For histo buffering reasons, if the root file name changes, the name label
# must also change, otherwise it will throw a mismatch error**


# Define directory paths
basedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_a/'
basedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_d/'
basedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_e/'

sigdirA  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16a_sig_merged_withxsec_skimmed/'
sigdirD  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16d_sig_merged_withxsec_skimmed/'
sigdirE  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16e_sig_merged_withxsec_skimmed/'

fakedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_a/'
fakedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_d/'
fakedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_e/'

# Define weights
basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral*(EventNumber!=24159755)*(EventNumber!=22049940)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(isPromptMatched==1)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(isPromptMatched==0)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(EventTruthType!=2)'
fakesel ='FFWeight'
cfsel ='CFWeight'
sigsel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral'
datasel ='EventWeight'

# Add-ons for bySouce grouping
# these will work for a 2lep region and for signal leptons
isPrompt='*(EventTruthType==1)'
isPrPc='*(EventTruthType==1 || EventTruthType==3)'
isCF='*(EventTruthType==2)'
isPhConv='*(EventTruthType==3)'
isFake='*(EventTruthType==0 || EventTruthType==4 || EventTruthType==5 || EventTruthType==6)' # all including Tau, ElFromMu and (known)Unknown
isFakeLF='*(EventTruthType==0 || EventTruthType==5 || EventTruthType==6)' # LF including Tau, ElFromMu and (known)Unknown
isFakeHF='*(EventTruthType==4)' # HF only


# import json file with final relative uncertainties
rel_unc_WZ = {}
rel_unc_WWSS = {}
rel_unc_CF = {}
rel_unc_Fake = {}
rel_unc_ttV = {}
rel_unc_Other = {}
with open( "/its/home/ma2008/Desktop/Analysis2018/SS3L/Framework/sussexplotting/WhSS_configurations/rel_unc_WhSS.json", "r" ) as infile :
    mydict = json.load(infile)
    rel_unc_WZ    = mydict[ "WZ" ]
    rel_unc_WWSS  = mydict[ "WWSS" ]
    rel_unc_CF    = mydict[ "ChargeFlip" ]
    rel_unc_Fake  = mydict[ "Fake" ]
    rel_unc_ttV   = mydict[ "ttV" ]
    rel_unc_Other = mydict[ "Other" ]


# Include data
merge(
input(name='Data16',  files=basedirA+'data16.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True),
input(name='Data17',  files=basedirD+'data17.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True),
input(name='Data18',  files=basedirE+'data18.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True)
)

# force_positive prevents negative fakes substracting from the total SM

# Include fake ntuple

merge(
input(name='FakeA',        files=basedirA+'Fake.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=1,       weights=fakesel,  label='Fake/non-prompt',  rel_unc=rel_unc_Fake ),
input(name='FakeD',        files=basedirD+'Fake.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=1,       weights=fakesel,  label='Fake/non-prompt',  rel_unc=rel_unc_Fake ),
input(name='FakeE',        files=basedirE+'Fake.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=1,       weights=fakesel,  label='Fake/non-prompt',  rel_unc=rel_unc_Fake )#,
#input(name='FakePsubA',    files=fakedirA+'fakePsub_SM.root',  treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=fakesel+"*"+basesel,  label='Fake(Est.)'),
#input(name='FakePsubD',    files=fakedirD+'fakePsub_SM.root',  treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=fakesel+"*"+basesel,  label='Fake(Est.)'),
#input(name='FakePsubE',    files=fakedirE+'fakePsub_SM.root',  treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=fakesel+"*"+basesel,  label='Fake(Est.)')
)

merge(
input(name='ChargeFlipA',   files=basedirA+'ChargeFlip.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel,    label='Charge-flip',  rel_unc=rel_unc_CF ),
input(name='ChargeFlipD',   files=basedirD+'ChargeFlip.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel,    label='Charge-flip',  rel_unc=rel_unc_CF ),
input(name='ChargeFlipE',   files=basedirE+'ChargeFlip.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel,    label='Charge-flip',  rel_unc=rel_unc_CF )
)

# Grouped backgrounds

# Entire WZ - no bySource splitting
mu_WZ=1.06
merge(
input(name='VV_3L_A',    files=basedirA+'VV_3L.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=mu_WZ, weights=basesel,    label='WZ',  rel_unc=rel_unc_WZ ), 
input(name='VV_3L_D',    files=basedirD+'VV_3L.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=mu_WZ, weights=basesel,    label='WZ',  rel_unc=rel_unc_WZ ), 
input(name='VV_3L_E',    files=basedirE+'VV_3L.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=mu_WZ, weights=basesel,    label='WZ',  rel_unc=rel_unc_WZ ) 
)

# Entire WW-SS - no bySource splitting - 1.5 scale factor applied
#k_WWSS=1.
mu_WWSS=1.00
k_WWSS=1.44  # Sherpa k-factor
merge(
input(name='VV_2L_SS_A',    files=basedirA+'VV_2L_SS.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=k_WWSS*mu_WWSS, weights=basesel,    label='W^{#pm}W^{#pm}',  rel_unc=rel_unc_WWSS ), 
input(name='VV_2L_SS_D',    files=basedirD+'VV_2L_SS.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=k_WWSS*mu_WWSS, weights=basesel,    label='W^{#pm}W^{#pm}',  rel_unc=rel_unc_WWSS ), 
input(name='VV_2L_SS_E',    files=basedirE+'VV_2L_SS.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=k_WWSS*mu_WWSS, weights=basesel,    label='W^{#pm}W^{#pm}',  rel_unc=rel_unc_WWSS ) 
)

# prompt ttV - no bySource splitting
merge(
input(name='ttV_A',          files=basedirA+'ttV.root',         treename='HFntuple',    color=ROOT.kCyan-1,  scale=lumiE,    weights=basesel+isPrPc,   label='t#bar{t}+V',  rel_unc=rel_unc_ttV ), 
input(name='ttV_D',          files=basedirD+'ttV.root',         treename='HFntuple',    color=ROOT.kCyan-1,  scale=lumiE,    weights=basesel+isPrPc,   label='t#bar{t}+V',  rel_unc=rel_unc_ttV ), 
input(name='ttV_E',          files=basedirE+'ttV.root',         treename='HFntuple',    color=ROOT.kCyan-1,  scale=lumiE,    weights=basesel+isPrPc,   label='t#bar{t}+V',  rel_unc=rel_unc_ttV ), 
)

# Other - grouped - prompt and PhConv only
merge(
input(name='singleTop_pr_A',    files=basedirA+'SingleTop.root',   treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='singleTop_pr_D',    files=basedirD+'SingleTop.root',   treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='singleTop_pr_E',    files=basedirE+'SingleTop.root',   treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='VVV_pr_A',          files=basedirA+'VVV_221.root',     treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='VVV_pr_D',          files=basedirD+'VVV_221.root',     treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='VVV_pr_E',          files=basedirE+'VVV_221.root',     treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
#input(name='ttV_pr_A',          files=basedirA+'ttV.root',         treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
#input(name='ttV_pr_D',          files=basedirD+'ttV.root',         treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
#input(name='ttV_pr_E',          files=basedirE+'ttV.root',         treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='ttbar_pr_A',        files=basedirA+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='ttbar_pr_D',        files=basedirD+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='ttbar_pr_E',        files=basedirE+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='Wjets_pr_A',        files=basedirA+'WJets.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='Wjets_pr_D',        files=basedirD+'WJets.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='Wjets_pr_E',        files=basedirE+'WJets.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='VV_1L_pr_A',        files=basedirA+'VV_1L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='VV_1L_pr_D',        files=basedirD+'VV_1L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='VV_1L_pr_E',        files=basedirE+'VV_1L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='VV_2L_OS_pr_A',     files=basedirA+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='VV_2L_OS_pr_D',     files=basedirD+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='VV_2L_OS_pr_E',     files=basedirE+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='VV_4L_pr_A',        files=basedirA+'VV_4L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='VV_4L_pr_D',        files=basedirD+'VV_4L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='VV_4L_pr_E',        files=basedirE+'VV_4L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
#input(name='Higgs_pr_A',       files=basedirA+'Higgs.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
#input(name='Higgs_pr_D',       files=basedirD+'Higgs.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
#input(name='Higgs_pr_E',       files=basedirE+'Higgs.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='Zjets_ee_pr_A',     files=basedirA+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='Zjets_ee_pr_D',     files=basedirD+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='Zjets_ee_pr_E',     files=basedirE+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='Zjets_mm_pr_A',     files=basedirA+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='Zjets_mm_pr_D',     files=basedirD+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='Zjets_mm_pr_E',     files=basedirE+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='Zjets_tt_pr_A',     files=basedirA+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='Zjets_tt_pr_D',     files=basedirD+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ), 
input(name='Zjets_tt_pr_E',     files=basedirE+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other',  rel_unc=rel_unc_Other ) 
)

# include signal

merge(
input(name='sig_177p5_47p5_A',    files=sigdirA+'C1N2_Wh_hall_177p5_47p5_2L7.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(177.5,47.5)',   isSignal=True),
input(name='sig_177p5_47p5_D',    files=sigdirD+'C1N2_Wh_hall_177p5_47p5_2L7.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(177.5,47.5)',   isSignal=True),
input(name='sig_177p5_47p5_E',    files=sigdirE+'C1N2_Wh_hall_177p5_47p5_2L7.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(177.5,47.5)',   isSignal=True)
)

merge(
input(name='sig_300p0_100p0_A',    files=sigdirA+'C1N2_Wh_hall_300p0_100p0_2L7.root',     treename='HFntuple',    color=ROOT.kGreen+3,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(300,100)',   isSignal=True),
input(name='sig_300p0_100p0_D',    files=sigdirD+'C1N2_Wh_hall_300p0_100p0_2L7.root',     treename='HFntuple',    color=ROOT.kGreen+3,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(300,100)',   isSignal=True),
input(name='sig_300p0_100p0_E',    files=sigdirE+'C1N2_Wh_hall_300p0_100p0_2L7.root',     treename='HFntuple',    color=ROOT.kGreen+3,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(300,100)',   isSignal=True)
)

merge(
input(name='sig_400p0_0p0_A',    files=sigdirA+'C1N2_Wh_hall_400p0_0p0_2L7.root',     treename='HFntuple',    color=ROOT.kMagenta+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(400,0)',   isSignal=True),
input(name='sig_400p0_0p0_D',    files=sigdirD+'C1N2_Wh_hall_400p0_0p0_2L7.root',     treename='HFntuple',    color=ROOT.kMagenta+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(400,0)',   isSignal=True),
input(name='sig_400p0_0p0_E',    files=sigdirE+'C1N2_Wh_hall_400p0_0p0_2L7.root',     treename='HFntuple',    color=ROOT.kMagenta+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(400,0)',   isSignal=True)
)
