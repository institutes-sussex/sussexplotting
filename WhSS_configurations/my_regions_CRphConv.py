from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define SRs

region('CR_phConv_LooseEl',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            ('nCombLep', 3),
            ('nBaseLep', 3),
            ('nMu_baseline', 2),
            ('nEl_baseline', 1),
            #('nEl', 1), # signal electron
            ('nMu', 1, None), #  signal muons
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('BL_LepPt[2]', 25, None),
            ('BL_OneSFOS', 1), # opposite-sign BL muon pair
            #('num_bjets', 0),
            ('nBjet_WP70', 0),
						('nJets',  1, None),
						#('nJets',  0),
						('BL_ECIDS_L_pass[0]',  1), # BL ele pass ECIDS
						('fabs(BL_LepEta[0]*(BL_LepPdgId[0]==11)+BL_LepEta[1]*(BL_LepPdgId[1]==11)+BL_LepEta[2]*(BL_LepPdgId[2]==11))',  None, 2), # BL ele |eta|<2
						('(nMu==1 && nEl==0 && LepCharge[0]==(BL_LepCharge[0]*(BL_LepPdgId[0]==11)+BL_LepCharge[1]*(BL_LepPdgId[1]==11)+BL_LepCharge[2]*(BL_LepPdgId[2]==11))) || (nMu==2) || (nMu==1 && nEl==1 && (LepCharge[0]*(LepPdgId[0]==13)+LepCharge[1]*(LepPdgId[1]==13))==(BL_LepCharge[0]*(BL_LepPdgId[0]==11)+BL_LepCharge[1]*(BL_LepPdgId[1]==11)+BL_LepCharge[2]*(BL_LepPdgId[2]==11)))',), # SS El-Mu pair with signal muon 
            ],
            # Define N-1 cuts
            n_mo_cuts=[
            #('met_Sig', 6, None,0,0,800,1),
						('BL_mlll',  81.2, 101.2, 0,0,1e4,20),
						('eT_miss',  None, 50, 0,0,3e3,20),
            ]
)

region('CR_phConv_TightEl',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            ('nCombLep', 3),
            ('nBaseLep', 3),
            ('nMu_baseline', 2),
            ('nEl_baseline', 1),
            ('nEl', 1), # signal electron
            ('nMu', 1, None), #  signal muons
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('BL_LepPt[2]', 25, None),
            ('BL_OneSFOS', 1), # opposite-sign BL muon pair
            #('num_bjets', 0),
            ('nBjet_WP70', 0),
						('nJets',  1, None),
						#('nJets',  0),
						('BL_ECIDS_L_pass[0]',  1), # BL ele pass ECIDS
						('fabs(BL_LepEta[0]*(BL_LepPdgId[0]==11)+BL_LepEta[1]*(BL_LepPdgId[1]==11)+BL_LepEta[2]*(BL_LepPdgId[2]==11))',  None, 2), # BL ele |eta|<2
						('(nMu==1 && nEl==0 && LepCharge[0]==(BL_LepCharge[0]*(BL_LepPdgId[0]==11)+BL_LepCharge[1]*(BL_LepPdgId[1]==11)+BL_LepCharge[2]*(BL_LepPdgId[2]==11))) || (nMu==2) || (nMu==1 && nEl==1 && (LepCharge[0]*(LepPdgId[0]==13)+LepCharge[1]*(LepPdgId[1]==13))==(BL_LepCharge[0]*(BL_LepPdgId[0]==11)+BL_LepCharge[1]*(BL_LepPdgId[1]==11)+BL_LepCharge[2]*(BL_LepPdgId[2]==11)))',), # SS El-Mu pair with signal muon 
            ],
            # Define N-1 cuts
            n_mo_cuts=[
            #('met_Sig', 6, None,0,0,800,1),
						('BL_mlll',  81.2, 101.2, 0,0,1e4,20),
						('eT_miss',  None, 50, 0,0,3e3,20),
            ]
)

region('CR_phConv_LooseEl_prompt',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            ('nCombLep', 3),
            ('nBaseLep', 3),
            ('nMu_baseline', 2),
            ('nEl_baseline', 1),
            #('nEl', 1), # signal electron
            ('nMu', 1, None), #  signal muons
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('BL_LepPt[2]', 25, None),
            ('BL_OneSFOS', 1), # opposite-sign BL muon pair
            #('num_bjets', 0),
            ('nBjet_WP70', 0),
						('nJets',  1, None),
						#('nJets',  0),
						('BL_ECIDS_L_pass[0]',  1), # BL ele pass ECIDS
						('fabs(BL_LepEta[0]*(BL_LepPdgId[0]==11)+BL_LepEta[1]*(BL_LepPdgId[1]==11)+BL_LepEta[2]*(BL_LepPdgId[2]==11))',  None, 2), # BL ele |eta|<2
						('(nMu==1 && nEl==0 && LepCharge[0]==(BL_LepCharge[0]*(BL_LepPdgId[0]==11)+BL_LepCharge[1]*(BL_LepPdgId[1]==11)+BL_LepCharge[2]*(BL_LepPdgId[2]==11))) || (nMu==2) || (nMu==1 && nEl==1 && (LepCharge[0]*(LepPdgId[0]==13)+LepCharge[1]*(LepPdgId[1]==13))==(BL_LepCharge[0]*(BL_LepPdgId[0]==11)+BL_LepCharge[1]*(BL_LepPdgId[1]==11)+BL_LepCharge[2]*(BL_LepPdgId[2]==11)))',), # SS El-Mu pair with signal muon 
						('BL_EleType_IFFclass[0]', 2), # only MC prompt ele
            ],
            # Define N-1 cuts
            n_mo_cuts=[
            #('met_Sig', 6, None,0,0,800,1),
						('BL_mlll',  81.2, 101.2, 0,0,1e4,20),
						('eT_miss',  None, 50, 0,0,3e3,20),
            ]
)

region('CR_phConv_TightEl_prompt',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            ('nCombLep', 3),
            ('nBaseLep', 3),
            ('nMu_baseline', 2),
            ('nEl_baseline', 1),
            ('nEl', 1), # signal electron
            ('nMu', 1, None), #  signal muons
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('BL_LepPt[2]', 25, None),
            ('BL_OneSFOS', 1), # opposite-sign BL muon pair
            #('num_bjets', 0),
            ('nBjet_WP70', 0),
						('nJets',  1, None),
						#('nJets',  0),
						('BL_ECIDS_L_pass[0]',  1), # BL ele pass ECIDS
						('fabs(BL_LepEta[0]*(BL_LepPdgId[0]==11)+BL_LepEta[1]*(BL_LepPdgId[1]==11)+BL_LepEta[2]*(BL_LepPdgId[2]==11))',  None, 2), # BL ele |eta|<2
						('(nMu==1 && nEl==0 && LepCharge[0]==(BL_LepCharge[0]*(BL_LepPdgId[0]==11)+BL_LepCharge[1]*(BL_LepPdgId[1]==11)+BL_LepCharge[2]*(BL_LepPdgId[2]==11))) || (nMu==2) || (nMu==1 && nEl==1 && (LepCharge[0]*(LepPdgId[0]==13)+LepCharge[1]*(LepPdgId[1]==13))==(BL_LepCharge[0]*(BL_LepPdgId[0]==11)+BL_LepCharge[1]*(BL_LepPdgId[1]==11)+BL_LepCharge[2]*(BL_LepPdgId[2]==11)))',), # SS El-Mu pair with signal muon 
						('BL_EleType_IFFclass[0]', 2), # only MC prompt ele
            ],
            # Define N-1 cuts
            n_mo_cuts=[
            #('met_Sig', 6, None,0,0,800,1),
						('BL_mlll',  81.2, 101.2, 0,0,1e4,20),
						('eT_miss',  None, 50, 0,0,3e3,20),
            ]
)
