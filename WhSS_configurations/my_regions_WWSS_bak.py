from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


region('CR_WWSS_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('isEE', 1),
            ('num_bjets', 0),
						('nJets',  2, None),
						('eT_miss',  50, 150),
						('fabs(mll-91.18)', 15, None),
						('mll', 20, None),
						('mjj',  350, None),
            #('met_Sig', 4, None),
            ('JetPt[1]', 75, None),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            ('met_Sig', 4, None,0,0,30,1),
            ]
)

region('CR_WWSS_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('isEM', 1),
            ('num_bjets', 0),
						('nJets',  2, None),
						('eT_miss',  50, 150),
						#('fabs(mll-91.18)', 15, None),
						('mll', 20, None),
						('mjj',  350, None),
            ('JetPt[1]', 75, None),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            ('met_Sig', 2, None,0,0,80,1),
            ]
)

region('CR_WWSS_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('isMM', 1),
            ('num_bjets', 0),
						('nJets',  2, None),
						('eT_miss',  50, 150),
						#('fabs(mll-91.18)', 15, None),
						('mll', 20, None),
						('mjj',  350, None),
            ('JetPt[1]', 75, None),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            ('met_Sig', 2, None,0,0,80,1),
            ]
)

region('VR_WWSS_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('isEE', 1),
            ('num_bjets', 0),
						('nJets',  2, None),
						('eT_miss',  150, None),
						('fabs(mll-91.18)', 15, None),
						('mll', 20, None),
						('mjj',  350, None),
            #('met_Sig', 4, None),
            ('JetPt[1]', 75, None),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            ('met_Sig', 4, None,0,0,30,1),
            ]
)

region('VR_WWSS_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('isEM', 1),
            ('num_bjets', 0),
						('nJets',  2, None),
						('eT_miss',  150, None),
						#('fabs(mll-91.18)', 15, None),
						('mll', 20, None),
						('mjj',  350, None),
            ('JetPt[1]', 75, None),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            ('met_Sig', 2, None,0,0,80,1),
            ]
)

region('VR_WWSS_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('isMM', 1),
            ('num_bjets', 0),
						('nJets',  2, None),
						('eT_miss',  150, None),
						#('fabs(mll-91.18)', 15, None),
						('mll', 20, None),
						('mjj',  350, None),
            ('JetPt[1]', 75, None),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            ('met_Sig', 2, None,0,0,80,1),
            ]
)
