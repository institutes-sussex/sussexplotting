from config.vars import variable

# Variables to plot
# reverse_zn=true will reverse the Zn curves in N-1 plots in order to look at
# upper cuts instead of lower cuts
# Branch can be specified independent of plot name, so that upper and lower
# plots can be run together

variable(name='nJets', unit='', label='N_{jets}', bins=(10, 0, 10))
#variable(name='num_bjets', unit='', label='N_{bjets}', bins=(10, 0, 10))

#variable(name='mTmin', branch='min(mTl1,mTl2)', unit='GeV', label='m_{T}^{min}', bins=(30, 0, 600))
#variable(name='mTmax', branch='mTlmax', unit='GeV', label='m_{T}^{max}', bins=(30, 0, 600))
variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(12, 50, 350))
#variable(name='mt2', unit='GeV', label='m_{T2}', bins=(30, 0, 300))
#variable(name='met_Sig', unit='', label='E_{T}^{miss} significance', bins=(20, 0, 20))
variable(name='BL_mll', unit='GeV', label='m_{ll}', bins=(20, 0, 200))
#variable(name='ptll', unit='GeV', label='p_{T}^{ll}', bins=(20, 0, 500))
#variable(name='mlj', branch='mlj_Wh', unit='GeV', label='m_{lj(j)}', bins=(30, 0, 600),reverse_zn='true')

variable(name='LepPt0', branch='BL_LepPt[0]', unit='GeV', label='p_{T}^{l1}', bins=(20, 0, 500))
variable(name='LepEta0', branch='BL_LepEta[0]', unit='', label='#eta^{l1}', bins=(20, -5, 5))
#variable(name='LepPhi0', branch='LepPhi[0]', unit='', label='#phi^{l1}', bins=(20, -5, 5))
#variable(name='LepPt1', branch='LepPt[1]', unit='GeV', label='p_{T}^{l2}', bins=(20, 0, 500))
#variable(name='LepEta1', branch='LepEta[1]', unit='', label='#eta^{l2}', bins=(20, -5, 5))
#variable(name='LepPhi1', branch='LepPhi[1]', unit='', label='#phi^{l2}', bins=(20, -5, 5))
#variable(name='JetPt', branch='JetPt',unit='GeV', label='p_{T}^{jet}', bins=(20, 0, 500))

#variable(name='IFFtype_Ele0', branch='BL_EleType_IFFclass[0]',unit='', label='IFF type', bins=(11, 0, 11))
#variable(name='IFFtype_Ele1', branch='BL_EleType_IFFclass[1]',unit='', label='IFF type', bins=(11, 0, 11))
#variable(name='IFFtype_Mu0', branch='BL_MuType_IFFclass[0]',unit='', label='IFF type', bins=(11, 0, 11))
#variable(name='IFFtype_Mu1', branch='BL_MuType_IFFclass[1]',unit='', label='IFF type', bins=(11, 0, 11))
