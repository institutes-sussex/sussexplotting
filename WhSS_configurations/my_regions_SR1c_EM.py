from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


region('SR1c_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('isSS', 1),
            ('isEM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            #('EventNumber != 22049940',),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            ('mt2', 80, None,0,0,1e4,20),
            ('met_Sig', 7, None,0,0,20,1),
            ('eT_miss', 200, None,0,0,11,20),
            #('nJets', 1, None,0,0,1e5,1),
						('mjj',  None, 350, 0,0,1e2,100),
            ]
)
