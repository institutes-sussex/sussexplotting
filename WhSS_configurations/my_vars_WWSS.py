from config.vars import variable

# Variables to plot
# reverse_zn=true will reverse the Zn curves in N-1 plots in order to look at
# upper cuts instead of lower cuts
# Branch can be specified independent of plot name, so that upper and lower
# plots can be run together

#variable(name='nJets', unit='', label='N_{jets}', bins=(10, 0, 10))
#variable(name='num_bjets', unit='', label='N_{bjets}', bins=(10, 0, 10))
#variable(name='num_bjets', branch='nBjet_WP70', unit='', label='N_{bjets}', bins=(10, 0, 10))
#variable(name='DRbl', branch='sqrt( pow(atan2(sin(LepPhi[1]-BJetPhi[0]),cos(LepPhi[1]-BJetPhi[0])),2) + pow(fabs(LepEta[1]-BJetEta[0]),2) )', unit='', label='#Delta R(bjet, #mu 2)', bins=(20, 0, 5))

#variable(name='BL_mTWZ', unit='GeV', label='m_{T}^{W}', bins=(15, 0, 300))
variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(12, 50, 350))
#variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(30, 0, 300))
variable(name='met_Sig', unit='', label='E_{T}^{miss} significance', bins=(20, 0, 20))
#variable(name='BL_mll3lZ', unit='GeV', label='m_{ll}^{SFOS}', bins=(40, 0, 200))
#variable(name='BL_mlll', unit='GeV', label='m_{3l}', bins=(40, 0, 200))
variable(name='mll', unit='GeV', label='m_{ll}', bins=(40, 0, 200))
#variable(name='mjj', unit='GeV', label='m_{jj}', bins=(40, 0, 2000))
variable(name='mjj', unit='GeV', label='m_{jj}', bins=(22, 350, 2550))
#variable(name='DEtajj', unit='', label='|#Delta#eta_{jj}|', bins=(40, 0, 5))
#variable(name='DRjj', unit='', branch='fabs(DRjj)', label='|#DeltaR_{jj}|', bins=(40, 0, 5))
#variable(name='DPhijj', unit='', branch='fabs(DPhijj)', label='|#Delta#phi_{jj}|', bins=(40, 0, 5))
#variable(name='ZEP', branch='max(fabs(LepEta[0]-(JetEta[0]+JetEta[1])/2)/DEtajj,fabs(LepEta[1]-(JetEta[0]+JetEta[1])/2)/DEtajj)', unit='', label='max(Z_{l}^{*})', bins=(40, 0, 10))
variable(name='mt2', unit='GeV', label='m_{T2}', bins=(15, 0, 300))
variable(name='mTmin', branch='min(mTl1,mTl2)', unit='GeV', label='m_{T}^{min}', bins=(15, 0, 300))
variable(name='JetPt1', branch='JetPt[0]',unit='GeV', label='p_{T}^{j1}', bins=(20, 0, 500))
variable(name='JetPt2', branch='JetPt[1]',unit='GeV', label='p_{T}^{j2}', bins=(20, 0, 500))
#variable(name='HT', branch='JetPt[0]+JetPt[1]',unit='GeV', label='H_{T}', bins=(20, 0, 500))

#variable(name='IFFtype_Ele0', branch='BL_EleType_IFFclass[0]',unit='', label='Electron IFF type', bins=(12, 0, 12))
#variable(name='IFFtype_Ele1', branch='BL_EleType_IFFclass[1]',unit='', label='IFF type', bins=(12, 0, 12))
#variable(name='IFFtype_Mu0', branch='BL_MuType_IFFclass[0]',unit='', label='Mu1 type', bins=(12, 0, 12))
#variable(name='IFFtype_Mu1', branch='BL_MuType_IFFclass[1]',unit='', label='Mu2 type', bins=(12, 0, 12))
#variable(name='BL_MuType_IFFclass',unit='', label='Mu type', bins=(12, 0, 12))
#variable(name='Mu1Pt', branch='BL_LepPt[0]', unit='GeV', label='p_{T}(#mu_{1})', bins=(8, np.asarray([0, 10, 25, 40, 55, 70, 85, 100, 150],'f')))
#variable(name='Mu1Eta', branch='BL_LepEta[0]', unit='GeV', label='|#eta(#mu_{1})|', bins=(4, np.asarray([0, 0.75, 1.52, 2.0, 3.0],'f')))
#variable(name='Mu1D0sig', branch='BL_LepD0sig[0]', unit='', label='|d_{0}(#mu_{1})/#sigma(d_{0}(#mu_{1}))|', bins=(10, 0, 10) )
#variable(name='Mu1mT', branch='BL_mTl1', unit='', label='m_{T}(#mu_{1},E_{T}^{miss})', bins=(15, 0, 300) )
#variable(name='Mu2Pt', branch='BL_LepPt[1]', unit='GeV', label='p_{T}(#mu_{2})', bins=(8, np.asarray([0, 10, 25, 40, 55, 70, 85, 100, 150],'f')))
#variable(name='Mu2Eta', branch='BL_LepEta[1]', unit='GeV', label='|#eta(#mu_{2})|', bins=(4, np.asarray([0, 0.75, 1.52, 2.0, 3.0],'f')))
#variable(name='Mu2D0sig', branch='BL_LepD0sig[1]', unit='', label='|d_{0}(#mu_{2})/#sigma(d_{0}(#mu_{2}))|', bins=(10, 0, 10) )
#variable(name='Mu2mT', branch='BL_mTl2', unit='', label='m_{T}(#mu_{2},E_{T}^{miss})', bins=(15, 0, 300) )

#variable(name='mll', branch='BL_mll', unit='GeV', label='m_{ll}', bins=(40, 0, 200))

# BL electron variables in 3L selection
#variable(name='BL_ElPt', branch='BL_LepPt[0]*(BL_LepPdgId[0]==11)+BL_LepPt[1]*(BL_LepPdgId[1]==11)+BL_LepPt[2]*(BL_LepPdgId[2]==11)', unit='GeV', label='Electron p_{T}', bins=(20, 0, 500))
#variable(name='BL_ElEta', branch='BL_LepEta[0]*(BL_LepPdgId[0]==11)+BL_LepEta[1]*(BL_LepPdgId[1]==11)+BL_LepEta[2]*(BL_LepPdgId[2]==11)', unit='', label='Electron #eta', bins=(20, -5, 5))

# BL electron variables in 2L selection
#variable(name='BL_ElPt', branch='BL_LepPt[0]*(BL_LepPdgId[0]==11)+BL_LepPt[1]*(BL_LepPdgId[1]==11)', unit='GeV', label='Electron p_{T}', bins=(20, 0, 500))
#variable(name='BL_ElEta', branch='BL_LepEta[0]*(BL_LepPdgId[0]==11)+BL_LepEta[1]*(BL_LepPdgId[1]==11)', unit='', label='Electron #eta', bins=(20, -5, 5))
