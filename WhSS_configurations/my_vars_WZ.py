from config.vars import variable

# Variables to plot
# reverse_zn=true will reverse the Zn curves in N-1 plots in order to look at
# upper cuts instead of lower cuts
# Branch can be specified independent of plot name, so that upper and lower
# plots can be run together

variable(name='nJets', unit='', label='N_{jets}', bins=(10, 0, 10))
#variable(name='num_bjets', unit='', label='N_{bjets}', bins=(10, 0, 10))

#variable(name='mTminCorr', branch='fabs(min(mTl1,mTl2)-80)', unit='GeV', label='|m_{T}^{min}-80|', bins=(15, 0, 300))
#variable(name='mTmin', branch='min(mTl1,mTl2)', unit='GeV', label='m_{T}^{min}', bins=(15, 0, 300))
#variable(name='mTmax', branch='mTlmax', unit='GeV', label='m_{T}^{max}', bins=(30, 0, 600))
#variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(12, 50, 350))
variable(name='MET', branch='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(10, 50, 250))
variable(name='mll', branch='BL_mll3lZ', unit='GeV', label='m_{ll}^{SFOS}', bins=(20, 0, 200))
variable(name='mlll', branch='BL_mlll', unit='GeV', label='m_{lll}', bins=(7, 0, 350))
variable(name='BL_mTWZ', unit='GeV', label='m_{T}^{WZ}', bins=(10, 0, 200))
variable(name='met_Sig', unit='', label='E_{T}^{miss} significance', bins=(7, 0, 14))
variable(name='mll_Wh', unit='GeV', label='m_{ll}', bins=(15, 0, 300))
#variable(name='ptll', unit='GeV', label='p_{T}^{ll}', bins=(20, 0, 500),reverse_zn='true')
#variable(name='mlj', branch='mlj_Wh', unit='GeV', label='m_{lj(j)}', bins=(30, 0, 600),reverse_zn='true')
#variable(name='mT2', branch='mt2_CR', unit='GeV', label='m_{T2}', bins=(20, 0, 200))
variable(name='LepPt0', branch='LepPt[0]', unit='GeV', label='p_{T}^{l1}', bins=(20, 0, 500))
variable(name='LepEta0', branch='LepEta[0]', unit='', label='#eta^{l1}', bins=(6, -3, 3))
#variable(name='LepPhi0', branch='LepPhi[0]', unit='', label='#phi^{l1}', bins=(20, -5, 5))
variable(name='LepPt1', branch='LepPt[1]', unit='GeV', label='p_{T}^{l2}', bins=(20, 0, 500))
variable(name='LepEta1', branch='LepEta[1]', unit='', label='#eta^{l2}', bins=(6, -3, 3))
#variable(name='LepPhi1', branch='LepPhi[1]', unit='', label='#phi^{l2}', bins=(20, -5, 5))
#variable(name='JetPt', branch='JetPt',unit='GeV', label='p_{T}^{jet}', bins=(20, 0, 500))

variable(name='EleLNTPt', branch='BL_LepPt[0]*(BL_LepPdgId[0]==11 && BL_Ele_isSignal[0]==0)+BL_LepPt[1]*(BL_LepPdgId[1]==11 && BL_Ele_isSignal[1]==0)+BL_LepPt[2]*(BL_LepPdgId[2]==11 && BL_Ele_isSignal[2]==0)', unit='GeV', label='p_{T}^{e_{Loose-not-Tight}}', bins=(20, 0, 500))
variable(name='EleLNTEta', branch='BL_LepEta[0]*(BL_LepPdgId[0]==11 && BL_Ele_isSignal[0]==0)+BL_LepEta[1]*(BL_LepPdgId[1]==11 && BL_Ele_isSignal[1]==0)+BL_LepEta[2]*(BL_LepPdgId[2]==11 && BL_Ele_isSignal[2]==0)', unit='GeV', label='#eta^{e_{Loose-not-Tight}}', bins=(6, -3, 3))

variable(name='MuLNTPt', branch='BL_LepPt[0]*(BL_LepPdgId[0]==13 && BL_Mu_isSignal[0]==0)+BL_LepPt[1]*(BL_LepPdgId[1]==13 && BL_Mu_isSignal[1]==0)+BL_LepPt[2]*(BL_LepPdgId[2]==13 && BL_Mu_isSignal[2]==0)', unit='GeV', label='p_{T}^{#mu_{Loose-not-Tight}}', bins=(20, 0, 500))
variable(name='MuLNTEta', branch='BL_LepEta[0]*(BL_LepPdgId[0]==13 && BL_Mu_isSignal[0]==0)+BL_LepEta[1]*(BL_LepPdgId[1]==13 && BL_Mu_isSignal[1]==0)+BL_LepEta[2]*(BL_LepPdgId[2]==13 && BL_Mu_isSignal[2]==0)', unit='GeV', label='#eta^{#mu_{Loose-not-Tight}}', bins=(6, -3, 3))

variable(name='IFFtype_Ele0', branch='BL_EleType_IFFclass[0]',unit='', label='e_{1} IFF type', bins=(11, 0, 11))
variable(name='IFFtype_Ele1', branch='BL_EleType_IFFclass[1]',unit='', label='e_{2} IFF type', bins=(11, 0, 11))
variable(name='IFFtype_Ele2', branch='BL_EleType_IFFclass[2]',unit='', label='e_{3} IFF type', bins=(11, 0, 11))
variable(name='IFFtype_EleLNT', branch='BL_EleType_IFFclass[0]*(BL_Ele_isSignal[0]==0)+BL_EleType_IFFclass[1]*(BL_Ele_isSignal[1]==0)+BL_EleType_IFFclass[2]*(BL_Ele_isSignal[2]==0)',unit='', label='e_{Loose-not-Tight} IFF type', bins=(11, 0, 11))

variable(name='IFFtype_Mu0', branch='BL_MuType_IFFclass[0]',unit='', label='#mu_{1} IFF type', bins=(11, 0, 11))
variable(name='IFFtype_Mu1', branch='BL_MuType_IFFclass[1]',unit='', label='#mu_{2} IFF type', bins=(11, 0, 11))
variable(name='IFFtype_Mu2', branch='BL_MuType_IFFclass[2]',unit='', label='#mu_{3} IFF type', bins=(11, 0, 11))
variable(name='IFFtype_MuLNT', branch='BL_MuType_IFFclass[0]*(BL_Mu_isSignal[0]==0)+BL_MuType_IFFclass[1]*(BL_Mu_isSignal[1]==0)+BL_MuType_IFFclass[2]*(BL_Mu_isSignal[2]==0)',unit='', label='#mu_{Loose-not-Tight} IFF type', bins=(11, 0, 11))
