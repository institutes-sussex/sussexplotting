from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np

#lumiA = 36207.66
#lumiD = 44304.7
#lumiE = 58450.1
#lumiE = 138962.46

lumiA = 1.
lumiD = 1.
lumiE = 1.

# **For histo buffering reasons, if the root file name changes, the name label
# must also change, otherwise it will throw a mismatch error**


# Define directory paths
basedirA = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/bySource/mc16a/'
basedirD = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/bySource/mc16d/'
basedirE = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/bySource/mc16e/'

sigdirA  = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16a_Sig_merged_withxsec/'
sigdirD  = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16d_Sig_merged_withxsec/'
sigdirE  = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16e_Sig_merged_withxsec/'

fakedirA = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16a/fakes/'
fakedirD = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16d/fakes/'
fakedirE = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16e/fakes/'

datadir16 = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/data16_merged/'
datadir17 = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/data17_merged/'
datadir18 = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/data18_merged/'

# Define weights
basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*bjetSF_WP70'
#basesel ='lumiXsecWgt*TotalWeight'
fakesel ='FakeWeight'
sigsel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*bjetSF_WP70'
#sigsel ='lumiXsecWgt*TotalWeight'
datasel ='EventWeight'


# Include data
#merge(
#input(name='Data16',  files=datadir16+'data16.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True),
#input(name='Data17',  files=datadir17+'data17.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True),
#input(name='Data18',  files=datadir18+'data18.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True)
#)

# Include fake ntuple
# force_positive prevents negative fakes substracting from the total SM
#input(name='Fakes', files=basedir+'Fakes_MM_new.root',      treename='HFntuple',       color=ROOT.kOrange-2,   scale=36.1, weights=fakesel,  label='Reducible', force_positive=True)

merge(
input(name='ChargeFlip_A',    files=basedirA+'ChargeFlip.root',    treename='HFntuple',    color=ROOT.kOrange-2,		scale=lumiE, weights=basesel,    label='ChargeFlip', force_positive=True),
input(name='ChargeFlip_D',    files=basedirD+'ChargeFlip.root',    treename='HFntuple',    color=ROOT.kOrange-2,		scale=lumiE, weights=basesel,    label='ChargeFlip', force_positive=True),
input(name='ChargeFlip_E',    files=basedirE+'ChargeFlip.root',    treename='HFntuple',    color=ROOT.kOrange-2,		scale=lumiE, weights=basesel,    label='ChargeFlip', force_positive=True)
)

merge(
input(name='FakeHF_A',    		files=basedirA+'FakeHF.root',			   treename='HFntuple',    color=ROOT.kAzure+6,			scale=lumiE, weights=basesel,    label='HeavyF F/NP', force_positive=True),
input(name='FakeHF_D',    		files=basedirD+'FakeHF.root',			   treename='HFntuple',    color=ROOT.kAzure+6,			scale=lumiE, weights=basesel,    label='HeavyF F/NP', force_positive=True),
input(name='FakeHF_E',    		files=basedirE+'FakeHF.root',			   treename='HFntuple',    color=ROOT.kAzure+6,			scale=lumiE, weights=basesel,    label='HeavyF F/NP', force_positive=True)
)

merge(
input(name='FakeLF_A',    		files=basedirA+'FakeLF.root',			   treename='HFntuple',    color=ROOT.kCyan+2,			scale=lumiE, weights=basesel,    label='LightF F/NP', force_positive=True),
input(name='FakeLF_D',    		files=basedirD+'FakeLF.root',			   treename='HFntuple',    color=ROOT.kCyan+2,			scale=lumiE, weights=basesel,    label='LightF F/NP', force_positive=True),
input(name='FakeLF_E',    		files=basedirE+'FakeLF.root',			   treename='HFntuple',    color=ROOT.kCyan+2,			scale=lumiE, weights=basesel,    label='LightF F/NP', force_positive=True)
)

merge(
input(name='Other_A',    			files=basedirA+'Other.root', 			   treename='HFntuple',    color=ROOT.kGray,				scale=lumiE, weights=basesel,    label='Other', force_positive=True),
input(name='Other_D',    			files=basedirD+'Other.root', 			   treename='HFntuple',    color=ROOT.kGray,				scale=lumiE, weights=basesel,    label='Other', force_positive=True),
input(name='Other_E',    			files=basedirE+'Other.root', 			   treename='HFntuple',    color=ROOT.kGray,				scale=lumiE, weights=basesel,    label='Other', force_positive=True)
)

merge(
input(name='PhotConv_A', 			files=basedirA+'PhotConv.root', 	   treename='HFntuple',    color=ROOT.kOrange-3,		scale=lumiE, weights=basesel,    label='PhotConv', force_positive=True),
input(name='PhotConv_D', 			files=basedirD+'PhotConv.root', 	   treename='HFntuple',    color=ROOT.kOrange-3,		scale=lumiE, weights=basesel,    label='PhotConv', force_positive=True),
input(name='PhotConv_E', 			files=basedirE+'PhotConv.root', 	   treename='HFntuple',    color=ROOT.kOrange-3,		scale=lumiE, weights=basesel,    label='PhotConv', force_positive=True)
)

merge(
input(name='Unknown_A', 			files=basedirA+'Unknown.root',	 	   treename='HFntuple',    color=ROOT.kRed-9,				scale=lumiE, weights=basesel,    label='Unknown', force_positive=True),
input(name='Unknown_D', 			files=basedirD+'Unknown.root',	 	   treename='HFntuple',    color=ROOT.kRed-9,				scale=lumiE, weights=basesel,    label='Unknown', force_positive=True),
input(name='Unknown_E', 			files=basedirE+'Unknown.root',	 	   treename='HFntuple',    color=ROOT.kRed-9,				scale=lumiE, weights=basesel,    label='Unknown', force_positive=True)
)

merge(
input(name='Prompt_rest_A',				 	files=basedirA+'Prompt_rest.root',				  treename='HFntuple',    color=ROOT.kGreen-6,		scale=lumiE, weights=basesel,    label='Prompt', force_positive=True),
input(name='Prompt_rest_D',				 	files=basedirD+'Prompt_rest.root',				  treename='HFntuple',    color=ROOT.kGreen-6,		scale=lumiE, weights=basesel,    label='Prompt', force_positive=True),
input(name='Prompt_rest_E',				 	files=basedirE+'Prompt_rest.root',				  treename='HFntuple',    color=ROOT.kGreen-6,		scale=lumiE, weights=basesel,    label='Prompt', force_positive=True),
input(name='Prompt_VJets_A',			 	files=basedirA+'Prompt_VJets.root',	 				treename='HFntuple',    color=ROOT.kGreen-6,		scale=lumiE, weights=basesel,    label='Prompt', force_positive=True),
input(name='Prompt_VJets_D',			 	files=basedirD+'Prompt_VJets.root',	 				treename='HFntuple',    color=ROOT.kGreen-6,		scale=lumiE, weights=basesel,    label='Prompt', force_positive=True),
input(name='Prompt_VJets_E',			 	files=basedirE+'Prompt_VJets.root',	 				treename='HFntuple',    color=ROOT.kGreen-6,		scale=lumiE, weights=basesel,    label='Prompt', force_positive=True)
)

# include signal
merge(
input(name='sig_202p5_72p5_A',    files=sigdirA+'C1N2_Wh_hall_202p5_72p5_2L7.root',     treename='HFntuple',    color=ROOT.kBlue,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(202.5,72.5)',   isSignal=True),
input(name='sig_202p5_72p5_D',    files=sigdirD+'C1N2_Wh_hall_202p5_72p5_2L7.root',     treename='HFntuple',    color=ROOT.kBlue,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(202.5,72.5)',   isSignal=True),
input(name='sig_202p5_72p5_E',    files=sigdirE+'C1N2_Wh_hall_202p5_72p5_2L7.root',     treename='HFntuple',    color=ROOT.kBlue,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(202.5,72.5)',   isSignal=True)
)
""""
merge(
input(name='sig_400p0_0p0_A',    files=sigdirA+'C1N2_Wh_hall_400p0_0p0_2L7.root',     treename='HFntuple',    color=ROOT.kGreen+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(400,0)',   isSignal=True),
input(name='sig_400p0_0p0_D',    files=sigdirD+'C1N2_Wh_hall_400p0_0p0_2L7.root',     treename='HFntuple',    color=ROOT.kGreen+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(400,0)',   isSignal=True),
input(name='sig_400p0_0p0_E',    files=sigdirE+'C1N2_Wh_hall_400p0_0p0_2L7.root',     treename='HFntuple',    color=ROOT.kGreen+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(400,0)',   isSignal=True)
)

merge(
input(name='sig_177p5_47p5_A',    files=sigdirA+'C1N2_Wh_hall_177p5_47p5_2L7.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(177.5,47.5)',   isSignal=True),
input(name='sig_177p5_47p5_D',    files=sigdirD+'C1N2_Wh_hall_177p5_47p5_2L7.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(177.5,47.5)',   isSignal=True),
input(name='sig_177p5_47p5_E',    files=sigdirE+'C1N2_Wh_hall_177p5_47p5_2L7.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(177.5,47.5)',   isSignal=True)
)
"""
merge(
input(name='sig_300p0_100p0_A',    files=sigdirA+'C1N2_Wh_hall_300p0_100p0_2L7.root',     treename='HFntuple',    color=ROOT.kMagenta+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(300,100)',   isSignal=True),
input(name='sig_300p0_100p0_D',    files=sigdirD+'C1N2_Wh_hall_300p0_100p0_2L7.root',     treename='HFntuple',    color=ROOT.kMagenta+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(300,100)',   isSignal=True),
input(name='sig_300p0_100p0_E',    files=sigdirE+'C1N2_Wh_hall_300p0_100p0_2L7.root',     treename='HFntuple',    color=ROOT.kMagenta+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(300,100)',   isSignal=True)
)
