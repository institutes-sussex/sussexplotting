from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np

#lumiA = 36207.66
#lumiD = 44304.7
#lumiE = 58450.1
#lumiE = 138962.46

lumiA = 1.
lumiD = 1.
lumiE = 1.

# **For histo buffering reasons, if the root file name changes, the name label
# must also change, otherwise it will throw a mismatch error**


# Define directory paths
basedirA = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16a_merged_withxsec/'
basedirD = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16d_merged_withxsec/'
basedirE = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16e_merged_withxsec/'

zjetsdirA = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16a_merged_withxsec/'
zjetsdirD = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16d_merged_withxsec/'
zjetsdirE = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/ZJets_mc16e_merged_withxsec/'

sigdirA  = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16a_Sig_merged_withxsec/'
sigdirD  = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16d_Sig_merged_withxsec/'
sigdirE  = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/mc16e_Sig_merged_withxsec/'

fakedirA = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16a/fakes/'
fakedirD = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16d/fakes/'
fakedirE = '/lustre/scratch/epp/atlas/ma2008/Analysis2018/WhSS/ntuples/SS3L/Rel21-2-65/mc16e/fakes/'

datadir15 = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/data15_merged/'
datadir16 = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/data16_merged/'
datadir17 = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/data17_merged/'
datadir18 = '/mnt/lustre/scratch/epp_test/shared/WhSS/v120_v2/data18_merged/'

# Define weights
basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*bjetSF_WP85'
#basesel ='lumiXsecWgt*TotalWeight'
fakesel ='FakeWeight'
sigsel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*bjetSF_WP85'
#sigsel ='lumiXsecWgt*TotalWeight'
datasel ='EventWeight'


# Include data
input(name='Data18',  files=datadir18+'data18.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True)

# Include fake ntuple
# force_positive prevents negative fakes substracting from the total SM
#input(name='Fakes', files=basedir+'Fakes_MM_new.root',      treename='HFntuple',       color=ROOT.kOrange-2,   scale=36.1, weights=fakesel,  label='Reducible', force_positive=True)

input(name='singleTop_E',    files=basedirE+'SingleTop.root',    treename='HFntuple',    color=ROOT.kGreen+9,     scale=lumiE, weights=basesel,    label='SingleTop', force_positive=True)
input(name='VVV_E',    files=basedirE+'VVV.root',    treename='HFntuple',    color=ROOT.kCyan-5,     scale=lumiE, weights=basesel,    label='VVV', force_positive=True)
input(name='ttV_E',    files=basedirE+'ttV.root',    treename='HFntuple',    color=ROOT.kCyan-1,     scale=lumiE, weights=basesel,    label='ttV', force_positive=True)
input(name='ttbar_E',    files=basedirE+'ttbar.root',    treename='HFntuple',    color=ROOT.kMagenta-5,     scale=lumiE, weights=basesel,    label='ttbar', force_positive=True)
#input(name='Vgamma_E',    files=basedirE+'Vgamma.root',    treename='HFntuple',    color=ROOT.kAzure+1,     scale=lumiE, weights=basesel,    label='Vgamma', force_positive=True)
input(name='Wjets_E',    files=basedirE+'WJets.root',    treename='HFntuple',    color=ROOT.kGray,     scale=lumiE, weights=basesel,    label='Wjets', force_positive=True)
input(name='VV_1L_E',    files=basedirE+'VV_1L.root',    treename='HFntuple',    color=ROOT.kCyan-6,     scale=lumiE, weights=basesel,    label='VV_1L', force_positive=True)
input(name='VV_2L_E',    files=basedirE+'VV_2L.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=lumiE, weights=basesel,    label='VV_2L', force_positive=True)
input(name='VV_3L_E',    files=basedirE+'VV_3L.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=lumiE, weights=basesel,    label='VV_3L', force_positive=True)
input(name='VV_4L_E',    files=basedirE+'VV_4L.root',    treename='HFntuple',    color=ROOT.kYellow-3,     scale=lumiE, weights=basesel,    label='VV_4L', force_positive=True)
merge(
input(name='Zjets_ee_E',			files=basedirE+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets', force_positive=True),
input(name='Zjets_mm_E',			files=basedirE+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets', force_positive=True),
input(name='Zjets_tt_E',			files=basedirE+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kRed-7, scale=lumiE,   weights=basesel,  label='Zjets', force_positive=True)
)

# include signal
#input(name='sig_202p5_72p5',    files=sigdirE+'user.safarzad.396647.MGPy8EG_A14N23LO_C1N2_Wh_hall_202p5_72p5_2L7.SUSY2.mc16e.p3990.SSV0.07May20_HFntuple.root',     treename='HFntuple',    color=ROOT.kBlue,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(202.5,72.5)',   isSignal=True)
#input(name='sig_400p0_0p0',    files=sigdirE+'user.safarzad.396678.MGPy8EG_A14N23LO_C1N2_Wh_hall_400p0_0p0_2L7.SUSY2.mc16e.p3990.SSV0.07May20_HFntuple.root',     treename='HFntuple',    color=ROOT.kGreen+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(400,0)',   isSignal=True)
#input(name='sig_250p0_75p0',    files=sigdirE+'user.safarzad.396659.MGPy8EG_A14N23LO_C1N2_Wh_hall_250p0_75p0_2L7.SUSY2.mc16e.p3990.SSV0.07May20_HFntuple.root',     treename='HFntuple',    color=ROOT.kRed+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(250,75)',   isSignal=True)
#input(name='sig_300p0_25p0',    files=sigdirE+'user.safarzad.396665.MGPy8EG_A14N23LO_C1N2_Wh_hall_300p0_25p0_2L7.SUSY2.mc16e.p3990.SSV0.07May20_HFntuple.root',     treename='HFntuple',    color=ROOT.kMagenta+2,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(300,25)',   isSignal=True)
