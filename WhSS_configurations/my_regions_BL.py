from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define SRs

region('BL_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig', 1),
            ('BL_pass2Lsig', 1),
            ('nCombLep', 2),
            #('num_bjets', 0),
            ('num_bjets', 1, None),
            #('nJets', 1),
            ('BL_isSS', 1),
            ('BL_isEE', 1),
            #('BL_ECIDS_L_pass[0]', 1),
            #('BL_ECIDS_L_pass[1]', 1),
            ('BL_Ele_isSignal[0]', 1),
            ('BL_Ele_isSignal[1]', 1),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('( BL_LepPdgId[0]==11 && fabs(BL_LepEta[0])<2 )',),
            ('( BL_LepPdgId[1]==11 && fabs(BL_LepEta[1])<2 )',),
						#('eT_miss', 50, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('eT_miss', 120, None,0,0,10,20),
            ]
)

region('BL_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig', 1),
            ('BL_pass2Lsig', 1),
            ('nCombLep', 2),
            #('num_bjets', 0),
            ('num_bjets', 1, None),
            #('nJets', 1),
            ('BL_isSS', 1),
            ('BL_isEM || BL_isME', 1),
            #('BL_ECIDS_L_pass[0]', 1),
            ('BL_Ele_isSignal[0]', 1),
            ('BL_Mu_isSignal[0]', 1),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('( BL_LepPdgId[0]==11 && fabs(BL_LepEta[0])<2 ) || ( BL_LepPdgId[1]==11 && fabs(BL_LepEta[1])<2 )',),
						#('eT_miss', 50, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('eT_miss', 120, None,0,0,10,20),
            ]
)

region('BL_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig', 1),
            ('BL_pass2Lsig', 1),
            ('nCombLep', 2),
            #('num_bjets', 0),
            ('num_bjets', 1, None),
            #('nJets', 1),
            ('BL_isSS', 1),
            ('BL_isMM', 1),
            ('BL_Mu_isSignal[0]', 1),
            ('BL_Mu_isSignal[1]', 1),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
						#('eT_miss', 50, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('eT_miss', 120, None,0,0,10,20),
            ]
)
