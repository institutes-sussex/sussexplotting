from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define SRs

region('EE_OS',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            ('BL_pass2L', 1),
            #('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP85', 0),
            ('nJets', 1, None),
            ('BL_isSS', 0),
            ('BL_isEE', 1),
            #('BL_ECIDS_L_pass[0]', 1),
            #('BL_ECIDS_L_pass[1]', 1),
            #('BL_Ele_isSignal[0]', 1),
            #('BL_Ele_isSignal[1]', 1),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('( BL_LepPdgId[0]==11 && fabs(BL_LepEta[0])<2 )',),
            ('( BL_LepPdgId[1]==11 && fabs(BL_LepEta[1])<2 )',),
            ('BL_LepIsSigID[0]', 1),
            ('BL_LepIsSigID[1]', 1),
            ('BL_LepIsIso[0]', 1),
            ('BL_LepIsIso[1]', 1),
						('eT_miss', 50, None),
            ('BL_mll', 12, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('eT_miss', 120, None,0,0,10,20),
            ]
)

region('EM_OS',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            ('BL_pass2L', 1),
            #('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP85', 0),
            ('nJets', 1, None),
            ('BL_isSS', 0),
            ('BL_isEM', 1),
            #('BL_ECIDS_L_pass[0]', 1),
            #('BL_Ele_isSignal[0]', 1),
            #('BL_Mu_isSignal[0]', 1),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('( (BL_LepPdgId[0]==11 && fabs(BL_LepEta[0])<2) || (BL_LepPdgId[0]==13 && fabs(BL_LepD0sig[0])<3) )',),
            ('( (BL_LepPdgId[1]==11 && fabs(BL_LepEta[1])<2) || (BL_LepPdgId[1]==13 && fabs(BL_LepD0sig[1])<3) )',),
            ('BL_LepIsSigID[0]', 1),
            ('BL_LepIsSigID[1]', 1),
            ('BL_LepIsIso[0]', 1),
            ('BL_LepIsIso[1]', 1),
						('eT_miss', 50, None),
            ('BL_mll', 12, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('eT_miss', 120, None,0,0,10,20),
            ]
)

region('MM_OS',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            ('BL_pass2L', 1),
            #('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP85', 0),
            ('nJets', 1, None),
            ('BL_isSS', 0),
            ('BL_isMM', 1),
            #('BL_Mu_isSignal[0]', 1),
            #('BL_Mu_isSignal[1]', 1),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('( BL_LepPdgId[0]==13 && fabs(BL_LepD0sig[0])<3 )',),
            ('( BL_LepPdgId[1]==13 && fabs(BL_LepD0sig[1])<3 )',),
            ('BL_LepIsSigID[0]', 1),
            ('BL_LepIsSigID[1]', 1),
            ('BL_LepIsIso[0]', 1),
            ('BL_LepIsIso[1]', 1),
						('eT_miss', 50, None),
            ('BL_mll', 12, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('eT_miss', 120, None,0,0,10,20),
            ]
)

