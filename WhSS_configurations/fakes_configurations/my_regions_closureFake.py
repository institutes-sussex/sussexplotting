from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define SRs

region('CR_FakeEl',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('nBaseLep', 2),
            #('num_bjets', 1),
            #('nBjet_WP70', 0), # None),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEE', 1),
						('eT_miss',  None, 50),
						('mll', 20, None),
						('fabs(mll-91.2)', 15, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('BL_EleID_TightLH[0]+BL_EleID_TightLH[1]', 1, None),
            ('BL_EleID_TightLH[itag]', 1),
            ('EventNumber != 12331171',),
            ('EventNumber != 17791776',),
            ('EventNumber != 13085295',),
            ('EventNumber != 5574904',),
            ('EventNumber != 19814386',),
            ('EventNumber != 40851845',),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('nBjet_WP85', 1, 2, 0,0,1e5,0.5),
            #('mt2', None, 80, 0,0,1e4,20),
            #('mTl1', None, 60, 0,0,1e3,20),
            #('eT_miss', 30, None, 0,0,1e3,5),
            ]
)

region('CR_FakeMu',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 1, None),
						('nJets',  1, None),
            #('isOS', 1),
            ('isSS', 1),
            ('isMM', 1),
            ('BL_MuISO_FCTight[0]+BL_MuISO_FCTight[1]', 1, None),
            ('BL_MuISO_FCTight[itag]', 1), # Tag = FCTight
						('eT_miss',  None, 50),
						('mll', 20, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('nBjet_WP70', 1, 2, 0,0,2e3,1),
						#('eT_miss',  None, 50, 0,0,1e3,20),
						#('mll', 20, None, 0,0,1e2,20),
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', 6, None,0,0,800,1),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)

region('Presel_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
            #('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('isSS', 1),
            ('isEE', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('EventNumber != 28797154',),
            ('EventNumber != 40066058',),
            ('EventNumber != 71196207',),
            ('EventNumber != 32174010',),
            ('EventNumber != 22321628',),
            ('EventNumber != 48417183',),
            ('EventNumber != 130065334',),
            ('EventNumber != 267359714',),
            ('EventNumber != 8206533',),
            ('EventNumber != 89614619',),
            ('EventNumber != 38514543',),
            ('EventNumber != 56422253',),
            ('EventNumber != 42830970',),
            ('EventNumber != 17132585',),
            ('EventNumber != 54473615',),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', 6, None,0,0,800,1),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)

region('Presel_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
            #('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('isSS', 1),
            ('isEM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', 6, None,0,0,800,1),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)

region('Presel_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
            #('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('isSS', 1),
            ('isMM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', 6, None,0,0,800,1),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)
