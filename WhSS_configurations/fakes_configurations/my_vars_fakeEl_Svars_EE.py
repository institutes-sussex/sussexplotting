from config.vars import variable

# Variables to plot
# reverse_zn=true will reverse the Zn curves in N-1 plots in order to look at
# upper cuts instead of lower cuts
# Branch can be specified independent of plot name, so that upper and lower
# plots can be run together

itag='0'
iprobe='1'

variable(name='nJets', unit='', label='N_{jets}', bins=(10, 0, 10))
#variable(name='num_bjets', unit='', label='N_{bjets}', bins=(10, 0, 10))
variable(name='num_bjets', branch='nBjet_WP70', unit='', label='N_{bjets}', bins=(10, 0, 10))

#variable(name='BL_mTWZ', unit='GeV', label='m_{T}^{W}', bins=(15, 0, 300))
#:q
#variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(12, 50, 350))
#variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(30, 0, 300))
variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(10, 0, 50))
variable(name='met_Sig', unit='', label='E_{T}^{miss} significance', bins=(20, 0, 20))
#variable(name='BL_mll3lZ', unit='GeV', label='m_{ll}^{SFOS}', bins=(40, 0, 200))
#variable(name='BL_mlll', unit='GeV', label='m_{3l}', bins=(40, 0, 200))
variable(name='mll', unit='GeV', label='m_{ll}', bins=(40, 0, 200))
#variable(name='mjj', unit='GeV', label='m_{jj}', bins=(40, 0, 2000))
#variable(name='mjj', unit='GeV', label='m_{jj}', bins=(25, 0, 2500))
#variable(name='DEtajj', unit='', label='|#Delta#eta_{jj}|', bins=(40, 0, 5))
#variable(name='DRjj', unit='', branch='fabs(DRjj)', label='|#DeltaR_{jj}|', bins=(40, 0, 5))
#variable(name='DPhijj', unit='', branch='fabs(DPhijj)', label='|#Delta#phi_{jj}|', bins=(40, 0, 5))
#variable(name='ZEP', branch='max(fabs(LepEta[0]-(JetEta[0]+JetEta[1])/2)/DEtajj,fabs(LepEta[1]-(JetEta[0]+JetEta[1])/2)/DEtajj)', unit='', label='max(Z_{l}^{*})', bins=(40, 0, 10))
variable(name='mTmin', branch='min(mTl1,mTl2)', unit='GeV', label='m_{T}^{min}', bins=(15, 0, 300))
#variable(name='mTtag', branch='(BL_EleID_TightLH[0]==1)*mTl1+(BL_EleID_TightLH[1]==1)*mTl2', unit='GeV', label='m_{T}(e_{tag},E_{T}^{miss})', bins=(15, 0, 300))
#variable(name='mTprobe', branch='(BL_EleID_TightLH[0]==0)*mTl1+(BL_EleID_TightLH[1]==0)*mTl2', unit='GeV', label='m_{T}(e_{ptobe},E_{T}^{miss})', bins=(15, 0, 300))
variable(name='mTl1', branch='('+itag+'==0)*mTl1+('+itag+'==1)*mTl2', unit='GeV', label='m_{T}(e_{1},E_{T}^{miss})', bins=(15, 0, 300))
variable(name='mTl2', branch='('+iprobe+'==0)*mTl1+('+iprobe+'==1)*mTl2', unit='GeV', label='m_{T}(e_{2},E_{T}^{miss})', bins=(15, 0, 300))
variable(name='mt2', unit='GeV', label='m_{T2}', bins=(30, 0, 300))
#variable(name='JetPt1', branch='JetPt[0]',unit='GeV', label='p_{T}^{j1}', bins=(20, 0, 500))
#variable(name='JetPt2', branch='JetPt[1]',unit='GeV', label='p_{T}^{j2}', bins=(20, 0, 500))
variable(name='ht', unit='GeV', label='H_{T}', bins=(20, 0, 500))
#variable(name='DRetagj1', branch="sqrt(pow((BL_EleID_TightLH[0]==1)*BL_LepEta[0]+(BL_EleID_TightLH[1]==1)*BL_LepEta[1]-JetEta[0],2)+pow(atan2(sin((BL_EleID_TightLH[0]==1)*BL_LepPhi[0]+(BL_EleID_TightLH[1]==1)*BL_LepPhi[1]-JetPhi[0]),cos((BL_EleID_TightLH[0]==1)*BL_LepPhi[0]+(BL_EleID_TightLH[1]==1)*BL_LepPhi[1]-JetPhi[0])),2))", unit='', label='#DeltaR(e_{tag},jet_{1})', bins=(40, 0, 5))
#variable(name='DReprobej1', branch="sqrt(pow((BL_EleID_TightLH[0]==0)*BL_LepEta[0]+(BL_EleID_TightLH[1]==0)*BL_LepEta[1]-JetEta[0],2)+pow(atan2(sin((BL_EleID_TightLH[0]==0)*BL_LepPhi[0]+(BL_EleID_TightLH[1]==0)*BL_LepPhi[1]-JetPhi[0]),cos((BL_EleID_TightLH[0]==0)*BL_LepPhi[0]+(BL_EleID_TightLH[1]==0)*BL_LepPhi[1]-JetPhi[0])),2))", unit='', label='#DeltaR(e_{probe},jet_{1})', bins=(40, 0, 5))

#variable(name='IFFtype_EleTag', branch='(BL_EleID_TightLH[0]==1)*BL_EleType_IFFclass[0]+(BL_EleID_TightLH[1]==1)*BL_EleType_IFFclass[1]',unit='', label='Tag Electron IFF type', bins=(12, 0, 12))
#variable(name='IFFtype_EleProbe', branch='(BL_EleID_TightLH[0]==0)*BL_EleType_IFFclass[0]+(BL_EleID_TightLH[1]==0)*BL_EleType_IFFclass[1]',unit='', label='Probe Electron IFF type', bins=(12, 0, 12))
#variable(name='ElTagPt', branch='(BL_EleID_TightLH[0]==1)*BL_LepPt[0]+(BL_EleID_TightLH[1]==1)*BL_LepPt[1]', unit='GeV', label='p_{T}(e_{tag})', bins=(5, np.asarray([0, 10, 25, 40, 65, 150],'f')))
#variable(name='ElProbePt', branch='(BL_EleID_TightLH[0]==0)*BL_LepPt[0]+(BL_EleID_TightLH[1]==0)*BL_LepPt[1]', unit='GeV', label='p_{T}(e_{probe})', bins=(5, np.asarray([0, 10, 25, 40, 65, 150],'f')))
#variable(name='ElTagEta', branch='(BL_EleID_TightLH[0]==1)*fabs(BL_LepEta[0])+(BL_EleID_TightLH[1]==1)*fabs(BL_LepEta[1])', unit='', label='|#eta(e_{tag})|', bins=(2, np.asarray([0, 1.25, 2.0],'f')))
#variable(name='ElProbeEta', branch='(BL_EleID_TightLH[0]==0)*fabs(BL_LepEta[0])+(BL_EleID_TightLH[1]==0)*fabs(BL_LepEta[1])', unit='', label='|#eta(e_{probe})|', bins=(2, np.asarray([0, 1.25, 2.0],'f')))

#variable(name='DRetagj1', branch="sqrt(pow(BL_LepEta[itag]-JetEta[0],2)+pow(atan2(sin(BL_LepPhi[itag]-JetPhi[0]),cos(BL_LepPhi[itag]-JetPhi[0])),2))", unit='', label='#DeltaR(e_{tag},jet_{1})', bins=(40, 0, 5))
#variable(name='DReprobej1', branch="sqrt(pow(BL_LepEta[iprobe]-JetEta[0],2)+pow(atan2(sin(BL_LepPhi[iprobe]-JetPhi[0]),cos(BL_LepPhi[iprobe]-JetPhi[0])),2))", unit='', label='#DeltaR(e_{probe},jet_{1})', bins=(40, 0, 5))

variable(name='IFFtype_Ele1', branch='BL_EleType_IFFclass['+itag+']',unit='', label='Leading Electron IFF type', bins=(12, 0, 12))
variable(name='IFFtype_Ele2', branch='BL_EleType_IFFclass['+iprobe+']',unit='', label='Sub-leading Electron IFF type', bins=(12, 0, 12))
#variable(name='ElTagPt', branch='BL_LepPt[0]', unit='GeV', label='p_{T}(e_{tag})', bins=(5, np.asarray([0, 10, 25, 40, 65, 150],'f')))
variable(name='El1Pt', branch='BL_LepPt['+itag+']', unit='GeV', label='p_{T}(e_{1})', bins=(30,0,150))
#variable(name='ElProbePt', branch='BL_LepPt[1]', unit='GeV', label='p_{T}(e_{probe})', bins=(5, np.asarray([0, 10, 25, 40, 65, 150],'f')))
variable(name='El2Pt', branch='BL_LepPt['+iprobe+']', unit='GeV', label='p_{T}(e_{2})', bins=(30,0,150))
#variable(name='ElTagEta', branch='fabs(BL_LepEta[0])', unit='', label='|#eta(e_{tag})|', bins=(2, np.asarray([0, 1.25, 2.0],'f')))
variable(name='El1Eta', branch='fabs(BL_LepEta['+itag+'])', unit='', label='|#eta(e_{1})|', bins=(10,0,2))
#variable(name='ElProbeEta', branch='fabs(BL_LepEta[1])', unit='', label='|#eta(e_{probe})|', bins=(2, np.asarray([0, 1.25, 2.0],'f')))
variable(name='El2Eta', branch='fabs(BL_LepEta['+iprobe+'])', unit='', label='|#eta(e_{2})|', bins=(10,0,2))

# BL electron variables in 3L selection
#variable(name='BL_ElPt', branch='BL_LepPt[0]*(BL_LepPdgId[0]==11)+BL_LepPt[1]*(BL_LepPdgId[1]==11)+BL_LepPt[2]*(BL_LepPdgId[2]==11)', unit='GeV', label='Electron p_{T}', bins=(20, 0, 500))
#variable(name='BL_ElEta', branch='BL_LepEta[0]*(BL_LepPdgId[0]==11)+BL_LepEta[1]*(BL_LepPdgId[1]==11)+BL_LepEta[2]*(BL_LepPdgId[2]==11)', unit='', label='Electron #eta', bins=(20, -5, 5))

# BL electron variables in 2L selection
#variable]](name='BL_ElPt', branch='BL_LepPt[0]*(BL_LepPdgId[0]==11)+BL_LepPt[1]*(BL_LepPdgId[1]==11)', unit='GeV', label='Electron p_{T}', bins=(20, 0, 500))
#variable(name='BL_ElEta', branch='BL_LepEta[0]*(BL_LepPdgId[0]==11)+BL_LepEta[1]*(BL_LepPdgId[1]==11)', unit='', label='Electron #eta', bins=(20, -5, 5))
