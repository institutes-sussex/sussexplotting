from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define SRs

region('CR_FakeMu',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 1, None),
						('nJets',  1, None),
            #('isOS', 1),
            ('isSS', 1),
            ('isMM', 1),
            ('BL_MuISO_FCTight[0]+BL_MuISO_FCTight[1]', 1, None),
            ('BL_MuISO_FCTight[itag]', 1), # Tag = FCTight
            ('BL_Mu_isSignal[itag]', 1),  # Tag = signal
            ('BL_Mu_isSignal[iprobe]', 1), # probe = signal
						('eT_miss',  None, 50),
						('mll', 20, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('fabs(LepEta[iprobe])', 0, 2.5),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('nBjet_WP70', 1, 2, 0,0,2e3,1),
						#('eT_miss',  None, 50, 0,0,1e3,20),
						#('mll', 20, None, 0,0,1e2,20),
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', 6, None,0,0,800,1),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)
