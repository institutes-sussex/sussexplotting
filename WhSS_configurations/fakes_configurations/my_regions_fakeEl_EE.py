from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define SRs

region('CR_FakeEl_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('nBaseLep', 2),
            #('num_bjets', 1),
            #('nBjet_WP70', 0), # None),
						('nJets',  1, None),
            ('isSS', 1),
            ('isEE', 1),
						('eT_miss',  None, 50),
						#('eT_miss',  None, 50),
						('mll', 20, None),
						('fabs(mll-91.2)', 15, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('BL_EleID_TightLH[0]+BL_EleID_TightLH[1]', 1, None),
            ('BL_EleID_TightLH[itag]', 1),
            #('BL_EleType_IFFclass[1]==0 || BL_EleType_IFFclass[1]==1 || BL_EleType_IFFclass[1]==6 || BL_EleType_IFFclass[1]==7 || BL_EleType_IFFclass[1]==8 || BL_EleType_IFFclass[1]==9 || BL_EleType_IFFclass[1]==10',),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            #('nBjet_WP85', 1, 2, 0,0,1e5,0.5),
            #('mt2', None, 80, 0,0,1e4,20),
            #('mTl1', None, 60, 0,0,1e3,20),
            ('eT_miss', 30, None, 0,0,1e3,5),
            ]
)
