from config.syst import syst


# Include systematics
# Weight based systematics should be defined with isWeight=True
# In the case of the Fake ntuple, the nominal branch also needs to end with
# 'NONE'
syst(name='NONE', central=True)
#syst(name='EG_SCALE_ALL__1', up_down=('up', 'down'))
#syst(name='EG_RESOLUTION_ALL__1', up_down=('up', 'down'))
#syst(name='JET_GroupedNP_1__1', up_down=('up', 'down'))
#syst(name='JET_GroupedNP_2__1', up_down=('up', 'down'))
#syst(name='JET_GroupedNP_3__1', up_down=('up', 'down'))
#syst(name='JET_JER_SINGLE_NP__1up')
#syst(name='MET_SoftTrk_ResoPara')
#syst(name='MET_SoftTrk_ResoPerp')
#syst(name='MET_SoftTrk_Scale', up_down=('Up', 'Down'))
#syst(name='MUON_ID__1', up_down=('up', 'down'))
#syst(name='MUON_MS__1', up_down=('up', 'down'))
#syst(name='MUON_SCALE__1', up_down=('up', 'down'))
#syst(name='PRW_DATASF__1', up_down=('up', 'down'))
#syst(name='FT_EFF_extrapolation', replaces='btagSFCentral', up_down=('_DOWN', '_UP'), isWeight=True )
#syst(name='FT_EFF_extrapolationFromCharm', replaces='btagSFCentral', up_down=('_DOWN', '_UP'), isWeight=True )
#syst(name='elecSF_EFF_Iso', replaces='elecSF', up_down=('_Down', '_Up'), isWeight=True )
#syst(name='elecSF_EFF_ID', replaces='elecSF', up_down=('_Down', '_Up'), isWeight=True )
#syst(name='elecSF_EFF_Reco', replaces='elecSF', up_down=('_Down', '_Up'), isWeight=True )
#syst(name='elecSF_EFF_Trigger', replaces='elecSF', up_down=('_Down', '_Up'), isWeight=True )
#syst(name='elecSF_EFF_TriggerEff', replaces='elecSF', up_down=('_Down', '_Up'), isWeight=True )
#syst(name='muonSF_EFF_STAT', replaces='muonSF', up_down=('_Down', '_Up'), isWeight=True )
#syst(name='muonSF_EFF_STAT_LOWPT', replaces='muonSF', up_down=('_Down', '_Up'), isWeight=True )
#syst(name='muonSF_EFF_SYS', replaces='muonSF', up_down=('_Down', '_Up'), isWeight=True )
#syst(name='muonSF_EFF_SYS_LOWPT', replaces='muonSF', up_down=('_Down', '_Up'), isWeight=True )
#syst(name='muonSF_EFF_TrigStatUnc', replaces='muonSF', up_down=('_Down', '_Up'), isWeight=True )
#syst(name='muonSF_EFF_TrigSystUnc', replaces='muonSF', up_down=('_Down', '_Up'), isWeight=True )
#syst(name='muonSF_ISO_STAT', replaces='muonSF', up_down=('_Down', '_Up'), isWeight=True )
#syst(name='muonSF_ISO_SYS', replaces='muonSF', up_down=('_Down', '_Up'), isWeight=True )
#syst(name='muonSF_TTVA_STAT', replaces='muonSF', up_down=('_Down', '_Up'), isWeight=True )
#syst(name='muonSF_TTVA_SYS', replaces='muonSF', up_down=('_Down', '_Up'), isWeight=True )
#syst(name='phSF_ID_Uncertainty', replaces=central_weight, up_down=('Down', 'Up'), isWeight=True, method='add')
#syst(name='phSF_ISO_Uncertainty', replaces=central_weight, up_down=('Down', 'Up'), isWeight=True, method='add')
#syst(name='jvtSF', replaces='jvtSF', up_down=('down', 'up'), isWeight=True )
#syst(name='FT_EFF_B', replaces='btagSFCentral', up_down=('_down', '_up'), isWeight=True )
#syst(name='FT_EFF_C', replaces='btagSFCentral', up_down=('_down', '_up'), isWeight=True )
#syst(name='FT_EFF_L', replaces='btagSFCentral', up_down=('_down', '_up'), isWeight=True )
#
#
## Fake ntuple systematic
#syst(name='syst_FF', isWeight=True, replaces='eventweight', method='add')
#
#
## ChargeFlip ntuple systematic
syst(name='CFWeight__MCstat', replaces='CFWeight', up_down=('_up', '_dn'), isWeight=True )
syst(name='CFWeight__STAT', replaces='CFWeight', up_down=('_dn', '_up'), isWeight=True )
syst(name='CFWeight__SYST', replaces='CFWeight', up_down=('_dn', '_up'), isWeight=True )
