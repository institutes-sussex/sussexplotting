from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define SRs

region('Fel_EE_TSig_PTight_CF_eta2',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            #('pass2lcut', 1),
            ('nCombLep', 2),
            ('nBaseLep', 2),
            #('num_bjets', 0),
            #('nBjet_WP70', 0), # None),
						('nJets',  1, None),
            ('isSS', 1),
            ('BL_isEE', 1),
						('eT_miss',  30, 50),
						('BL_mll', 20, None),
						('fabs(BL_mll-91.2)', 15, None),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('BL_EleID_TightLH[0]+BL_EleID_TightLH[1]', 1, None),
            ('(BL_EleID_TightLH[0]+BL_EleID_TightLH[1]==1)+(BL_EleID_TightLH[0]+BL_EleID_TightLH[1]==2)*(fabs(BL_LepEta[itag])<1)',),
            ('BL_EleID_TightLH[itag]', 1),  # tag = tightID
            ('BL_Ele_isSignal[itag]', 1),  # Tag electron = signal
            ('BL_Ele_isSignal[iprobe]', 1),  # Probe electron = signal
						('BL_ECIDS_L_pass[iprobe]', 1), # Probe electron passes ECIDS
            ('fabs(BL_LepEta[iprobe])', 1.25, 2),  # Probe electron eta bin
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('nBjet_WP85', 1, 2, 0,0,1e5,0.5),
            ]
)

region('Fel_EE_TSig_PLooseNT_CF_eta2',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            #('pass2lcut', 1),
            ('nCombLep', 2),
            ('nBaseLep', 2),
            #('num_bjets', 0),
            #('nBjet_WP70', 0), # None),
						('nJets',  1, None),
            ('BL_isSS', 1),
            ('BL_isEE', 1),
						('eT_miss',  30, 50),
						('BL_mll', 20, None),
						('fabs(BL_mll-91.2)', 15, None),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('BL_EleID_TightLH[0]+BL_EleID_TightLH[1]', 1, None),
            ('(BL_EleID_TightLH[0]+BL_EleID_TightLH[1]==1)+(BL_EleID_TightLH[0]+BL_EleID_TightLH[1]==2)*(fabs(BL_LepEta[itag])<1)',),
            ('BL_EleID_TightLH[itag]', 1),  # tag = tightID
            ('BL_Ele_isSignal[itag]', 1),  # Tag electron = signal
            ('BL_Ele_isSignal[iprobe]', 0),  # Probe electron != signal
						('BL_ECIDS_L_pass[iprobe]', 1), # Probe electron passes ECIDS
            ('fabs(BL_LepEta[iprobe])', 1.25, 2),  # Probe electron eta bin
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('nBjet_WP85', 1, 2, 0,0,1e5,0.5),
            ]
)
