from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np

#lumiA = 36207.66
#lumiD = 44304.7
#lumiE = 58450.1
#lumiE = 138962.46

lumiA = 1.
lumiD = 1.
lumiE = 1.

# **For histo buffering reasons, if the root file name changes, the name label
# must also change, otherwise it will throw a mismatch error**


# Define directory paths
basedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16a_merged_withxsec_forDDEst_withvars/'
basedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16d_merged_withxsec_forDDEst_withvars/'
basedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16e_merged_withxsec_forDDEst_withvars/'

sigdirA  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16a_sig_merged_withxsec_forDDEst/'
sigdirD  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16d_sig_merged_withxsec_forDDEst/'
sigdirE  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16e_sig_merged_withxsec_forDDEst/'

fakedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_a/'
fakedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_d/'
fakedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_e/'

datadir = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/data_merged_withvars/'

# Define weights
basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(isPromptMatched==1)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(isPromptMatched==0)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(EventTruthType!=2)'
fakesel ='FFWeight'
cfsel ='CFWeight'
sigsel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral'
datasel ='EventWeight'

# Add-ons for bySouce grouping
# these will work for a 2lep region and for signal leptons
isPrompt='*(EventTruthType==1)'
isCF='*(EventTruthType==2)'
isPhConv='*(EventTruthType==3)'
isFake='*(EventTruthType==0 || EventTruthType==4 || EventTruthType==5 || EventTruthType==6)' # all including Tau, ElFromMu and (known)Unknown
isFakeLF='*(EventTruthType==0 || EventTruthType==5 || EventTruthType==6)' # LF including Tau, ElFromMu and (known)Unknown
isFakeHF='*(EventTruthType==4)' # HF only

# Include data
merge(
input(name='Data16',  files=datadir+'data16.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True),
input(name='Data17',  files=datadir+'data17.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True),
input(name='Data18',  files=datadir+'data18.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True)
)

# force_positive prevents negative fakes substracting from the total SM

# Include fake ntuple

merge(
input(name='FakeA',        files=fakedirA+'Fake.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=1,       weights=fakesel,  label='Fake(Est.)'),
input(name='FakeD',        files=fakedirD+'Fake.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=1,       weights=fakesel,  label='Fake(Est.)'),
input(name='FakeE',        files=fakedirE+'Fake.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=1,       weights=fakesel,  label='Fake(Est.)')#,
#input(name='FakePsubA',    files=fakedirA+'fakePsub_SM.root',  treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=fakesel+"*"+basesel,  label='Fake(Est.)'),
#input(name='FakePsubD',    files=fakedirD+'fakePsub_SM.root',  treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=fakesel+"*"+basesel,  label='Fake(Est.)'),
#input(name='FakePsubE',    files=fakedirE+'fakePsub_SM.root',  treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=fakesel+"*"+basesel,  label='Fake(Est.)')
)

merge(
input(name='ChargeFlipA',   files=fakedirA+'ChargeFlip.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel,    label='ChargeFlip(Est.)'),
input(name='ChargeFlipD',   files=fakedirD+'ChargeFlip.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel,    label='ChargeFlip(Est.)'),
input(name='ChargeFlipE',   files=fakedirE+'ChargeFlip.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel,    label='ChargeFlip(Est.)')
)

# Grouped backgrounds

# Entire WZ - no bySource splitting
merge(
input(name='VV_3L_A',    files=basedirA+'VV_3L.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=lumiE, weights=basesel,    label='WZ'), 
input(name='VV_3L_D',    files=basedirD+'VV_3L.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=lumiE, weights=basesel,    label='WZ'), 
input(name='VV_3L_E',    files=basedirE+'VV_3L.root',    treename='HFntuple',    color=ROOT.kOrange-3,     scale=lumiE, weights=basesel,    label='WZ') 
)

# Entire WW-SS - no bySource splitting - 1.5 scale factor applied
merge(
input(name='VV_2L_SS_A',    files=basedirA+'VV_2L_SS.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=1.5, weights=basesel,    label='W^{#pm}W^{#pm}'), 
input(name='VV_2L_SS_D',    files=basedirD+'VV_2L_SS.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=1.5, weights=basesel,    label='W^{#pm}W^{#pm}'), 
input(name='VV_2L_SS_E',    files=basedirE+'VV_2L_SS.root',    treename='HFntuple',    color=ROOT.kAzure+4,     scale=1.5, weights=basesel,    label='W^{#pm}W^{#pm}') 
)

# Other - grouped - prompt only
merge(
input(name='singleTop_pr_A',    files=basedirA+'SingleTop.root',   treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='singleTop_pr_D',    files=basedirD+'SingleTop.root',   treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='singleTop_pr_E',    files=basedirE+'SingleTop.root',   treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='VVV_pr_A',          files=basedirA+'VVV_221.root',     treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='VVV_pr_D',          files=basedirD+'VVV_221.root',     treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='VVV_pr_E',          files=basedirE+'VVV_221.root',     treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='ttV_pr_A',          files=basedirA+'ttV.root',         treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='ttV_pr_D',          files=basedirD+'ttV.root',         treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='ttV_pr_E',          files=basedirE+'ttV.root',         treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='ttbar_pr_A',        files=basedirA+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='ttbar_pr_D',        files=basedirD+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='ttbar_pr_E',        files=basedirE+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='Wjets_pr_A',        files=basedirA+'WJets.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='Wjets_pr_D',        files=basedirD+'WJets.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='Wjets_pr_E',        files=basedirE+'WJets.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='VV_1L_pr_A',        files=basedirA+'VV_1L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='VV_1L_pr_D',        files=basedirD+'VV_1L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='VV_1L_pr_E',        files=basedirE+'VV_1L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='VV_2L_OS_pr_A',     files=basedirA+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='VV_2L_OS_pr_D',     files=basedirD+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='VV_2L_OS_pr_E',     files=basedirE+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='VV_4L_pr_A',        files=basedirA+'VV_4L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='VV_4L_pr_D',        files=basedirD+'VV_4L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='VV_4L_pr_E',        files=basedirE+'VV_4L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
#input(name='Higgs_pr_A',       files=basedirA+'Higgs.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
#input(name='Higgs_pr_D',       files=basedirD+'Higgs.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
#input(name='Higgs_pr_E',       files=basedirE+'Higgs.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='Zjets_ee_pr_A',     files=basedirA+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='Zjets_ee_pr_D',     files=basedirD+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='Zjets_ee_pr_E',     files=basedirE+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='Zjets_mm_pr_A',     files=basedirA+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='Zjets_mm_pr_D',     files=basedirD+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='Zjets_mm_pr_E',     files=basedirE+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='Zjets_tt_pr_A',     files=basedirA+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='Zjets_tt_pr_D',     files=basedirD+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other'), 
input(name='Zjets_tt_pr_E',     files=basedirE+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrompt,   label='Other') 
)
""""
# Charge-Flip - grouped
merge(
input(name='singleTop_CF_A',    files=basedirA+'SingleTop.root',   treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='singleTop_CF_D',    files=basedirD+'SingleTop.root',   treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='singleTop_CF_E',    files=basedirE+'SingleTop.root',   treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='VVV_CF_A',          files=basedirA+'VVV_221.root',     treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='VVV_CF_D',          files=basedirD+'VVV_221.root',     treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='VVV_CF_E',          files=basedirE+'VVV_221.root',     treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='ttV_CF_A',          files=basedirA+'ttV.root',         treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='ttV_CF_D',          files=basedirD+'ttV.root',         treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='ttV_CF_E',          files=basedirE+'ttV.root',         treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='ttbar_CF_A',        files=basedirA+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='ttbar_CF_D',        files=basedirD+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='ttbar_CF_E',        files=basedirE+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='Wjets_CF_A',        files=basedirA+'WJets.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='Wjets_CF_D',        files=basedirD+'WJets.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='Wjets_CF_E',        files=basedirE+'WJets.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='VV_1L_CF_A',        files=basedirA+'VV_1L.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='VV_1L_CF_D',        files=basedirD+'VV_1L.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='VV_1L_CF_E',        files=basedirE+'VV_1L.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='VV_2L_OS_CF_A',     files=basedirA+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='VV_2L_OS_CF_D',     files=basedirD+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='VV_2L_OS_CF_E',     files=basedirE+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='VV_4L_CF_A',        files=basedirA+'VV_4L.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='VV_4L_CF_D',        files=basedirD+'VV_4L.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='VV_4L_CF_E',        files=basedirE+'VV_4L.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
#input(name='Higgs_CF_A',       files=basedirA+'Higgs.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
#input(name='Higgs_CF_D',       files=basedirD+'Higgs.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
#input(name='Higgs_CF_E',       files=basedirE+'Higgs.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='Zjets_ee_CF_A',     files=basedirA+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='Zjets_ee_CF_D',     files=basedirD+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='Zjets_ee_CF_E',     files=basedirE+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='Zjets_mm_CF_A',     files=basedirA+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='Zjets_mm_CF_D',     files=basedirD+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='Zjets_mm_CF_E',     files=basedirE+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='Zjets_tt_CF_A',     files=basedirA+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='Zjets_tt_CF_D',     files=basedirD+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='Zjets_tt_CF_E',     files=basedirE+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)') 
)
"""
# ISR+FSR Photon conversion - grouped
merge(
input(name='singleTop_PC_A',    files=basedirA+'SingleTop.root',   treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='singleTop_PC_D',    files=basedirD+'SingleTop.root',   treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='singleTop_PC_E',    files=basedirE+'SingleTop.root',   treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='VVV_PC_A',          files=basedirA+'VVV_221.root',     treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='VVV_PC_D',          files=basedirD+'VVV_221.root',     treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='VVV_PC_E',          files=basedirE+'VVV_221.root',     treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='ttV_PC_A',          files=basedirA+'ttV.root',         treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='ttV_PC_D',          files=basedirD+'ttV.root',         treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='ttV_PC_E',          files=basedirE+'ttV.root',         treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='ttbar_PC_A',        files=basedirA+'ttbar.root',       treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='ttbar_PC_D',        files=basedirD+'ttbar.root',       treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='ttbar_PC_E',        files=basedirE+'ttbar.root',       treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='Wjets_PC_A',        files=basedirA+'WJets.root',       treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='Wjets_PC_D',        files=basedirD+'WJets.root',       treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='Wjets_PC_E',        files=basedirE+'WJets.root',       treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='VV_1L_PC_A',        files=basedirA+'VV_1L.root',       treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='VV_1L_PC_D',        files=basedirD+'VV_1L.root',       treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='VV_1L_PC_E',        files=basedirE+'VV_1L.root',       treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='VV_2L_OS_PC_A',     files=basedirA+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='VV_2L_OS_PC_D',     files=basedirD+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='VV_2L_OS_PC_E',     files=basedirE+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='VV_4L_PC_A',        files=basedirA+'VV_4L.root',       treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='VV_4L_PC_D',        files=basedirD+'VV_4L.root',       treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='VV_4L_PC_E',        files=basedirE+'VV_4L.root',       treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
#input(name='Higgs_PC_A',       files=basedirA+'Higgs.root',       treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
#input(name='Higgs_PC_D',       files=basedirD+'Higgs.root',       treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
#input(name='Higgs_PC_E',       files=basedirE+'Higgs.root',       treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='Zjets_ee_PC_A',     files=basedirA+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='Zjets_ee_PC_D',     files=basedirD+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='Zjets_ee_PC_E',     files=basedirE+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='Zjets_mm_PC_A',     files=basedirA+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='Zjets_mm_PC_D',     files=basedirD+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='Zjets_mm_PC_E',     files=basedirE+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='Zjets_tt_PC_A',     files=basedirA+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='Zjets_tt_PC_D',     files=basedirD+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)'), 
input(name='Zjets_tt_PC_E',     files=basedirE+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kGray,      scale=lumiE,    weights=basesel+isPhConv,   label='#gamma-conv.(MC)') 
)
""""
# Fake inclusive - grouped
merge(
input(name='singleTop_F_A',    files=basedirA+'SingleTop.root',   treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='singleTop_F_D',    files=basedirD+'SingleTop.root',   treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='singleTop_F_E',    files=basedirE+'SingleTop.root',   treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='VVV_F_A',          files=basedirA+'VVV_221.root',     treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='VVV_F_D',          files=basedirD+'VVV_221.root',     treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='VVV_F_E',          files=basedirE+'VVV_221.root',     treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='ttV_F_A',          files=basedirA+'ttV.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='ttV_F_D',          files=basedirD+'ttV.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='ttV_F_E',          files=basedirE+'ttV.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='ttbar_F_A',        files=basedirA+'ttbar.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='ttbar_F_D',        files=basedirD+'ttbar.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='ttbar_F_E',        files=basedirE+'ttbar.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='Wjets_F_A',        files=basedirA+'WJets.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='Wjets_F_D',        files=basedirD+'WJets.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='Wjets_F_E',        files=basedirE+'WJets.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='VV_1L_F_A',        files=basedirA+'VV_1L.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='VV_1L_F_D',        files=basedirD+'VV_1L.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='VV_1L_F_E',        files=basedirE+'VV_1L.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='VV_2L_OS_F_A',     files=basedirA+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='VV_2L_OS_F_D',     files=basedirD+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='VV_2L_OS_F_E',     files=basedirE+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='VV_4L_F_A',        files=basedirA+'VV_4L.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='VV_4L_F_D',        files=basedirD+'VV_4L.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='VV_4L_F_E',        files=basedirE+'VV_4L.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
#input(name='Higgs_F_A',       files=basedirA+'Higgs.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
#input(name='Higgs_F_D',       files=basedirD+'Higgs.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
#input(name='Higgs_F_E',       files=basedirE+'Higgs.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='Zjets_ee_F_A',     files=basedirA+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='Zjets_ee_F_D',     files=basedirD+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='Zjets_ee_F_E',     files=basedirE+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='Zjets_mm_F_A',     files=basedirA+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='Zjets_mm_F_D',     files=basedirD+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='Zjets_mm_F_E',     files=basedirE+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='Zjets_tt_F_A',     files=basedirA+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='Zjets_tt_F_D',     files=basedirD+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)'), 
input(name='Zjets_tt_F_E',     files=basedirE+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,   weights=basesel+isFake,   label='Fake(MC)') 
)
"""
""""
# FakeLF inclusive - grouped
merge(
input(name='singleTop_LF_A',    files=basedirA+'SingleTop.root',   treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='singleTop_LF_D',    files=basedirD+'SingleTop.root',   treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='singleTop_LF_E',    files=basedirE+'SingleTop.root',   treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='VVV_LF_A',          files=basedirA+'VVV_221.root',     treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='VVV_LF_D',          files=basedirD+'VVV_221.root',     treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='VVV_LF_E',          files=basedirE+'VVV_221.root',     treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='ttV_LF_A',          files=basedirA+'ttV.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='ttV_LF_D',          files=basedirD+'ttV.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='ttV_LF_E',          files=basedirE+'ttV.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='ttbar_LF_A',        files=basedirA+'ttbar.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='ttbar_LF_D',        files=basedirD+'ttbar.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='ttbar_LF_E',        files=basedirE+'ttbar.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='Wjets_LF_A',        files=basedirA+'WJets.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='Wjets_LF_D',        files=basedirD+'WJets.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='Wjets_LF_E',        files=basedirE+'WJets.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='VV_1L_LF_A',        files=basedirA+'VV_1L.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='VV_1L_LF_D',        files=basedirD+'VV_1L.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='VV_1L_LF_E',        files=basedirE+'VV_1L.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='VV_2L_OS_LF_A',     files=basedirA+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='VV_2L_OS_LF_D',     files=basedirD+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='VV_2L_OS_LF_E',     files=basedirE+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='VV_4L_LF_A',        files=basedirA+'VV_4L.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='VV_4L_LF_D',        files=basedirD+'VV_4L.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='VV_4L_LF_E',        files=basedirE+'VV_4L.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
#input(name='Higgs_LF_A',       files=basedirA+'Higgs.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
#input(name='Higgs_LF_D',       files=basedirD+'Higgs.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
#input(name='Higgs_LF_E',       files=basedirE+'Higgs.root',       treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='Zjets_ee_LF_A',     files=basedirA+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='Zjets_ee_LF_D',     files=basedirD+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='Zjets_ee_LF_E',     files=basedirE+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='Zjets_mm_LF_A',     files=basedirA+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='Zjets_mm_LF_D',     files=basedirD+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='Zjets_mm_LF_E',     files=basedirE+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='Zjets_tt_LF_A',     files=basedirA+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='Zjets_tt_LF_D',     files=basedirD+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)'), 
input(name='Zjets_tt_LF_E',     files=basedirE+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kMagenta-9,   scale=lumiE,    weights=basesel+isFakeLF,   label='FakeLF(MC)') 
)

# FakeHF - grouped
merge(
input(name='singleTop_HF_A',    files=basedirA+'SingleTop.root',   treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='singleTop_HF_D',    files=basedirD+'SingleTop.root',   treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='singleTop_HF_E',    files=basedirE+'SingleTop.root',   treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='VVV_HF_A',          files=basedirA+'VVV_221.root',     treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='VVV_HF_D',          files=basedirD+'VVV_221.root',     treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='VVV_HF_E',          files=basedirE+'VVV_221.root',     treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='ttV_HF_A',          files=basedirA+'ttV.root',         treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='ttV_HF_D',          files=basedirD+'ttV.root',         treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='ttV_HF_E',          files=basedirE+'ttV.root',         treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='ttbar_HF_A',        files=basedirA+'ttbar.root',       treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='ttbar_HF_D',        files=basedirD+'ttbar.root',       treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='ttbar_HF_E',        files=basedirE+'ttbar.root',       treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='Wjets_HF_A',        files=basedirA+'WJets.root',       treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='Wjets_HF_D',        files=basedirD+'WJets.root',       treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='Wjets_HF_E',        files=basedirE+'WJets.root',       treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='VV_1L_HF_A',        files=basedirA+'VV_1L.root',       treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='VV_1L_HF_D',        files=basedirD+'VV_1L.root',       treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='VV_1L_HF_E',        files=basedirE+'VV_1L.root',       treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='VV_2L_OS_HF_A',     files=basedirA+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='VV_2L_OS_HF_D',     files=basedirD+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='VV_2L_OS_HF_E',     files=basedirE+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='VV_4L_HF_A',        files=basedirA+'VV_4L.root',       treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='VV_4L_HF_D',        files=basedirD+'VV_4L.root',       treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='VV_4L_HF_E',        files=basedirE+'VV_4L.root',       treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
#input(name='Higgs_HF_A',       files=basedirA+'Higgs.root',       treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
#input(name='Higgs_HF_D',       files=basedirD+'Higgs.root',       treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
#input(name='Higgs_HF_E',       files=basedirE+'Higgs.root',       treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='Zjets_ee_HF_A',     files=basedirA+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='Zjets_ee_HF_D',     files=basedirD+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='Zjets_ee_HF_E',     files=basedirE+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='Zjets_mm_HF_A',     files=basedirA+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='Zjets_mm_HF_D',     files=basedirD+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='Zjets_mm_HF_E',     files=basedirE+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='Zjets_tt_HF_A',     files=basedirA+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='Zjets_tt_HF_D',     files=basedirD+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)'), 
input(name='Zjets_tt_HF_E',     files=basedirE+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kRed-7,   scale=lumiE,    weights=basesel+isFakeHF,   label='FakeHF(MC)') 
)
"""

# include signal
""""
merge(
input(name='sig_177p5_47p5_A',    files=sigdirA+'C1N2_Wh_hall_177p5_47p5_2L7.root',     treename='HFntuple',    color=ROOT.kBlue,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(177.5,47.5)',   isSignal=True),
input(name='sig_177p5_47p5_D',    files=sigdirD+'C1N2_Wh_hall_177p5_47p5_2L7.root',     treename='HFntuple',    color=ROOT.kBlue,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(177.5,47.5)',   isSignal=True),
input(name='sig_177p5_47p5_E',    files=sigdirE+'C1N2_Wh_hall_177p5_47p5_2L7.root',     treename='HFntuple',    color=ROOT.kBlue,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(177.5,47.5)',   isSignal=True)
)

merge(
input(name='sig_175p0_0p0_A',    files=sigdirA+'C1N2_Wh_hall_175p0_0p0_2L7.root',     treename='HFntuple',    color=ROOT.kGreen+3,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(175,0)',   isSignal=True),
input(name='sig_175p0_0p0_D',    files=sigdirD+'C1N2_Wh_hall_175p0_0p0_2L7.root',     treename='HFntuple',    color=ROOT.kGreen+3,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(175,0)',   isSignal=True),
input(name='sig_175p0_0p0_E',    files=sigdirE+'C1N2_Wh_hall_175p0_0p0_2L7.root',     treename='HFntuple',    color=ROOT.kGreen+3,      linestyle=5,    scale=lumiE, weights=sigsel,    label='Wh(175,0)',   isSignal=True)
)
"""
