from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np

#lumiA = 36207.66
#lumiD = 44304.7
#lumiE = 58450.1
#lumiE = 138962.46

lumiA = 1.
lumiD = 1.
lumiE = 1.

# **For histo buffering reasons, if the root file name changes, the name label
# must also change, otherwise it will throw a mismatch error**


# Define directory paths
basedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16a_merged_withxsec_forDDEst_withvars/'
basedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16d_merged_withxsec_forDDEst_withvars/'
basedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16e_merged_withxsec_forDDEst_withvars/'

sigdirA  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16a_sig_merged_withxsec_forDDEst/'
sigdirD  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16d_sig_merged_withxsec_forDDEst/'
sigdirE  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16e_sig_merged_withxsec_forDDEst/'

fakedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_a/'
fakedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_d/'
fakedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_e/'

datadir = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/data_merged_withvars/'

# Define weights
basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*bjetSF_WP70'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*bjetSF_WP70*(isPromptMatched==1)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*bjetSF_WP70*(isPromptMatched==0)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*bjetSF_WP70*(EventTruthType!=2)'
#fakesel ='FFWeight'
#fakesel ='FFWeight__STAT_up'
#fakesel ='FFWeight__STAT_dn'
#fakesel ='FFWeight__CRSRextr_up'
#fakesel ='FFWeight__CRSRextr_dn'
#fakesel ='FFWeight__FakeTag_up'
#fakesel ='FFWeight__FakeTag_dn'
#fakesel ='FFWeight__Psub_up'
#fakesel ='FFWeight__Psub_dn'
#fakesel ='FFWeight__CFsub_up'
fakesel ='FFWeight__CFsub_dn'
cfsel ='CFWeight'
sigsel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*bjetSF_WP70'
datasel ='EventWeight'

# Include fake ntuple
merge(
input(name='FakeA',        files=fakedirA+'Fake.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=1,       weights=fakesel,  label='Fake(Est.)'),
input(name='FakeD',        files=fakedirD+'Fake.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=1,       weights=fakesel,  label='Fake(Est.)'),
input(name='FakeE',        files=fakedirE+'Fake.root',         treename='HFntuple',    color=ROOT.kMagenta-9,   scale=1,       weights=fakesel,  label='Fake(Est.)')
)
