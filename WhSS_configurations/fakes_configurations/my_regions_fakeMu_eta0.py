from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define SRs

region('Fmu_TSig_PLoose_All_eta0',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            #('pass2lcut', 1),
            ('nCombLep', 2),
            ('nBaseLep', 2),
            ('num_bjets', 1, None),
						('nJets',  1, None),
            ('BL_isSS', 1),
            ('BL_isMM', 1),
            ('BL_MuISO_FCTight[0]+BL_MuISO_FCTight[1]', 1, None),
            ('BL_MuISO_FCTight[itag]', 1), # Tag = FCTight
            ('BL_Mu_isSignal[itag]', 1),  # Tag = signal
            #('BL_Mu_isSignal[iprobe]', 1), # probe = signal
						('eT_miss',  None, 50),
						('BL_mll', 20, None),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('fabs(BL_LepEta[iprobe])', 0, 2.5),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('nBjet_WP85', 1, 2, 0,0,1e5,0.5),
            ]
)

region('Fmu_TSig_PTight_All_eta0',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            #('pass2lcut', 1),
            ('nCombLep', 2),
            ('nBaseLep', 2),
            ('num_bjets', 1, None),
						('nJets',  1, None),
            ('BL_isSS', 1),
            ('BL_isMM', 1),
            ('BL_MuISO_FCTight[0]+BL_MuISO_FCTight[1]', 1, None),
            ('BL_MuISO_FCTight[itag]', 1), # Tag = FCTight
            ('BL_Mu_isSignal[itag]', 1),  # Tag = signal
            ('BL_Mu_isSignal[iprobe]', 1), # probe = signal
						('eT_miss',  None, 50),
						('BL_mll', 20, None),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('fabs(BL_LepEta[iprobe])', 0, 2.5),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('nBjet_WP85', 1, 2, 0,0,1e5,0.5),
            ]
)

region('Fmu_TSig_PLooseNT_All_eta0',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            #('pass2lcut', 1),
            ('nCombLep', 2),
            ('nBaseLep', 2),
            ('num_bjets', 1, None),
						('nJets',  1, None),
            ('BL_isSS', 1),
            ('BL_isMM', 1),
            ('BL_MuISO_FCTight[0]+BL_MuISO_FCTight[1]', 1, None),
            ('BL_MuISO_FCTight[itag]', 1), # Tag = FCTight
            ('BL_Mu_isSignal[itag]', 1),  # Tag = signal
            ('BL_Mu_isSignal[iprobe]', 0), # probe != signal
						('eT_miss',  None, 50),
						('BL_mll', 20, None),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('fabs(BL_LepEta[iprobe])', 0, 2.5),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('nBjet_WP85', 1, 2, 0,0,1e5,0.5),
            ]
)

# regions for probeMC != fake

region('Fmu_TSig_PLoose_notF_eta0',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            #('pass2lcut', 1),
            ('nCombLep', 2),
            ('nBaseLep', 2),
            ('num_bjets', 1, None),
						('nJets',  1, None),
            ('BL_isSS', 1),
            ('BL_isMM', 1),
            ('BL_MuISO_FCTight[0]+BL_MuISO_FCTight[1]', 1, None),
            ('BL_MuISO_FCTight[itag]', 1), # Tag = FCTight
            ('BL_Mu_isSignal[itag]', 1),  # Tag = signal
            #('BL_Mu_isSignal[iprobe]', 1), # probe = signal
						('BL_MuType_IFFclass[iprobe]!=1 && BL_MuType_IFFclass[iprobe]!=8 && BL_MuType_IFFclass[iprobe]!=9 && BL_MuType_IFFclass[iprobe]!=10', ),
						('eT_miss',  None, 50),
						('BL_mll', 20, None),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('fabs(BL_LepEta[iprobe])', 0, 2.5),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('nBjet_WP85', 1, 2, 0,0,1e5,0.5),
            ]
)

region('Fmu_TSig_PTight_notF_eta0',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            #('pass2lcut', 1),
            ('nCombLep', 2),
            ('nBaseLep', 2),
            ('num_bjets', 1, None),
						('nJets',  1, None),
            ('BL_isSS', 1),
            ('BL_isMM', 1),
            ('BL_MuISO_FCTight[0]+BL_MuISO_FCTight[1]', 1, None),
            ('BL_MuISO_FCTight[itag]', 1), # Tag = FCTight
            ('BL_Mu_isSignal[itag]', 1),  # Tag = signal
            ('BL_Mu_isSignal[iprobe]', 1), # probe = signal
						('BL_MuType_IFFclass[iprobe]!=1 && BL_MuType_IFFclass[iprobe]!=8 && BL_MuType_IFFclass[iprobe]!=9 && BL_MuType_IFFclass[iprobe]!=10', ),
						('eT_miss',  None, 50),
						('BL_mll', 20, None),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('fabs(BL_LepEta[iprobe])', 0, 2.5),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('nBjet_WP85', 1, 2, 0,0,1e5,0.5),
            ]
)

region('Fmu_TSig_PLooseNT_notF_eta0',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            #('pass2lcut', 1),
            ('nCombLep', 2),
            ('nBaseLep', 2),
            ('num_bjets', 1, None),
						('nJets',  1, None),
            ('BL_isSS', 1),
            ('BL_isMM', 1),
            ('BL_MuISO_FCTight[0]+BL_MuISO_FCTight[1]', 1, None),
            ('BL_MuISO_FCTight[itag]', 1), # Tag = FCTight
            ('BL_Mu_isSignal[itag]', 1),  # Tag = signal
            ('BL_Mu_isSignal[iprobe]', 0), # probe != signal
						('BL_MuType_IFFclass[iprobe]!=1 && BL_MuType_IFFclass[iprobe]!=8 && BL_MuType_IFFclass[iprobe]!=9 && BL_MuType_IFFclass[iprobe]!=10', ),
						('eT_miss',  None, 50),
						('BL_mll', 20, None),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('fabs(BL_LepEta[iprobe])', 0, 2.5),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('nBjet_WP85', 1, 2, 0,0,1e5,0.5),
            ]
)
