from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np

#lumiA = 36207.66
#lumiD = 44304.7
#lumiE = 58450.1
#lumiE = 138962.46

lumiA = 1.
lumiD = 1.
lumiE = 1.

# **For histo buffering reasons, if the root file name changes, the name label
# must also change, otherwise it will throw a mismatch error**


# Define directory paths
basedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16a_merged_withxsec_forDDEst_withvars/'
basedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16d_merged_withxsec_forDDEst_withvars/'
basedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16e_merged_withxsec_forDDEst_withvars/'

sigdirA  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16a_sig_merged_withxsec_forDDEst/'
sigdirD  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16d_sig_merged_withxsec_forDDEst/'
sigdirE  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16e_sig_merged_withxsec_forDDEst/'

fakedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_a/'
fakedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_d/'
fakedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_e/'

datadir = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/data_merged_withvars/'

# Define weights
basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(isPromptMatched==1)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(isPromptMatched==0)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(EventTruthType!=2)'
fakesel ='FFWeight'
cfsel ='CFWeight'
sigsel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral'
datasel ='EventWeight'

# Add-ons for bySouce grouping
# these will work for a 2lep region and for signal leptons
isPrompt='*(EventTruthType==1)'
#isCF='*(EventTruthType==2)'
isCF='*((BL_EleType_IFFclass[0]==2 && BL_EleType_IFFclass[1]==3)||(BL_EleType_IFFclass[1]==2 && BL_EleType_IFFclass[0]==3))'
isPhConv='*(EventTruthType==3)'
isFake='*(EventTruthType==0 || EventTruthType==4 || EventTruthType==5 || EventTruthType==6)' # all including Tau, ElFromMu and (known)Unknown
isFakeLF='*(EventTruthType==0 || EventTruthType==5 || EventTruthType==6)' # LF including Tau, ElFromMu and (known)Unknown
isFakeHF='*(EventTruthType==4)' # HF only

merge(
#input(name='ChargeFlip_ttbar_A',   files=fakedirA+'ChargeFlip_ttbar.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
#input(name='ChargeFlip_ttbar_D',   files=fakedirD+'ChargeFlip_ttbar.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
#input(name='ChargeFlip_ttbar_E',   files=fakedirE+'ChargeFlip_ttbar.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
#input(name='ChargeFlip_ZJets_A',   files=fakedirA+'ChargeFlip_ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
#input(name='ChargeFlip_ZJets_D',   files=fakedirD+'ChargeFlip_ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
#input(name='ChargeFlip_ZJets_E',   files=fakedirE+'ChargeFlip_ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True)
input(name='ChargeFlip_ttbar_A',   files=fakedirA+'ChargeFlip_LooseNT_ttbar.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
input(name='ChargeFlip_ttbar_D',   files=fakedirD+'ChargeFlip_LooseNT_ttbar.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
input(name='ChargeFlip_ttbar_E',   files=fakedirE+'ChargeFlip_LooseNT_ttbar.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
input(name='ChargeFlip_ZJets_A',   files=fakedirA+'ChargeFlip_LooseNT_ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
input(name='ChargeFlip_ZJets_D',   files=fakedirD+'ChargeFlip_LooseNT_ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlipEst.)', isData=True),
input(name='ChargeFlip_ZJets_E',   files=fakedirE+'ChargeFlip_LooseNT_ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlipEst.)', isData=True)
)

""""
merge(
input(name='ChargeFlip_ttbar_A',   files=fakedirA+'ChargeFlipMC_ttbar.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
input(name='ChargeFlip_ttbar_D',   files=fakedirD+'ChargeFlipMC_ttbar.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
input(name='ChargeFlip_ttbar_E',   files=fakedirE+'ChargeFlipMC_ttbar.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
input(name='ChargeFlip_ZJets_A',   files=fakedirA+'ChargeFlipMC_ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
input(name='ChargeFlip_ZJets_D',   files=fakedirD+'ChargeFlipMC_ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
input(name='ChargeFlip_ZJets_E',   files=fakedirE+'ChargeFlipMC_ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True)
#input(name='ChargeFlip_ttbar_A',   files=fakedirA+'ChargeFlipMC_LooseNT_ttbar.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
#input(name='ChargeFlip_ttbar_D',   files=fakedirD+'ChargeFlipMC_LooseNT_ttbar.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
#input(name='ChargeFlip_ttbar_E',   files=fakedirE+'ChargeFlipMC_LooseNT_ttbar.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
#input(name='ChargeFlip_ZJets_A',   files=fakedirA+'ChargeFlipMC_LooseNT_ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlip(Est.)', isData=True),
#input(name='ChargeFlip_ZJets_D',   files=fakedirD+'ChargeFlipMC_LooseNT_ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlipEst.)', isData=True),
#input(name='ChargeFlip_ZJets_E',   files=fakedirE+'ChargeFlipMC_LooseNT_ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,    scale=1,   weights=cfsel+'*'+basesel,    label='ChargeFlipEst.)', isData=True)
)
"""

# Grouped backgrounds

# Charge-Flip - grouped
merge(
input(name='ttbar_CF_A',        files=basedirA+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='ttbar_CF_D',        files=basedirD+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='ttbar_CF_E',        files=basedirE+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'),
input(name='Zjets_ee_CF_A',     files=basedirA+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='Zjets_ee_CF_D',     files=basedirD+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)'), 
input(name='Zjets_ee_CF_E',     files=basedirE+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-7,  scale=lumiE,    weights=basesel+isCF,   label='ChargeFlip(MC)') 
)
