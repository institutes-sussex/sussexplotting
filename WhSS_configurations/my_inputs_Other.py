from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np

#lumiA = 36207.66
#lumiD = 44304.7
#lumiE = 58450.1
#lumiE = 138962.46

lumiA = 1.
lumiD = 1.
lumiE = 1.

# **For histo buffering reasons, if the root file name changes, the name label
# must also change, otherwise it will throw a mismatch error**


# Define directory paths
basedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_a/'
basedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_d/'
basedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/HFntuples_e/'

sigdirA  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16a_sig_merged_withxsec_skimmed/'
sigdirD  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16d_sig_merged_withxsec_skimmed/'
sigdirE  = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/mc16e_sig_merged_withxsec_skimmed/'

fakedirA = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_a/'
fakedirD = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_d/'
fakedirE = '/mnt/lustre/projects/epp/general/atlas/SUSYEWK/2LSSAna/v150_SYS/DataDriven_e/'

# Define weights
basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral*(EventNumber!=22049940)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(isPromptMatched==1)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(isPromptMatched==0)'
#basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*chargeFlipSF*SS3LTriggerSF_noMET*btagSFCentral*(EventTruthType!=2)'
fakesel ='FFWeight'
cfsel ='CFWeight'
sigsel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF*elecChargeSF*SS3LTriggerSF_noMET*btagSFCentral'
datasel ='EventWeight'

# Add-ons for bySouce grouping
# these will work for a 2lep region and for signal leptons
isPrompt='*(EventTruthType==1)'
isPrPc='*(EventTruthType==1 || EventTruthType==3)'
isCF='*(EventTruthType==2)'
isPhConv='*(EventTruthType==3)'
isFake='*(EventTruthType==0 || EventTruthType==4 || EventTruthType==5 || EventTruthType==6)' # all including Tau, ElFromMu and (known)Unknown
isFakeLF='*(EventTruthType==0 || EventTruthType==5 || EventTruthType==6)' # LF including Tau, ElFromMu and (known)Unknown
isFakeHF='*(EventTruthType==4)' # HF only

# Other - grouped - prompt and PhConv only
merge(
input(name='singleTop_pr_A',    files=basedirA+'SingleTop.root',   treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='singleTop_pr_D',    files=basedirD+'SingleTop.root',   treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='singleTop_pr_E',    files=basedirE+'SingleTop.root',   treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VVV_pr_A',          files=basedirA+'VVV_221.root',     treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VVV_pr_D',          files=basedirD+'VVV_221.root',     treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VVV_pr_E',          files=basedirE+'VVV_221.root',     treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
#input(name='ttV_pr_A',          files=basedirA+'ttV.root',         treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
#input(name='ttV_pr_D',          files=basedirD+'ttV.root',         treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
#input(name='ttV_pr_E',          files=basedirE+'ttV.root',         treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='ttbar_pr_A',        files=basedirA+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='ttbar_pr_D',        files=basedirD+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='ttbar_pr_E',        files=basedirE+'ttbar.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Wjets_pr_A',        files=basedirA+'WJets.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Wjets_pr_D',        files=basedirD+'WJets.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Wjets_pr_E',        files=basedirE+'WJets.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VV_1L_pr_A',        files=basedirA+'VV_1L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VV_1L_pr_D',        files=basedirD+'VV_1L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VV_1L_pr_E',        files=basedirE+'VV_1L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VV_2L_OS_pr_A',     files=basedirA+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VV_2L_OS_pr_D',     files=basedirD+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VV_2L_OS_pr_E',     files=basedirE+'VV_2L_OS.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VV_4L_pr_A',        files=basedirA+'VV_4L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VV_4L_pr_D',        files=basedirD+'VV_4L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='VV_4L_pr_E',        files=basedirE+'VV_4L.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
#input(name='Higgs_pr_A',       files=basedirA+'Higgs.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
#input(name='Higgs_pr_D',       files=basedirD+'Higgs.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
#input(name='Higgs_pr_E',       files=basedirE+'Higgs.root',       treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Zjets_ee_pr_A',     files=basedirA+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Zjets_ee_pr_D',     files=basedirD+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Zjets_ee_pr_E',     files=basedirE+'ZJets_ee.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Zjets_mm_pr_A',     files=basedirA+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Zjets_mm_pr_D',     files=basedirD+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Zjets_mm_pr_E',     files=basedirE+'ZJets_mm.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Zjets_tt_pr_A',     files=basedirA+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Zjets_tt_pr_D',     files=basedirD+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other'), 
input(name='Zjets_tt_pr_E',     files=basedirE+'ZJets_tt.root',    treename='HFntuple',    color=ROOT.kYellow-3,  scale=lumiE,    weights=basesel+isPrPc,   label='Other') 
)
