from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)



region('VR_WZ_EE',cuts=[ 
				    ('passSS3LTrig_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
            ('nJets',  1, None),
            ('isSS', 1),
            ('isEE', 1),
            ('eT_miss',  50, 75),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', 80, None),
						('mjj', None, 350),
            ],
            
)

region('VR_WZ_EM',cuts=[
            ('passSS3LTrig_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
            ('nJets',  1, None),
            ('isSS', 1),
            ('isEM', 1),
            ('eT_miss',  50, 75),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', 80, None),
						('mjj', None, 350),
            ],

)

region('VR_WZ_MM',cuts=[
            ('passSS3LTrig_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
            ('nJets',  1, None),
            ('isSS', 1),
            ('isMM', 1),
            ('eT_miss',  50, 75),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mt2', 80, None),
						('mjj', None, 350),
            ],

)
