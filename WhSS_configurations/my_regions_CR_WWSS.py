from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define SRs
region('CR_WWSS_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            ('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('isEE', 1),
            #('num_bjets', 0),
            ('nBjet_WP70', 0),
						('nJets',  2, None),
						('eT_miss',  50, None),
						('fabs(mll-91.18)', 15, None),
						('mll', 20, None),
            ],
            #Define N-1 cuts
            n_mo_cuts=[
						('mjj',  350, None, 0,0,1e2,100),
            ('met_Sig', 4, None,0,0,100,1),
            ('JetPt[1]', 75, None,0,0,100,25),
            ]
)

region('CR_WWSS_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            ('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('isEM', 1),
            #('num_bjets', 0),
            ('nBjet_WP70', 0),
						('nJets',  2, None),
						('eT_miss',  50, None),
						#('fabs(mll-91.18)', 15, None),
						('mll', 20, None),
            ],
            #Define N-1 cuts
            n_mo_cuts=[
						('mjj',  350, None, 0,0,2e2,100),
            ('JetPt[1]', 75, None,0,0,100,25),
            ]
)

region('CR_WWSS_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            ('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('isMM', 1),
            #('num_bjets', 0),
            ('nBjet_WP70', 0),
						('nJets',  2, None),
						('eT_miss',  50, None),
						#('fabs(mll-91.18)', 15, None),
						('mll', 20, None),
            ],
            #Define N-1 cuts
            n_mo_cuts=[
						('mjj',  350, None, 0,0,2e2,100),
            ('JetPt[1]', 75, None,0,0,100,25),
            ]
)
