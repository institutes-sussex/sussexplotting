from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


region('CR_WWSS',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('num_bjets', 0),
						('nJets',  2, None),
						('eT_miss',  50, None),
						('(isEM==1)+(isMM==1)+(isEE==1)*(fabs(mll-91.18)>15)', 1),
						('mll', 20, None),
						('mjj',  350, None),
            #('met_Sig', 4, None),
            #('JetPt[1]', 75, None),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            #('met_Sig', 4, 6, 0,0,80,1),
            ('met_Sig', None, 6, 0,0,110,1),
            ('JetPt[1]', 75, None,0,0,80,25),
            ]
)

region('VR_WWSS',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nCombLep', 2),
            ('pass2lcut', 1),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('isSS', 1),
            ('num_bjets', 0),
						('nJets',  2, None),
						('eT_miss',  50, None),
						('(isEM==1)+(isMM==1)+(isEE==1)*(fabs(mll-91.18)>15)', 1),
						('mll', 20, None),
						('mjj',  350, None),
            #('met_Sig', 4, None),
            #('JetPt[1]', 75, None),
            ],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            n_mo_cuts=[
            ('met_Sig', 6, None, 0,0,110,1),
            ('JetPt[1]', 75, None,0,0,55,25),
            ]
)
