from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define SRs

region('EE_OS',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            #('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP85', 0),
						('nJets',  1, None),
            ('isOS', 1),
            ('isEE', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mll_Wh', 12, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('eT_miss', 50, None,0,0,1e5,20),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)

region('EM_OS',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            #('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP85', 0),
						('nJets',  1, None),
            ('isOS', 1),
            ('isEM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mll_Wh', 12, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('eT_miss', 50, None,0,0,1e5,20),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)

region('MM_OS',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            #('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP85', 0),
						('nJets',  1, None),
            ('isOS', 1),
            ('isMM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            ('mll_Wh', 12, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('eT_miss', 50, None,0,0,1e5,20),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)
