from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define SRs

region('WhSS_presel',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('isSS', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', 6, None,0,0,800,1),
            #('nJets', 1, None,0,0,1e5,1),
            ],
            blinded=True
)
""""
region('WhSS_presel_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('isSS', 1),
            ('isEE', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', 6, None,0,0,800,1),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)

region('WhSS_presel_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('isSS', 1),
            ('isEM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', 6, None,0,0,800,1),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)

region('WhSS_presel_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('isSS', 1),
            ('isMM', 1),
						('eT_miss',  50, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', 6, None,0,0,800,1),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)

region('presel_EM_LooseEl',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nBaseLep', 2),
            ('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('BL_isSS', 1),
            ('BL_isEM==1 || BL_isME==1',),
						('eT_miss',  50, None),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('BL_Mu_isSignal[0]', 1),
            ('fabs(BL_LepEta[0]*(BL_LepPdgId[0]==11)+BL_LepEta[1]*(BL_LepPdgId[1]==11))', None, 2),
            ('BL_Ele_isInCrack[0]', 0),
            ('BL_ECIDS_L_pass[0]', 1),
            ('fabs(BL_LepD0sig[0]*(BL_LepPdgId[0]==11)+BL_LepD0sig[1]*(BL_LepPdgId[1]==11))', None, 5),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', 6, None,0,0,800,1),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)

region('presel_EM_LooseNotTightEl',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nBaseLep', 2),
            ('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('BL_isSS', 1),
            ('BL_isEM==1 || BL_isME==1',),
						('eT_miss',  50, None),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('BL_Mu_isSignal[0]', 1),
            ('BL_Ele_isSignal[0]', 0),
            ('fabs(BL_LepEta[0]*(BL_LepPdgId[0]==11)+BL_LepEta[1]*(BL_LepPdgId[1]==11))', None, 2),
            ('BL_Ele_isInCrack[0]', 0),
            ('BL_ECIDS_L_pass[0]', 1),
            ('fabs(BL_LepD0sig[0]*(BL_LepPdgId[0]==11)+BL_LepD0sig[1]*(BL_LepPdgId[1]==11))', None, 5),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', 6, None,0,0,800,1),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)

region('presel_EM_TightEl',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nBaseLep', 2),
            ('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('BL_isSS', 1),
            ('BL_isEM==1 || BL_isME==1',),
						('eT_miss',  50, None),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
            ('BL_Mu_isSignal[0]', 1),
            ('BL_Ele_isSignal[0]', 1),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', 6, None,0,0,800,1),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)
"""
