from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define SRs

region('Fakes_MM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 1),
            #('nBjet_WP70', 1), # None),
						('nJets',  1, None),
            #('isOS', 1),
            #('isSS', 1),
            ('isMM', 1),
						('eT_miss',  None, 50),
						#('fabs(mll-91.18)', 15, None),
						('fabs(mll-90)', 15, None),
						('mll', 20, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
						#('sqrt( pow(atan2(sin(LepPhi[1]-BJetPhi[0]),cos(LepPhi[1]-BJetPhi[0])),2) + pow(fabs(LepEta[1]-BJetEta[0]),2) )', None, 0.3),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
						#('sqrt( pow(atan2(sin(LepPhi[1]-BJetPhi[0]),cos(LepPhi[1]-BJetPhi[0])),2) + pow(fabs(LepEta[1]-BJetEta[0]),2) )', None, 0.3, 0,0,1e5,0.5),
            #('nBjet_WP70', 1, 2, 0,0,1e5,0.5),
            #('mll', 20, None, 0,0,7e2,10),
            ]
)
""""
region('Fakes_EE',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP70', 1), # None),
						('nJets',  1, None),
            #('isOS', 1),
            #('isSS', 1),
            ('isEE', 1),
						('eT_miss', None, 50),
						#('fabs(mll-91.18)', 15, None),
            #('LepPt[0]', 25, None),
            #('LepPt[1]', 25, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', 6, None,0,0,800,1),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)

region('Fakes_EM',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            #('num_bjets', 0),
            ('nBjet_WP70', 1), # None),
						('nJets',  1, None),
            #('isOS', 1),
            #('isSS', 1),
            ('isEM', 1),
						('eT_miss',  None, 50),
						('fabs(mll-91.18)', 15, None),
            #('LepPt[0]', 25, None),
            #('LepPt[1]', 25, None),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', 6, None,0,0,800,1),
            #('nJets', 1, None,0,0,1e5,1),
            ]
)
"""
