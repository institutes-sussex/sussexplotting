from config.regions import region
from config.syst import syst


syst(name='NONE', central=True)


#Define VRs
""""
region('CF_valid',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('pass2lcut', 1),
            ('nCombLep', 2),
            ('num_bjets', 0),
            #('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('isSS', 1),
            ('isEE', 1),
						#('eT_miss',  30, None),
            ('LepPt[0]', 25, None),
            ('LepPt[1]', 25, None),
						('mll', 20, None),
            ('mt2', None, 80),
            ('min(mTl1,mTl2)', None, 100),
            ('met_Sig', None, 5),
						('mjj',  None, 350),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', None, 5, 0,0,800,1),
						#('mjj',  None, 350, 0,0,1e2,100),
            ]
)
"""

region('CF_valid_LooseNT',cuts=[
            # Format: variable, lower cut, upper cut
            #('Pass2ElTrigger || Pass2MuTrigger || PassElMuTrigger',),
            ('passSS3LTrig_noMET', 1),
            #('passSS3LTrigMatch_noMET', 1),
            ('nBaseLep', 2),
            ('nCombLep', 2),
            ('nEl', 0),
            ('num_bjets', 0),
            #('nBjet_WP70', 0),
						('nJets',  1, None),
            #('isOS', 1),
            ('BL_isSS', 1),
            ('BL_isEE', 1),
						('eT_miss',  30, None),
            ('BL_LepPt[0]', 25, None),
            ('BL_LepPt[1]', 25, None),
						('BL_mll', 20, None),
            ('BL_mt2', None, 80),
            ('min(BL_mTl1,BL_mTl2)', None, 100),
            ('met_Sig', None, 5),
						('mjj',  None, 350),
						('BL_ECIDS_L_pass[0]', 1),
						('BL_ECIDS_L_pass[1]', 1),
						('fabs(BL_LepEta[0])', 0, 2),
						('fabs(BL_LepEta[1])', 0, 2),
            #],
            # Define N-1 cuts
            # All cuts will be applied apart from the one being plotted
            # The last four arguments format the N-1 arrow.
            # Format: lower arrow head, upper arrow head (where 0=single head
            # and 1=double head), arrow height, arrow width
            #n_mo_cuts=[
            #('mt2', None, 80, 0,0,1e4,20),
            #('min(mTl1,mTl2)', None, 100, 0,0,1e3,20),
            #('met_Sig', None, 5, 0,0,800,1),
						#('mjj',  None, 350, 0,0,1e2,100),
            ]
)

