import ROOT
from plotting.utils import rescale_limits


def plot_stack(stack, var=None):
    """Plots the stack. If var is specified, its binning and unit are used
    to construct the y label"""
    stack.Draw('HIST')
    if var:
        if len(var.bins) == 3:
            binwidth = (var.bins[2]-var.bins[1]) / float(var.bins[0])
            if var.unit:
                lbl = 'Events / {:.0f} {}'.format(binwidth, var.unit)
            else:
                lbl = 'Events / {}'.format(binwidth)
        else:
            if var.unit:
                lbl = 'Events {}'.format(var.unit)
            else:
                lbl = 'Events'
    else:
        lbl = 'Events'
    stack.GetYaxis().SetTitle(lbl)
    stack.GetXaxis().SetTitle(var.get_full_xlabel())
#    stack.GetYaxis().SetLabelSize(.05)
#    stack.GetYaxis().SetTitleSize(0.005)
    stack.GetYaxis().SetTitleOffset(1.05)


def plot_total_sm(total_sm):
    total_sm.SetLineColor(ROOT.kBlack)
    total_sm.SetFillColor(ROOT.kWhite)
    total_sm.SetFillStyle(1001)
    total_sm.SetLineStyle(1)
    total_sm.SetLineWidth(3)
    total_sm.SetMarkerSize(0)
    total_sm.Draw("HIST same][")
