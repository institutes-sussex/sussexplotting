from config import inputs
from config import config
import ROOT
import numpy as np
from utilities import helpers
from plotting.input_plotting import plot, plot_scatter
from plotting.input_plotting import make_sm_sum
from plotting.stack_plotting import make_final_plots
from plotting.n_minus_one_plotting import make_n_minus_one_plots
from plotting.compute_unc import total_sm_uncertainty, total_rel_unc


def merge_inputs(ipts):
    """Takes list of all inputs and merges the plots as requested. Modifies
    the list of inputs and removes inputs that have been added to another
    input due to merging."""

    # While loop to identify and merge all inputs as necessary
    master_inputs = [i for i in ipts if len(i._child_inputs) > 0]
    for ipt in master_inputs:
        for ichild in ipt._child_inputs:
            if ichild._master_input != ipt:
                print 'ERROR: Master and child input incompatible. Very wrong.'
                raise inputs.MissingInputError
            for syst in ipt.stored:
                try:
                    ipt.stored[syst].Add(ichild.stored[syst])
                except:
                    pass
            ipts.remove(ichild)


def plot_variable_for_region(var, region):
    """This function processes all inputs for the chosen combination of variable
    and region. Produced histograms are attached to the input objects."""

    allowed_inputs = [ipt for ipt in inputs.get_all()
                      if ipt.name not in region.banned_inputs]
    sm_inputs = [i for i in allowed_inputs if not i.isData and not i.isSignal]
    for input in allowed_inputs:
        plot(var, region, input)

    # Compute the sum of all standard model processes. Following function
    # loops over all inputs and combines the systematics based on their name.
    sm_sums = make_sm_sum(var, region, allowed_inputs)

    # Next step: we have uncertainty variations for all uncertainties on the
    # total SM contribution. Now combine the different uncertainties. To get
    # min and max around the central value.
    min_tot, max_tot = total_sm_uncertainty(sm_sums)
    rel_unc = total_rel_unc(sm_inputs, region)
    min_tot = np.sqrt(min_tot**2. + rel_unc[0]**2.)
    max_tot = np.sqrt(max_tot**2. + rel_unc[1]**2.)

    min_max_total = min_tot, max_tot

    # Clean the allowed inputs list from stuff that is merged after merging
    merge_inputs(allowed_inputs)
    # Create the stack plots.
    make_final_plots(allowed_inputs, var, region, sm_sums, min_max_total)
    for hist in sm_sums.values():
        try:
            helpers.delete_tobject(hist)
        except:
            helpers.delete_tobject(hist[0])
            helpers.delete_tobject(hist[1])
    for ipt in allowed_inputs:
        for hist in ipt.stored.values():
            try:
                helpers.delete_tobject(hist)
            except:
                helpers.delete_tobject(hist[0])
                helpers.delete_tobject(hist[1])


def plot_n_minus_one(var, region):
    """This function processes all inputs for the chosen combination of variable
    and region. Produced histograms are attached to the input objects."""

    allowed_inputs = [ipt for ipt in inputs.get_all()
                      if ipt.name not in region.banned_inputs]
    for input in allowed_inputs:
        plot(var, region, input)

    # Compute the sum of all standard model processes. Following function
    # loops over all inputs and combines the systematics based on their name.
    sm_sums = make_sm_sum(var, region, allowed_inputs)

    # Clean the allowed inputs list from stuff that is merged after merging
    merge_inputs(allowed_inputs)
    # Create the stack plots.
    make_n_minus_one_plots(allowed_inputs, var, region, sm_sums)
    for hist in sm_sums.values():
        try:
            helpers.delete_tobject(hist)
        except:
            helpers.delete_tobject(hist[0])
            helpers.delete_tobject(hist[1])
    for ipt in allowed_inputs:
        for hist in ipt.stored.values():
            try:
                helpers.delete_tobject(hist)
            except:
                helpers.delete_tobject(hist[0])
                helpers.delete_tobject(hist[1])


def plot_correlation(var1, var2, region):
    """This function processes all inputs for the chosen combination of variable
    and region. Produced histograms are attached to the input objects."""

    #ROOT.gStyle.SetPalette(ROOT.kViridis)
    ROOT.gStyle.SetPalette(ROOT.kBird)
    ROOT.gStyle.SetNumberContours(255)
    allowed_inputs = [ipt for ipt in inputs.get_all()
                      if ipt.name not in region.banned_inputs]
    for input in allowed_inputs:
        scatter = plot_scatter(var1, var2, region, input)
        canvas_name = 'canvas_{}_{}_{}_{}'.format(var1.name, var2.name,
                                                  region.name, input.name)
        canvas = ROOT.TCanvas(canvas_name, canvas_name, 800, 800)
        canvas.SetRightMargin(0.15)
        canvas.SetLogz()
        scatter.Draw('COLZ')
        outfile = config.output_prefix
        outfile += '/correlations/{}/{}'.format(region.name, input.name)
        helpers.ensure_directory_exists(outfile)
        outfile += '/{}_{}.{{}}'.format(var1.name, var2.name)
        for t in config.output_types:
            canvas.Print(outfile.format(t))

        helpers.delete_tobject(canvas)
