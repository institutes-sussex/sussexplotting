#!/usr/bin/env python

from config import vars, regions
from config.regions import region #  NOQA
from utilities import create_parser, helpers
from itertools import product, permutations
from plotting import plot_correlation
from config import style  # NOQA
import numpy as np
import ROOT
ROOT.TH1.SetDefaultSumw2()

helpers.title("Sussex SUSY plotting v42.00314159265359", True)

args = create_parser()

# Set the output level based on flag. Also turn off numpy complaining when
# dividing by zero for all but debug level
if args.verbose == 0:
    helpers.quiet_mode()
    np.seterr(divide='ignore', invalid='ignore')
elif args.verbose == 1:
    helpers.quiet_mode()
    helpers.enable_info()
    np.seterr(divide='ignore', invalid='ignore')
elif args.verbose == 2:
    helpers.quiet_mode()
    helpers.enable_debug()

for f in args.cfiles:
    execfile(f)

# We'll make the plots seperately per variable, using ROOTs TTree::Draw feature
# Get All combinations of regions and variables

var_permutations = permutations(vars.get_all(), 2)
regions_vars_combinations = product(regions.get_all(), var_permutations)

i=0
for reg, (var1, var2) in regions_vars_combinations:
    if i != args.plot:
        i += 1
        continue
    else:
        #print i
        plot_correlation(var1, var2, reg)
        break

helpers.title("Done. Bye Bye!", True)
