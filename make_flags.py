#!/usr/bin/env python

from config import vars, regions
from config.regions import region #  NOQA
from utilities import create_parser, helpers
from itertools import product
from plotting import plot_variable_for_region, plot_n_minus_one
from config import style  # NOQA
import sys
import numpy as np
import ROOT
ROOT.TH1.SetDefaultSumw2()

#helpers.title("Sussex SUSY plotting v0.02", True)

args = create_parser()

# Set the output level based on flag. Also turn off numpy complaining when
# dividing by zero for all but debug level
if args.verbose == 0:
    helpers.quiet_mode()
    np.seterr(divide='ignore', invalid='ignore')
elif args.verbose == 1:
    helpers.quiet_mode()
    helpers.enable_info()
    np.seterr(divide='ignore', invalid='ignore')
elif args.verbose == 2:
    helpers.quiet_mode()
    helpers.enable_debug()

if args.nobuffer:
    import config
    config.config.use_buffered_histograms = False

if args.autobuffer:
    import config
    config.config.automatically_deactivate_buffer = True

if args.autosystbuffer:
    import config
    config.config.delete_throwing_systs = True

if args.nmo:
    run_function = plot_n_minus_one
else:
    run_function = plot_variable_for_region

for f in args.cfiles:
    execfile(f)

# We'll make the plots seperately per variable, using ROOTs TTree::Draw feature
# Get All combinations of regions and variables
#
regions_vars_combinations = product(regions.get_all(), vars.get_all())
flags_string = ' '.join(sys.argv[1:])

outF = open("my_flags.txt", "w")
for i in range(len(list(regions_vars_combinations))):
    line = flags_string+" -p "+str(i)+"\n"
    outF.write( line )
outF.close()

#for reg, var in regions_vars_combinations:
#    run_function(var, reg)

#helpers.title("Done. Bye Bye!", True)
