#include "TGenPhaseSpace.h"
#include "susy_cpp.h"

#include <pybind11/cast.h>
#include <pybind11/numpy.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

PYBIND11_PLUGIN(susy_cpp) {
    py::module m("susy_cpp",
                 "Because pybind11 looks more interesting than actual work.");

    m.def("treesplitter", &TreeSplitter,
          "Splits given root files and only keeps selected and specified "
          "branches. Can also add additional variables.",
          py::arg("files"), py::arg("variables"), py::arg("output"),
          py::arg("treename"), py::arg("outputtreename") = "",
          py::arg("selection") = "1",
          py::arg("addvariables") = std::map<std::string, std::string>());

    return m.ptr();
}
