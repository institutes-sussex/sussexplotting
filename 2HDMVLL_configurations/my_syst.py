from config.syst import syst


# Include systematics
# Weight based systematics should be defined with isWeight=True
# In the case of the Fake ntuple, the nominal branch also needs to end with
# 'NONE'
syst(name='', central=True)
#syst(name='EG_SCALE_ALL__1', up_down=('up', 'down'))
#syst(name='FT_EFF_L', replaces='btagSFCentral', up_down=('_down', '_up'), isWeight=True )
#
#
## Fake ntuple systematic
#syst(name='syst_FF', isWeight=True, replaces='eventweight', method='add')
