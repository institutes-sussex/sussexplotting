from config.regions import region
from config.syst import syst


syst(name='', central=True)


region( 'Preselection_MuMu', 
        cuts = [ 
	          ('total_leptons', 2),
	          ('dilep_type', 1),
            #('met_met',  None, 60),
        ],
)
