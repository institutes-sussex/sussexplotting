from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np
import json

#lumiA = 36207.66
#lumiD = 44307.4
#lumiE = 58450.1
#lumiRun2 = 138965.16

lumiA = 1.
lumiD = 1.
lumiE = 1.

# **For histo buffering reasons, if the root file name changes, the name label
# must also change, otherwise it will throw a mismatch error**


# Define directory paths
basedirA = '/mnt/lustre/projects/epp/general/atlas/hs544/VLLem/mc16a/'
basedirD = '/mnt/lustre/projects/epp/general/atlas/hs544/VLLem/mc16d/'
basedirE = '/mnt/lustre/projects/epp/general/atlas/hs544/VLLem/mc16e/'

# Define weights
#basesel ='weight_globalLeptonTriggerSF*weight_jvt*weight_mc*weight_pileup*xs'
basesel ='(36207.66*(RunYear==2015||RunYear==2016)+44307.4*(RunYear==2017)+58450.1*(RunYear==2018))*weight_globalLeptonTriggerSF*weight_pileup*weight_jvt*weight_bTagSF_DL1r_70*weight_mc*xs/totalEventsWeighted'

# Include data
#merge(
#input(name='Data16',  files=basedirA+'data16.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True),
#input(name='Data17',  files=basedirD+'data17.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True),
#input(name='Data18',  files=basedirE+'data18.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True)
#)

# force_positive prevents negative fakes substracting from the total SM

# Grouped backgrounds

# ttbar
merge(
input(name='ttbar_A',    files=basedirA+'410470.root',    treename='nominal',    color=ROOT.kRed,     scale=lumiA,   weights=basesel,    label='ttbar'),  #, rel_unc=rel_unc ), 
input(name='ttbar_D',    files=basedirD+'410470.root',    treename='nominal',    color=ROOT.kRed,     scale=lumiD,   weights=basesel,    label='ttbar'),  #, rel_unc=rel_unc ), 
input(name='ttbar_E',    files=basedirE+'410470.root',    treename='nominal',    color=ROOT.kRed,     scale=lumiE,   weights=basesel,    label='ttbar')   #, rel_unc=rel_unc ) 
)

# include signal

merge(
input(name='dummysig_A',    files=basedirA+'410470.root',    treename='nominal',    color=ROOT.kRed,      linestyle=5,    scale=lumiE, weights=basesel,    label='signal',   isSignal=True),
input(name='dummysig_D',    files=basedirD+'410470.root',    treename='nominal',    color=ROOT.kRed,      linestyle=5,    scale=lumiE, weights=basesel,    label='signal',   isSignal=True),
input(name='dummysig_E',    files=basedirE+'410470.root',    treename='nominal',    color=ROOT.kRed,      linestyle=5,    scale=lumiE, weights=basesel,    label='signal',   isSignal=True)
)
