from config.vars import variable

# Variables to plot
# reverse_zn=true will reverse the Zn curves in N-1 plots in order to look at
# upper cuts instead of lower cuts
# Branch can be specified independent of plot name, so that upper and lower
# plots can be run together

variable( name='nJets',   branch='nJets_OR',      unit='',      label='N_{jets}',       bins=(10, 0, 10) )
variable( name='MET',     branch='met_met/1000',  unit='GeV',   label='E_{T}^{miss}',   bins=(50, 0, 500) )
